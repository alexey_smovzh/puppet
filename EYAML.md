### How to configure and use Hiera sensitive data encryption


##### 0. Project page
  https://github.com/voxpupuli/hiera-eyaml


##### 1. Install eyaml

```
  sudo /opt/puppetlabs/bin/puppetserver gem install hiera-eyaml

```


##### 2. Generate keys

```
  eyaml createkeys
  sudo mkdir /etc/puppetlabs/puppet/keys/
  sudo mv ./keys/* /etc/puppetlabs/puppet/keys/
  sudo chown -R puppet:puppet /etc/puppetlabs/puppet/keys/
  sudo chmod -R 0500 /etc/puppetlabs/puppet/keys/
  sudo chmod 0400 /etc/puppetlabs/puppet/keys/*.pem
  sudo ls -lha /etc/puppetlabs/puppet/keys/
  total 16K
    dr-x------ 2 puppet puppet 4.0K Jul 14 04:03 .
    drwxr-xr-x 4 root   root   4.0K Jul 23 17:35 ..
    -r-------- 1 puppet puppet 1.7K Jul 14 04:03 private_key.pkcs7.pem
    -r-------- 1 puppet puppet 1.1K Jul 14 04:03 public_key.pkcs7.pem
```


##### 3. Configure keys in hiera.yaml

```
- name: "Other YAML hierarchy levels"
  lookup_key: eyaml_lookup_key # eyaml backend
  paths:
    - "common.yaml"
  options:
    pkcs7_private_key: /etc/puppetlabs/puppet/keys/private_key.pkcs7.pem
    pkcs7_public_key:  /etc/puppetlabs/puppet/keys/public_key.pkcs7.pem
```


##### 4. Encrypt sensitive data

```
  sudo eyaml encrypt -l 'test_password' -s 'password' \       
                     --pkcs7-private-key=/etc/puppetlabs/puppet/keys/private_key.pkcs7.pem \
                     --pkcs7-public-key=/etc/puppetlabs/puppet/keys/public_key.pkcs7.pem

  test_password: ENC[PKCS7,MIIBeQYJKoZIhvcNAQcDoIIBajCCAWYCAQAxggEhMIIBHQIBADAFMAACAQEwDQYJKoZIhvcNAQEBBQAEggEArp0k8FFXcDS2qLmCGd1slBKzdLLIM1dUIDyY9O504pnlwYzd7R6e4VpG+S1/lpLZfDYqyDGpzJt+OYS8hx6ihl/w2/RVrsi25kjO/WvGJH1UqIie3vGqSwc1J/4nHg13OxafJLWhxcySqoYQvF70TVp9viGC2QS502GQqqt/guXMU8vkkqFI9otbpOE7rTBWYMX37ulHfE6+1FRpQG/M4LEz84Ju/ZAswJovZmjnbaAqFgjiNrJyrwJ0roK1AmxRK+fFPsJ7TgIfnwpyr0KYYgr9ph7hD2SEBUjaMoYoVR4G+zACX83QmMB24ByBrG6VctiWZc5oPfccrB63YTNL0jA8BgkqhkiG9w0BBwEwHQYJYIZIAWUDBAEqBBB4ogxzX0L2gKlpmAlTItBpgBCJ954cUSgyM90v6YrMemix]

  OR

  test_password: >
      ENC[PKCS7,MIIBeQYJKoZIhvcNAQcDoIIBajCCAWYCAQAxggEhMIIBHQIBADAFMAACAQEw
      DQYJKoZIhvcNAQEBBQAEggEArp0k8FFXcDS2qLmCGd1slBKzdLLIM1dUIDyY
      9O504pnlwYzd7R6e4VpG+S1/lpLZfDYqyDGpzJt+OYS8hx6ihl/w2/RVrsi2
      5kjO/WvGJH1UqIie3vGqSwc1J/4nHg13OxafJLWhxcySqoYQvF70TVp9viGC
      2QS502GQqqt/guXMU8vkkqFI9otbpOE7rTBWYMX37ulHfE6+1FRpQG/M4LEz
      84Ju/ZAswJovZmjnbaAqFgjiNrJyrwJ0roK1AmxRK+fFPsJ7TgIfnwpyr0KY
      Ygr9ph7hD2SEBUjaMoYoVR4G+zACX83QmMB24ByBrG6VctiWZc5oPfccrB63
      YTNL0jA8BgkqhkiG9w0BBwEwHQYJYIZIAWUDBAEqBBB4ogxzX0L2gKlpmAlT
      ItBpgBCJ954cUSgyM90v6YrMemix]
```


##### 5. Edit when needed

```
  sudo eyaml edit \
             --pkcs7-private-key=/etc/puppetlabs/puppet/keys/private_key.pkcs7.pem \
             --pkcs7-public-key=/etc/puppetlabs/puppet/keys/public_key.pkcs7.pem \
             /etc/puppetlabs/code/environments/production/hiera/common.yaml
```


##### 6. Insert encrypted data in hiera file

```
test_password:
  ENC[PKCS7,MIIBeQYJKoZIhvcNAQcDoIIBajCCAWYCAQAxggEhMIIBHQIBADAFMAACAQEwDQYJKoZIhvcNAQEBBQAEggEAByaeI6ZZhFAix7NM9wXLhMVH4tP33kq2ZTrCoj9ZyygCi0l+jDnCR3rBXtSXy3T7ImwRsmEUU+k3Er0fjFmBPhJ2cZMk20/NcW6++TCEmWx7lSy0sCKSXkvHTmOjyk+OZeGP4NtStMCeNtt4ro7842WYvfV3T8hWtYSVjUgTWjQoW/cK8MRa3jRfdWUSOTWMefebn5vVHFbu6Q96lVEgPYx/pyn4jntp4U1i2bZrtAVokNWxSkTWvniZH5zEk4Qr+xGVJI4rsvZp0BtN3wAcAEMvmVA4ocuciSUqc3cSioPLZj/6rIkvvyezUBzjrSWmNFMtGa2mQqywYSfxLfpI0jA8BgkqhkiG9w0BBwEwHQYJYIZIAWUDBAEqBBAfBs1r5eJN99jyeWzBDkE3gBDXgUqegQa1N5DG/dtWdb1J]
```

##### 7. Use in puppet manifest

```
  class test_module {

    $password = lookup('test_password')

  	notify { "password: ${password}": }

  	file { '/tmp/test':
  		ensure => present,
  		content => $password,
  	}

  	exec { 'encrypt_file':
  		command => "/usr/bin/zip -P ${password} /tmp/test.zip /tmp/test",
  		require => File['/tmp/test'],
  	}
  }
```


##### 8. Verify password

```
  unzip /tmp/test.zip
    Archive:  /tmp/test.zip
    [/tmp/test.zip] tmp/test password:
    extracting: tmp/test
```
