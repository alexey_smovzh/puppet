# setup ceph distributed storage for OpenStack
class profile::openstack_storage (
    Boolean $install = true
) {

  if $install == true {

    include consul::install
    include ceph::install

  } else {

    include consul::delete
    include ceph::delete

  }
}
