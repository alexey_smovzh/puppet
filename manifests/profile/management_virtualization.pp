class profile::management_virtualization (
    Boolean $install = true
) {

  if $install == true {

    include consul::install
    include pacemaker::install        # Provide floating ip. In production replace with VRRP on servers switch
    include ceph::install
    include docker::install
#    include lxd::install

  } else {

#    include lxd::delete
    include docker::delete
    include ceph::delete
    include pacemaker::delete
    include consul::delete

  }
}
