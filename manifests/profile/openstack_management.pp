# setup openstack management software mysql, rabbitmq, keystone, nova etc.
class profile::openstack_management (
    Boolean $install = true
) {


  if $install == true {

    include pacemaker::install
    include haproxy::install

    include mariadb::install
    include rabbitmq::install
    include openstack::install


#    class { 'gogs::dockerize': swarm => true }

  } else {

    include openstack::delete
    include mariadb::delete
    include rabbitmq::delete

    include haproxy::delete
    include pacemaker::delete

  }
}
