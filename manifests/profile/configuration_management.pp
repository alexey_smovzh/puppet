# setup configuration management services
# puppet, gogs git server, pxe boot server
class profile::configuration_management (
    Boolean $install = true
) {

  if $install == true {

    #	include gogs::install
    include pxe_boot_server::install

  } else {

    include gogs::delete
    include pxe_boot_server::delete

  }
}
