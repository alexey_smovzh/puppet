class profile::docker_swarm (
    Boolean $install = true
) {

  if $install == true {
    
    include consul::install
    include pacemaker::install        # Provide floating ip. In production replace with VRRP on servers switch
    include ceph::install
    include docker::install

    # create/pull images, run services
    class { 'gogs::dockerize': swarm => true }
    include mariadb::dockerize

  } else {

    include docker::delete
    include ceph::delete
    include pacemaker::delete
    include consul::delete

  }
}
