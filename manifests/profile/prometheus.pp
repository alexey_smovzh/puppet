# Install prometheus and exporter to get SNMP traps
class profile::prometheus (
    Boolean $install = true
) {
  # Hiera values
#  $resource = lookup('resource', Hash)

  if $install == true {

    class { 'prometheus::install': version => 'prometheus-2.3.1.linux-amd64' }
    class { 'prometheus::snmp_exporter::install': version => 'snmp_exporter-0.11.0.linux-amd64' }

  } else {

    include prometheus::delete
    include prometheus::snmp_exporter::delete

  }
}

# Install metrics exporter to nodes
class profile::prometheus::node_exporter (
    Boolean $install = true
) {
  case $::operatingsystem {
      'RedHat', 'Fedora', 'CentOS', 'Scientific', 'SLC', 'Ascendos', 'CloudLinux', 'PSBM', 'OracleLinux', 'OVS', 'OEL': {
          # redhat specific
      }
      'ubuntu', 'debian': {
          if $install == true {
              class { '::prometheus::node_exporter_ubuntu::install':
                              version => 'node_exporter-0.16.0.linux-amd64',
            	                home => '/opt/prometheus_node_exporter',
              }
          } else {
              class { '::prometheus::node_exporter_ubuntu::delete':
                              home => '/opt/prometheus_node_exporter',
              }
          }
      }
      'SLES', 'SLED', 'OpenSuSE', 'SuSE': {
          # suse specific
      }
  }
}
