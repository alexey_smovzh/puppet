# baremetal server defaults
class baremetal {

}

# virtual server defaults
class virtual {

}

# Defaults common for all nodes
class server {

	include default_admin
	include hosts
#	include ntp
	include chrony::install
	include ssh_server
	include rsyslog
	include disable_ipv6

	include network_config

	# todo: refactor to new manifest structure
	class { 'profile::prometheus::node_exporter': install => true } 				# export host metrics to prometheus

}

node default {

	# defaults to all servers
	include server

	# virtual machines and containers specific
	if $is_virtual {
		include virtual

	# baremetal servers specific
	} else {
		include baremetal
	}

	# By this instruction: https://www.example42.com/2018/04/23/puppet_tutorial_part_3/
	# Each node assigns with apropriate profile.
	# For now it assign via profile.yaml file, with are created during node OS
	# installation by install_puppet5.sh script from pxe_boot_server module.
	#
	# 		$ cat /opt/puppetlabs/facter/facts.d/profile.yaml
  #				---
	# 			profile: configuration_management
	#
	# In OpenStack enviroment this facts will be collected from VM metadata
	# like described in this article:
	# https://www.chriscowley.me.uk/blog/2015/09/10/how-i-classify-puppet-nodes/
	#
	# Also need to install and configure r10k for code branch management

	if $profile != undef {
		# todo: refactor
		# 			in this context decision install service or remove does't make sence
		# 	    because here modules can't be separated which are install and witch delete
		class {"profile::${profile}": install => true }
	}
}
