### 0. Create repository
Create repository puppet-production in gogs web interface without initialization


### 1. init git repo

```
cd /etc/puppetlabs/code/environments/production
sudo git init
sudo git add .
sudo git commit -m "Initial commit of Puppet files"
sudo git remote add origin http://alex:alex@puppet:3000/alex/puppet-production.git
sudo git push -u origin master
```


### 2. create git hook to pull git changes to puppet manifests folder after commit


```
$ sudo vi /var/opt/gogs/gogs-repositories/alex/puppet-production.git/hooks/post-receive

# pull changes
sudo /usr/bin/git --git-dir=/etc/puppetlabs/code/environments/production/.git/ --work-tree=/etc/puppetlabs/code/environments/production pull
```



### 3. allow gogs-git run git command in puppet folder as root without password



```
sudo vi /etc/sudoers

# Authorize gogs-git to use git command as root without password
gogs-git	ALL=(ALL)	NOPASSWD: /usr/bin/git
```
