0. This convention about puppet code style consist from a set of rules.
It is created with make "barrier to entry" lower as possible in mind.
Please read it before writing some module and strongly follow.


1. All new modules and changes to existing must be made on development git branch.
After module testing and validating create pull request to production branch.
Do not commit directly to production branch!
Commiting directly to production branch must be programmatically forbidden by git hooks.


2. Always write useful commit message.
Must be forbidden to commit with empty message by git hooks.
Read this article before first commit https://habr.com/post/416887/


3. Try to write modules straightforward as possible. Make it look like install
instructions from packet manual.


4. We do not need to support many OS, platforms and versions varieties.
Write modules with less use of condition statements. Do not avoid
to use absolute paths in variables and to use commands specific only to our linux
distribution.


5. Each module must contain README.md file with useful information about this module.

Mandatory is:
  - Description of module, it purposes, included packages
  - Operating systems and versions for what that module was designed and tested on
  - Examples with module usage
  - Known backgrounds and issues with this modules
  - References to documentation used during module development

Optional:
  - Graphical schema of current setup (if applicable)


6. After writing new or does major changes in existing module (makes many
changes in module logic), always test module by repeating install/remove cycle
3-4 times and checking result of puppet agent output, service status
and created files and directories.
This helps to detect sophisticated errors.
Because in some cases you can make error in logic and miss to delete
some resource. In that case first deletion will pass without any error, but
second installation and removing will fail.


7. Module manifest must be separated to three sections:
  - Init section, stored in init.pp file.
    This section dedicated for assign variables and evaluate Hiera data

  - Install section - install.pp file
    Code to install and configure module software

  - Delete section - delete.pp file
    Code to delete all software packages, unneeded decencies,
    configuration files, run-time data and so

  - optionally: Dockerize section - dockerize.pp
    Code to create and run module service in docker container

Usage example:
  \init.pp
      class app {

          # Evaluate Hiera data
          $file = lookup('file', String)

          # Create constant
          $path = '/opt/app'
      }


  \install.pp
      class app::install inherits app {

        # Thanks to inheritance we can use all variables from initial class
        exec {
          command => "/bin/touch ${path}/${file}",
        }
      }


  \delete.pp
      class app::delete inherits app {

        exec {
          command => "/bin/rm -f ${path}/${file}",
        }
      }


To use this module. You must provide module data in Hiera YAML file.
And then simply include module classes in profile:

      class profile::service (
          Boolean $install = true
      ) {

        if $install == true {

          include app::install
          include another_app::install
          include and_another_app::install

        } else {

          include app::delete
          include another_app::delete
          include and_another_app::delete

        }
      }


And finally in site.pp:

     class { 'profile::service': install => true }


To use app in docker container, assign to docker host via its profile:

      include app::dockerize



8. Because all configuration files are stored in GIT repository,
we don't need to preserve it when delete a package.
So uninstall service always with purge option:

      ensure => 'purged',



9. To be sure that service will be restarted automatically after configuration
change. Always add notify service command in configuration file section:

      notify => Service['my_service'],



10. Do not save passwords in manifest!
Always store passwords in Hiera yaml in encrypted form.
For configuration and instruction see EYAML.md file.


11. Augeas is the greate tool to manage configuration files. But not always
it is easy to use. For example to append configuration parameter with
new value, much easier by using conventional linux utils:
(good augeas doc: https://github.com/hercules-team/augeas/wiki/Path-expressions)

      $grub_conf = '/etc/default/grub'

      # Add kernel parameter
      exec { 'add_swapaccount':
        command => ". ${grub_conf}; \
                    /bin/sed -i \"s/\$GRUB_CMDLINE_LINUX_DEFAULT/\$GRUB_CMDLINE_LINUX_DEFAULT swapaccount=1/g\" ${grub_conf}",
        unless => "/bin/grep swapaccount ${grub_conf}",
        provider => 'shell',
        require => Package[$packages],
        notify => Exec['grub-update'],
      }

      # Remove kernel swapaccount parameter
      exec { 'remove_swapaccount':
        command => ". ${grub_conf}; \
                    /bin/sed -i \"s/swapaccount=1//g\" ${grub_conf}",
        onlyif => "/bin/grep swapaccount ${grub_conf}",
        provider => 'shell',
        require => Package[$packages],
        notify => Exec['grub-update'],
      }



12. Do not use ternary operators and puppet selector.
Do not use chaining ('->' and '~>' symbol).
Use metaparameters: require, subscribe, before, notify etc. instead.


13. Prefer to use 'content' parameter instead of 'source' to include some
content, because it is more economical.


14. Useful commands for testing puppet syntax before perform commit:

    $ puppet parser validate some_manifest.pp
    $ puppet apply --verbose --debug --trace --summarize some_manifest.pp
    $ puppet-lint some_manifest.pp


15. To get package default configuration files, you can download
and extract package to temporary directory without it actual installation.
(ar is the utility from binutils package)

    $ mkdir /tmp/<package>
    $ cd /tmp/<package>
    $ apt-get download <package>
    $ ar x <package>
