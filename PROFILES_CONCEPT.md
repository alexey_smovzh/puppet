/- Common
 |
 |-- admin accounts
 |-- hosts names and MAC addresses
 |
 |
 |---- Management virtualization profile (3 management servers)
 |     |
 |     |-- consul
 |     |-- ceph (depends on consult)
 |     |-- docker or lxd or kvm
 |     |
 |     |
 |     |-- openstack management profile (can be splitted to subprofiles)
 |         |
 |         |-- mariadb
 |         |-- rabbitmq
 |         |-- keystone
 |         |-- nova
 |         |-- cinder
 |         |-- horizon
 |         |-- telemetry
 |         |
 |         |-- prometheus
 |         |-- elastic
 |         |-- grafana
 |         |
 |         |-- puppet
 |         |-- git
 |         |-- pxe_boot_server
 |         |
 |         |-- OSPF routing daemon with VXLAN support and network channel encryption
 |
 |
 |---- openstack compute profile  (8 compute servers)
 |     |
 |     |-- compute node
 |
 |
 |---- ceph storage profile       (8 storage servers)
 |     |
 |     |-- consul
 |     |-- ceph (depends on consult)
 |
 |
 |---- backup profile       (2 backup servers)
 |     |
 |     |-- NFS server
 |
 |
 |-----/ Services running in OpenStack
       |
       |---- Novell profile
       |     |
       |     |-- eDirectory
       |     |-- GroupWise
       |     |-- print server
       |     |-- Vibe
       |     |-- Messenger
       |     |-- File server
       |
       |
       |---- Windows profiles?
             |
             |-- terminal server
             |-- antivirus
             |-- liga
