0. Description
This module installs Prometheus node exporter. This node exporter must be
installed on all servers which we want to monitor by Prometheus.
This module contains different manifests for different OS and Linux distribution.
Which of it use are decided in init.pp file.


1. Tested environments
This module developed and tested on Ubuntu 16.04LTS only.


2. Usage
Node exporter activated by default on all server nodes. There are different
manifests for specific server OS or Distribution. Which manifest apply,
install or delete node exporter all this logic concentrated in module init.pp.

```
  include prometheus_node_exporter
```


Check metrics are exported http://<yor_node>:9100 i.e. http://prometheus:9100

To view available metrics and its default state use command:

```
$ node_exporter --help
```

To enable some metric pass it as parameter --collector.<name> in ExecStart
parameter in templates/node_exporter.service.erb
For example:

```
ExecStart=/usr/bin/node_exporter --collector.cpu --no-collector.wifi
```

After installing node exporter on some node. Do not forget to add it to
prometheus/templates/prometheus.yml.erb to "scrape_configs:" Node Exporter section.


3. Known backgrounds and issues
Not found any yet


4. Used documentation
First steps with Prometheus: https://prometheus.io/docs/introduction/first_steps/
