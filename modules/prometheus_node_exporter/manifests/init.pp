class prometheus_node_exporter {

    $install = true

    case $::operatingsystem {
        'RedHat', 'Fedora', 'CentOS', 'Scientific', 'SLC', 'Ascendos', 'CloudLinux', 'PSBM', 'OracleLinux', 'OVS', 'OEL': {
            # redhat specific
        }
        'ubuntu', 'debian': {
            class { ::prometheus_node_exporter::node_exporter_ubuntu:
                install => $install,
                version => 'node_exporter-0.16.0.linux-amd64',
          }
        }
        'SLES', 'SLED', 'OpenSuSE', 'SuSE': {
            # suse specific
        }
    }
}
