class openstack::horizon::install inherits openstack::install {

  # include apache2 reload class to make possible to call => Service['apache2']
  include openstack::apache2::reload


  # install horizon
  package { 'openstack-dashboard':
    ensure => installed,
    require => Class['openstack::install'],
  }

  # create config
  file { '/etc/openstack-dashboard/local_settings.py':
    ensure => present,
    owner => 'root',
    group => 'horizon',
    content => template('openstack/local_settings.py.erb'),
    require => Package['openstack-dashboard'],
    notify => Service['apache2'],
  }

  # horizon over conf file are disabled
  # otherwise we get error of dublicate declaration
  file { '/etc/apache2/conf-enabled/openstack-dashboard.conf':
    ensure => 'absent',
    require => File['/etc/openstack-dashboard/local_settings.py'],
  }

  # https://docs.openstack.org/horizon/latest/install/from-source.html
  # sudo /usr/share/openstack-dashboard/manage.py make_web_conf \
  #                                               --apache \
  #                                               --ssl \
  #                                               --sslkey=/etc/ssl/private/control.key \
  #                                               --sslcert=/etc/ssl/certs/control.crt \
  #                                               > /etc/apache2/sites-available/horizon.conf
  file { '/etc/apache2/sites-available/horizon.conf':
    ensure => present,
    owner => 'root',
    group => 'root',
    content => template('openstack/site_horizon.conf.erb'),
    require => File['/etc/apache2/conf-enabled/openstack-dashboard.conf'],
  }

  file { '/etc/apache2/sites-enabled/horizon.conf':
    ensure => link,
    target => '/etc/apache2/sites-available/horizon.conf',
    require => File['/etc/apache2/sites-available/horizon.conf'],
    notify => Service['apache2'],
  }
}



class openstack::horizon::delete inherits openstack::install {

}
