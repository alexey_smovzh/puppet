class openstack::apache2::install inherits openstack::install {

  $apache = [ 'apache2',
              'libapache2-mod-wsgi',]

  $modules = [ 'wsgi', 'ssl' ]


  # install apache2
  package { $apache:
    ensure => 'installed',
  }

  # create config
  file { '/etc/apache2/apache2.conf':
    ensure => present,
    content => template('openstack/apache2.conf.erb'),
    require => Package[$apache],
  }

  # delete default sites
  exec { 'delete_apache_default_sites':
    command => "/bin/rm -f /etc/apache2/sites-enabled/*",
    user => 'root',
    group => 'root',
    require => Package[$apache],
    onlyif => '/bin/ls /etc/apache2/sites-enabled/*default*',
  }

  # enable modules
  $modules.each |$module| {
    exec { "enable_apache_module_${module}":
      command => "/usr/sbin/a2enmod ${module}",
      user => 'root',
      group => 'root',
      require => Package[$apache],
      creates => "/etc/apache2/mods-enabled/${module}.load",
    }
  }
}

class openstack::apache2::reload inherits openstack::apache2::install {

  service { 'apache2':
    ensure => running,
    enable => true,
  }

}
