class openstack::keystone::install inherits openstack::install {

  # include apache2 reload class to make possible to call => Service['apache2']
  include openstack::apache2::reload


  # create keystone database
  openstack::utils::create_database { 'create_database_keystone':
    dbroot => $db_root_pass,
    dbname => $keystone_db,
    dbuser => $keystone_db_user,
    dbpass => $keystone_db_pass,
  }

  # install keystone
  package { 'keystone':
    ensure => installed,
    require => [ Openstack::Utils::Create_database['create_database_keystone'],
                 Class['openstack::install'], ]
  }

  # create config
  file { '/etc/keystone/keystone.conf':
    ensure => present,
    owner => 'keystone',
    group => 'keystone',
    content => template('openstack/keystone.conf.erb'),
    require => Package['keystone'],
    notify => Service['apache2'],
  }

  # populate the Identity service database
  exec { "populate_database_${keystone_db}":
    command => "/bin/sh -c \"keystone-manage db_sync\" ${keystone_db}",
    user => 'root',
    group => 'root',
    provider => 'shell',
    require => File['/etc/keystone/keystone.conf'],
    onlyif => "/bin/hostname | /bin/grep ${initial_node}",        # run only on initial node, galera cluster replicates to others
    unless => "/usr/bin/mysql -u ${keystone_db_user} \
                               -p${keystone_db_pass} \
                               -e \"SHOW TABLES;\" \
                               ${keystone_db} | /bin/grep region",
  }

  # Initialize Fernet key repositories
  exec { 'initialize_key_repositories':
    command => "/usr/bin/keystone-manage fernet_setup --keystone-user ${keystone_admin_user} --keystone-group ${keystone_admin_user} \
             && /usr/bin/keystone-manage credential_setup --keystone-user ${keystone_admin_user} --keystone-group ${keystone_admin_user}",
    provider => 'shell',
    require => Exec["populate_database_${keystone_db}"],
    creates => [ '/etc/keystone/fernet-keys/0',
                 '/etc/keystone/fernet-keys/1', ],
  }

  # bootstrap the Identity service
  exec { 'bootstrap_keystone':
    command => "keystone-manage bootstrap --bootstrap-password ${keystone_admin_pass} \
                                          --bootstrap-admin-url http://${controller}:5000/v3/ \
                                          --bootstrap-internal-url http://${controller}:5000/v3/ \
                                          --bootstrap-public-url http://${controller}:5000/v3/ \
                                          --bootstrap-region-id ${region}",
    provider => 'shell',
    require => Exec['initialize_key_repositories'],
    unless => "/usr/bin/mysql -u ${keystone_db_user} \
                               -p${keystone_db_pass} \
                               -e \"SELECT id FROM region;\" \
                               ${keystone_db} | /bin/grep ${region} ",
  }

  # create keystone apache site config
  file { '/etc/apache2/sites-available/keystone.conf':
    ensure => present,
    owner => 'root',
    group => 'root',
    content => template('openstack/site_keystone.conf.erb'),
    require => Exec['bootstrap_keystone'],
  }

  # enable keystone site
  file { '/etc/apache2/sites-enabled/keystone.conf':
    ensure => 'link',
    target => '/etc/apache2/sites-available/keystone.conf',
    require => File['/etc/apache2/sites-available/keystone.conf'],
    notify => Service['apache2'],
  }

  /*
      create identity service domain, project, users, roles
  */
  # openstack domain create --description "${domain} domain" $domain
  # openstack project create --domain ${domain} --description "Service Project" service
  # openstack project create --domain ${domain} --description "Main Project" ${project}
  #
  # hash with commands and its checks
  $commands = {
    "create_domain" => [ "/usr/bin/openstack domain create --description '${domain} domain' $domain", "/usr/bin/openstack domain list | /bin/grep ${domain}" ],
    "create_project_service" => [ "/usr/bin/openstack project create --domain ${domain} --description 'Service Project' service", "/usr/bin/openstack project list | /bin/grep service" ],
    "create_project_${project}" => [ "/usr/bin/openstack project create --domain ${domain} --description '${project} Project' ${project}", "/usr/bin/openstack project list | /bin/grep ${project}" ],
  }

  $commands.each |$command, $code| {

    exec { "keystone_${command}":
      command => $code[0],
      provider => 'shell',
      environment => $environment,
      onlyif => "/bin/hostname | /bin/grep ${initial_node}",
      unless => $code[1],
      require => Service['apache2'],
    }
  }

  #  openstack user create --domain ${domain} --password PASSWORD new-user
  #  openstack role add --project myproject --user myuser myrole
  $admins.each |$admin, $value| {

    $password = $value['password']

    if $value['enabled'] == true {
      exec { "create_openstack_user_${admin}":
        command => "/usr/bin/openstack user create --domain ${domain} --password ${password} ${admin} \
                 && /usr/bin/openstack role add --project ${project} --user ${admin} admin",
        provider => 'shell',
        environment => $environment,
        onlyif => "/bin/hostname | /bin/grep ${initial_node}",
        unless => "/usr/bin/openstack user list | /bin/grep ${admin}",
        require => Service['apache2'],
      }

    # otherwise disable user accound
    } else {
      exec { "disable_openstack_user_${admin}":
        command => "/usr/bin/openstack user set ${admin} --disable",
        provider => 'shell',
        environment => $environment,
        onlyif => "/bin/hostname | /bin/grep ${initial_node}",
        unless => "/usr/bin/openstack user show ${admin} | /bin/grep 'enabled.*False'",
        require => Service['apache2'],
      }
    }
  }
}


class openstack::keystone::delete inherits openstack::delete {

}
