class openstack::install inherits openstack {


  # add openstack ubuntu repository
  exec { "add_${release}_repository":
    command => "/usr/bin/add-apt-repository cloud-archive:${release}",
    user => 'root',
    group => 'root',
    require => Class[ 'mariadb::install',
                      'pacemaker::install',
                      'haproxy::install',
                      'rabbitmq::install' ],
    unless => "/usr/bin/test -f /etc/apt/sources.list.d/cloudarchive-${release}.list",        # Check if repository file already exists and don't run exec tasks if it true
  }

  # check if it not already defined and install depencies
  $depencies.each|$dep| {
    if !defined(Package[$dep]) {
      package { $dep:
        ensure => 'installed',
        require => Exec["add_${release}_repository"],
      }
    }
  }

  # create user for openstack queue
  rabbitmq::utils::create_user { 'rabbitmq_openstack':
    user => $rabbitmq_user,
    password => $rabbitmq_pass,
    group => 'administrator',
  }


  include openstack::apache2::install

  include openstack::keystone::install
  include openstack::glance::install
  include openstack::nova::install
  include openstack::neutron::install
  include openstack::horizon::install
  include openstack::cinder::install
  include openstack::heat::install


  # todo: restart apache after modules installation

}
