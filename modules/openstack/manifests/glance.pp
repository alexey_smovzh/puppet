class openstack::glance::install inherits openstack::install {

  # https://docs.openstack.org/glance/rocky/install/verify.html
  # https://docs.openstack.org/glance/rocky/install/install-ubuntu.html
  $services = [ 'glance-registry',
                'glance-api', ]


  # create glance database
  openstack::utils::create_database { 'create_database_glance':
    dbroot => $db_root_pass,
    dbname => $glance_db,
    dbuser => $glance_db_user,
    dbpass => $glance_db_pass,
  }

  # openstack user create --domain default --password-prompt glance
  # openstack role add --project service --user glance admin
  # openstack service create --name glance --description "OpenStack Image" image
  # openstack endpoint create --region RegionOne image public http://controller:9292
  # openstack endpoint create --region RegionOne image internal http://controller:9292
  # openstack endpoint create --region RegionOne image admin http://controller:9292
  $commands = {
    "create_user" => [ "/usr/bin/openstack user create --domain ${domain} --password ${glance_admin_pass} ${glance_admin_user}", "/usr/bin/openstack user list | /bin/grep ${glance_admin_user}" ],
    "add_role" => [ "/usr/bin/openstack role add --project service --user ${glance_admin_user} admin", "/usr/bin/openstack role assignment list --user ${glance_admin_user} --names | /bin/grep service@${domain}" ],
    "create_service" => [ "/usr/bin/openstack service create --name glance --description 'OpenStack Image Service' image", "/usr/bin/openstack service list | /bin/grep glance" ],
    "create_public" => [ "/usr/bin/openstack endpoint create --region ${region} image public http://${controller}:9292", "/usr/bin/openstack endpoint list | /bin/grep 'image.*public'" ],
    "create_internal" => [ "/usr/bin/openstack endpoint create --region ${region} image internal http://${controller}:9292", "/usr/bin/openstack endpoint list | /bin/grep 'image.*internal'" ],
    "create_admin" => [ "/usr/bin/openstack endpoint create --region ${region} image admin http://${controller}:9292", "/usr/bin/openstack endpoint list | /bin/grep 'image.*admin'" ],
  }

  $commands.each |$command, $code| {

    exec { "glance_${command}":
      command => $code[0],
      provider => 'shell',
      environment => $environment,
      onlyif => "/bin/hostname | /bin/grep ${initial_node}",
      unless => $code[1],
      require => [ Openstack::Utils::Create_database['create_database_glance'],
                   Class['openstack::keystone::install'], ]
    }
  }

  # install glance
  package { 'glance':
    ensure => installed,
    require => Openstack::Utils::Create_database['create_database_glance']
  }


  # create config
  file { '/etc/glance/glance-api.conf':
    ensure => present,
    owner => 'glance',
    group => 'glance',
    content => template('openstack/glance-api.conf.erb'),
    require => Package['glance'],
    notify => Service[$services],
  }

  # create registry config
  file { '/etc/glance/glance-registry.conf':
    ensure => present,
    owner => 'glance',
    group => 'glance',
    content => template('openstack/glance-registry.conf.erb'),
    require => Package['glance'],
    notify => Service[$services],
  }

  # populate the Image service database
  exec { "populate_database_${glance_db}":
    command => "/bin/sh -c \"glance-manage db_sync\" ${glance_db}",
    user => 'root',
    group => 'root',
    provider => 'shell',
    require => [ File['/etc/glance/glance-api.conf'],
                 File['/etc/glance/glance-registry.conf'], ],
    notify => Service[$services],
    onlyif => "/bin/hostname | /bin/grep ${initial_node}",        # run only on initial node, galera cluster replicates to others
    unless => "/usr/bin/mysql -u ${glance_db_user} \
                               -p${glance_db_pass} \
                               -e \"SHOW TABLES;\" \
                               ${glance_db} | /bin/grep images",
  }

  # ensure service is running and enabled
	service { $services:
		ensure => running,
		enable => true,
    require => Exec["populate_database_${glance_db}"],
	}

/*
  # upload images
  $glance_images.each |$image| {
    # image full name - all from last slash
    $fn = regsubst($image, '^(.*[\\\/])', '')
    # short name
    $sn = regsubst($fn, '[^-]*', '')

    # download image
    exec { "download_glance_image_${fn}":
      command => "/usr/bin/wget ${image} \
               && /usr/bin/openstack image create '${sn}' \
                                            --file /tmp/${fn} \
                                            --disk-format qcow2 \
                                            --container-format bare \
                                            --public \
               && /bin/rm -f /tmp/${fn} ",
      user => 'root',
      group => 'root',
      cwd => '/tmp',
      environment => $environment,
      require => Service[$services],
      unless => "/usr/bin/openstack image list | /bin/grep ${sn}",
#      unless => "/usr/bin/test -f ${image_dir}/${sn}",
    }

  }
*/
}
