class openstack {

  $depencies = [ 'software-properties-common',
                 'python-pymysql',
                 'python-openstackclient', ]

  # create openstack admins with default_admins list
  $admins = lookup('default_admin', Hash)

  # controller name assigned to floating ip provided by pacemaker
  $nodes = lookup('nodes_static', Hash)
  $controller = $nodes['floatip']['aliases']

  $mariadb = lookup('mariadb', Hash)
  $db_root_pass = $mariadb['password']

  $pacemaker = lookup('pacemaker', Hash)
  $floatip = $pacemaker['floatip']

  $rabbitmq = lookup('rabbitmq', Hash)
  $rabbitmq_nodes = $rabbitmq['nodes']

  # Rocky cloud archive only supported on Bionic
  # Queens cloud archive only supported on Xenial (Bionic includes Queens by default)
  $release = $::lsbdistcodename ? { 'bionic' => 'rocky', 'xenial' => 'queens' }

  $home = '/opt/openstack'

  $openstack = lookup('openstack', Hash)
  $services = $openstack['services']
  $initial_node = $openstack['initial_node']
  $region = $openstack['region']
  $domain = $openstack['domain']
  $project = $openstack['project']
  $rabbitmq_user = $openstack['rabbitmq_user']
  $rabbitmq_pass = $openstack['rabbitmq_pass']


  $keystone = $services['keystone']
  $keystone_admin_user = $keystone['admin_user']
  $keystone_admin_pass = $keystone['admin_pass']
  $keystone_db = 'keystone'
  $keystone_db_user = $keystone['db_user']
  $keystone_db_pass = $keystone['db_pass']

  $glance = $services['glance']
  $glance_admin_user = $glance['admin_user']
  $glance_admin_pass = $glance['admin_pass']
  $glance_db = 'glance'
  $glance_db_user = $glance['db_user']
  $glance_db_pass = $glance['db_pass']
  $glance_image_dir = $glance['image_dir']
  $glance_images = $glance['images']


  # administrative account environment variables
  $environment = ["OS_USERNAME=admin",
                  "OS_PASSWORD=${keystone_admin_pass}",
                  "OS_PROJECT_NAME=admin",
                  "OS_USER_DOMAIN_NAME=Default",
                  "OS_PROJECT_DOMAIN_NAME=Default",
                  "OS_AUTH_URL=http://${controller}:5000/v3",
                  "OS_IDENTITY_API_VERSION=3", ]

}
