
Minimal deployment for Rocky¶

At a minimum, you need to install the following services. Install the services in the order specified below:

    Identity service – keystone installation for Rocky
    Image service – glance installation for Rocky
    Compute service – nova installation for Rocky
    Networking service – neutron installation for Rocky

We advise to also install the following components after you have installed the minimal deployment services:

    Dashboard – horizon installation for Rocky
    Block Storage service – cinder installation for Rocky


admin:

export OS_USERNAME=admin
export OS_PASSWORD=keystone
export OS_PROJECT_NAME=admin
export OS_USER_DOMAIN_NAME=Default
export OS_PROJECT_DOMAIN_NAME=Default
export OS_AUTH_URL=http://controller:5000/v3
export OS_IDENTITY_API_VERSION=3



RabbitMQ HA params:

# transport_url = rabbit://<rabbit-userid>:<rabbit-password>@<rabbit-host-address>:5672
transport_url = <% @rabbitmq_nodes.each_with_index do |(node, value), idx| -%><%= @rabbitmq_user %>:<%= @rabbitmq_pass %>@<%= node %>:5672<% if idx < @nodes.length - 1 -%>,<% end -%><% end -%>
replicated_policy_engine = True
rabbit_retry_interval=1
rabbit_retry_backoff=2
rabbit_max_retries=0
rabbit_durable_queues=true
rabbit_ha_queues=true













### 0. Description
This module install Consul cluster.
Cluster name and node list you must define in corresponding Hiera YAML file.


### 1. Tested environments
This module developed and tested on Ubuntu 16.04LTS only.


### 2. Usage
Make sure that Consul hosts can resolve each other by name.
Describe Consul cluster configuration in appropriate Hiera YAML file.

For example:

```
consul:
  cluster: docker_consul
  nodes: ["docker0:8301", "docker1:8301", "docker2:8301"]

```


Then to install or delete:

```
  # Install
  include consul::install

  # Delete
  include consul::delete
```


### 3. Known backgrounds and issues
Not found yet
todo: tls authentification!!!


### 4. Used documentation
