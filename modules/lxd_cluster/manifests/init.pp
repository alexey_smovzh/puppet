class lxd_cluster (Boolean $install = true) {

      class { ::lxd_cluster::drbd:
        install => $install,
        version => 'prometheus-2.3.1.linux-amd64',
      }

      class { ::lxd_cluster::lxd:
        install => $install,
        version => 'snmp_exporter-0.11.0.linux-amd64',
      }
}
