### 0. Description
This module installs Elasticsearch and Logstash for collecting logs from
servers and network equipment and netflow traffic.

Configured plugins:
- Syslog: listens on port 5140 TCP


### 1. Tested environments
This module developed and tested on Ubuntu 16.04LTS only.


### 2. Usage
To install Elasticsearch use:

```
  class { elasticsearch: install => true }
```

To purge Elasticsearch and all its depencies use:  

```
  class { elasticsearch: install => false }
```


### 3. Known backgrounds and issues
Graylog module are use Elasticsearch version 5.x, its not compatible
with version 6.x from this module. When use this module with Graylog module
on the same node error "dublicate class reference [elasticsearch]" arise.


### 4. Used documentation
Install instructions for Debian and Ubuntu:
https://www.elastic.co/guide/en/elasticsearch/reference/current/deb.html

TODO: practical experience: https://habr.com/company/yamoney/blog/419041/

Syslog plugin configuration:
https://support.halon.io/hc/en-us/articles/360000700065-Remote-syslog-to-Logstash
https://www.elastic.co/guide/en/logstash/current/plugins-inputs-syslog.html
