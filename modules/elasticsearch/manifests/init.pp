# https://logz.io/learn/complete-guide-elk-stack
# https://logz.io/blog/elasticsearch-cluster-tutorial/

class elasticsearch (Boolean $install = true) {

	# Constants
  $depencies = ['default-jre', 'apt-transport-https']
	$services = ['elasticsearch', 'logstash']
  $elastic_dir = ['/var/opt/elasticsearch', '/var/opt/elasticsearch/data', '/var/opt/elasticsearch/log']

	/*
					Install ELK stack
	*/
	if $install == true {

      # install depencies if they are not already installed
      $depencies.each |$dependency| {
          if ! defined(Package[$dependency]) {
            package { $dependency:
                ensure => installed,
            }
          }
      }

      # add elasticsearch repo
      apt::apt_repository { 'elasticsearch':
        key => 'https://artifacts.elastic.co/GPG-KEY-elasticsearch',
        file => '/etc/apt/sources.list.d/elastic-6.x.list',
        repository => 'deb https://artifacts.elastic.co/packages/6.x/apt stable main',
      }

			# install packages
			package { $services:
        require => Apt::Apt_repository['elasticsearch'],
				ensure => installed,
			}

      # create Elasticsearch config
      file { '/etc/elasticsearch/elasticsearch.yml':
          ensure => present,
          owner => root,
          group => elasticsearch,
          content => template('elasticsearch/elasticsearch.yml.erb'),
					notify => Service['elasticsearch'],
          require => Package['elasticsearch'],
      }

			# create elasticsearch data and log directories
			file { $elastic_dir:
					ensure => 'directory',
					owner => 'elasticsearch',
					group => 'elasticsearch',
					recurse => true,
					recurselimit => 1,
          require => Package['elasticsearch'],
			}

      # create Logstash syslog config
      file { '/etc/logstash/conf.d/syslog.conf':
          ensure => present,
          owner => root,
          group => root,
          content => template('elasticsearch/syslog.conf.erb'),
					notify => Service['logstash'],
          require => Package['logstash'],
      }

			# ensure all services are enabled and running
			$services.each |$service| {
					service { $service :
	          	ensure => running,
	          	enable => true,
	          	require => Package[$service],
	      	}
			}

	/*
					Purge ELK stack
	*/
  } else {

      # Stop and disable services
      service { $services :
          ensure => stopped,
          enable => false,
      }

			# need to delete custom created files before call 'packet purge'
			# because if it remain, apt-get not delete configuration folder on purging
      # delete custom configs and repository
      file { $elastic_dir:
          ensure => absent,
          recurse => true,
          purge => true,
          force => true,
      }

      # Remove a packages and purge its config files
      package { [$services, $depencies]:
          ensure => 'purged',
      }

      # Remove already unneeded depencies and dowloaded apk
      apt::apt_clean { 'clean': }
	}
}
