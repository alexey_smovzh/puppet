### 0. Description
This module provides static /etc/hosts file configuration for nodes that
can't rely on DNS service. Hosts information stored in Hiera in 'nodes_static:'
section.


### 1. Tested environments
This module developed and tested on Ubuntu 16.04LTS and 18.04LTS only.


### 2. Usage
Add host information to hiera/common.yaml:

```
prometheus:
  mac: 52:54:00:1f:9b:43
  ip: 192.168.122.110
  aliases: elasticsearch, grafana
  comment: Management and monitoring server
```

Include hosts class

```
  include hosts
```


### 3. Known backgrounds and issues
Not found any yet


### 4. Used documentation
