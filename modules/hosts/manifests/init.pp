class hosts {

  # delete 127.0.1.1 record
  exec { 'del_127.0.1.1':
    command => "/bin/sed -i '/127.0.1.1/d' /etc/hosts",
    onlyif => '/bin/grep 127.0.1.1 /etc/hosts',
  }

  # Getting data from Hiera
  lookup('nodes_static', Hash).each |String $name, Hash $value| {

      $ip = $value[ip]
      $comment = $value[comment]

      # In yaml file host aliases saved as string with comma separated
      # values. Also it could contain unpredictable amount of whitespaces.
      # Host_aliases param accepts only Array and can not be whitespaces in hostname
      if $value[aliases] != undef {
        $strip = regsubst($value[aliases], ' ', '', 'G')
        $aliases = split($strip, ',')
      }

      host { "${name}":
          ensure => present,
          ip => $ip,
          comment => $comment,
          host_aliases => $aliases,
      }
  }
}
