Facter.add('cluster_members') do
  setcode do
    if File.exist?('/snap/bin/lxd.lxc')
      Facter::Core::Execution.execute("/snap/bin/lxd.lxc cluster list | /bin/grep 'https' | /usr/bin/wc -l")
    end
  end
end
