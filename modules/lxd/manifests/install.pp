class lxd::install inherits lxd {

  # check if parameter already set
  # we need if statement to prevent execution of 'grub_update' on every puppet agent run
  # we can't use unless or onlyif statement because there are no way to check if grub was
  # already updated after config was changed or not
  if "/bin/grep swapaccount ${grub_conf}" == false {
    # Add kernel parameter
    exec { 'add_swapaccount':
      command => ". ${grub_conf}; \
                  /bin/sed -i \"s/\$GRUB_CMDLINE_LINUX_DEFAULT/\$GRUB_CMDLINE_LINUX_DEFAULT swapaccount=1/g\" ${grub_conf}",
      provider => 'shell',
      notify => Exec['grub_update'],
    }

    # update grub
# TODO: reboot host
    exec { 'grub_update':
      command => "/usr/sbin/update-grub",
      require => Exec['add_swapaccount'],
    }
  }

  # install snap
  package { $depencies:
    ensure => installed,
    require => Class['ceph::install',
                     'consul::install',
                     'pacemaker::install']
  }

  $members = inline_template("[<% @admins.each_with_index do |(key, value), idx| -%><%= key %><%= ', ' if idx < (@admins.size - 1) %><% end -%>]")
  # when installing from snap LXD group must be created before installation
  # also include admin users from default_admings to LXD group
  group { $user:
    ensure => present,
    system => true,
    members => $members,
  }

  # install LXD from snap
  exec { "install_${package}_on_${hostname}":
      command => "/usr/bin/snap install --stable ${package}",
      user => 'root',
      group => 'root',
      require => Package[$depencies],
      unless => "/usr/bin/snap list ${package}",
  }

  # create home directory
  file { $home:
    ensure => 'directory',
    owner => 'root',
    group => 'root',
    require => Exec["install_${package}_on_${hostname}"],
  }

  # in initial node preseed configs are different from pressed to joining node
  # on intial cluster setup generated certificate which we must propagate to
  # preseed file on other nodes
  if $hostname == $initial_node {
    # Create preseed cluster config
    # https://lxd.readthedocs.io/en/latest/clustering/
    file { $preseed:
      ensure => present,
      owner => 'root',
      group => 'root',
      content => template('lxd/cluster.preseed.erb'),
      require => File[$home],
    }

    # init cluster
    exec { "init_node_${hostname}":
      command => "/bin/cat ${preseed} | /snap/bin/lxd init --preseed",
      require => File[$preseed],
      tries => 3,                           # retry until daemon in snap starts
      try_sleep => 30,
      unless => "/snap/bin/lxd.lxc info | /bin/grep 'server_clustered: true'",
    }

    # grab cert and put it in KV
    exec { "put_kv_certificate":
      command => "/opt/consul/consul kv put ${certificate} $(/bin/sed ':a;N;$!ba;s/\\n/\\n\\n/g' ${home}/server.crt | /usr/bin/base32 -w 0 -)",
      user => 'root',
      group => 'root',
      require => Exec["init_node_${hostname}"],
      unless => "/opt/consul/consul kv get ${certificate}",
    }

  # joining node to existing cluster
  } else {

    file { $preseed:
      ensure => present,
      owner => 'root',
      group => 'root',
      content => template('lxd/cluster.preseed.erb'),
      require => File[$home],
    }

    # get cert from kv store and update preseed config
    exec { "get_kv_certificate":
      # we can not run shell command and save output to variable
      # we can not pass shell command AS IS to puppet manifest
      # because shell special characters are the same as a ruby special characters
      # so we need to split shell command to several parts to avoid
      # special characters overlapping
      # 1. echo cluster certificate to preseed file.
      #    in future position of double quote (") insert @ symbol
      # 2. replace @ symble with double quote
      # 3. insert ending double quote
      command => "/bin/echo \"  cluster_certificate: @$(/opt/consul/consul kv get ${certificate} | /usr/bin/base32 -d)\" >> ${preseed}
                  /bin/sed -i 's/@/\"/g' ${preseed}
                  /bin/echo '\"' >> ${preseed}",
      user => 'root',
      group => 'root',
      provider => 'shell',
      require => File[$preseed],
      onlyif => "/opt/consul/consul kv get ${certificate}",
    }

    # init cluster
    exec { "init_node_${hostname}":
      command => "/bin/cat ${preseed} | /snap/bin/lxd init --preseed",
      tries => 3,                           # retry until daemon in snap starts
      try_sleep => 30,
      require => Exec["get_kv_certificate"],
      unless => "/snap/bin/lxd.lxc info | /bin/grep 'server_clustered: true'",
    }
  }

  # cluster nodes count
  $size = $nodes.keys().length()

  # the following commands can be executed only in fully operational cluster
  # otherwise we can't assign new node to cluster due storage and profiles
  # configuration mismatch
  # compare cluster size from cluster_member.rb custom fact
  # and nodes count from Hiera configuration
  if "${cluster_members}" == "${size}" {
    include lxd::zfs::install
    include lxd::ceph::install
    include lxd::profile::install
  }
}
