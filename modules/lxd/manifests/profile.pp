class lxd::profile inherits lxd {
}


class lxd::profile::install inherits lxd::profile {

  # run only once on initial node
  if $hostname == $initial_node {

    # create our custom profiles
    $profiles.each|$profile, $value| {

      # storage == pool and profile name
      $storage = $value

      # create profile
      exec { "create_profile_${profile}":
        command => "/snap/bin/lxd.lxc profile create ${profile}",
        user => 'root',
        group => 'root',
        unless => "/snap/bin/lxd.lxc profile show ${profile}",
      }

      # create profile templates
      file { "$home/${profile}.profile.yaml":
        ensure => present,
        owner => $user,
        group => $user,
        content => template("lxd/${value}.profile.erb"),
        require => Exec["create_profile_${profile}"],
        notify => Exec["load_profile_${profile}"],
      }

      # load profiles
      # run only when template changes by notify from file section above
      exec { "load_profile_${profile}":
        command => "/snap/bin/lxd.lxc profile edit ${profile} < $home/${profile}.profile.yaml",
        user => 'root',
        group => 'root',
        refreshonly => true,
        require => File["$home/${profile}.profile.yaml"],
      }
    }
  }
}
