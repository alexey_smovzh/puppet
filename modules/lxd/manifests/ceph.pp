class lxd::ceph inherits lxd {

  $packages = ['xfsprogs']

  # Extract variables
  $cluster = $ceph['cluster_name']

  $client = "client.${user}"
  $keyring = "/etc/ceph/${cluster}.${client}.keyring"
  $ceph_kv = "ceph/${cluster}/lxd_keyring"

}


class lxd::ceph::install inherits lxd::ceph {

  # install needed extra packages
  package { $packages:
    ensure => installed,
  }

  # iterate over storages but process only Ceph
  $storages.each |$type, $value| {

    $storage = $value['name']
    $pool = $value['pool']

    if $type == 'ceph' {
      if $hostname == $initial_node {
        # generate ceph auth key for lxd
        exec { 'generate_ceph_lxd_key':
          command => "/usr/bin/ceph auth get-or-create-key ${client} \
                                    mgr 'allow r' \
                                    mon 'allow rw' \
                                    osd 'allow rwx pool=${pool}' \
                      && /usr/bin/ceph auth get ${client} -o ${keyring}",
          user => 'root',
          group => 'root',
          creates => $keyring,
        }

        # put keyring to consul KV store
        exec { 'put_keyring_consul_kv':
          command => "/opt/consul/consul kv put ${ceph_kv} $(/bin/cat ${keyring} | /usr/bin/base32 -w 0 -)",
          user => 'root',
          group => 'root',
          require => Exec['generate_ceph_lxd_key'],
          unless => "/opt/consul/consul kv get ${ceph_kv}",
        }

      } else {
        # get keyring from consul
        exec { 'get_ceph_lxd_key':
          command => "/opt/consul/consul kv get ${ceph_kv} | /usr/bin/base32 -d > ${keyring}",
          user => 'root',
          group => 'root',
          creates => $keyring,
        }
      }

      # prepare storage on each node
      $nodes.each|$node, $value| {
        exec { "prepare_storage_${pool}_${node}":
          command => "/snap/bin/lxd.lxc storage create --target ${node} ${storage} ceph source=${pool}",
          user => 'root',
          group => 'root',
          require => Package[$packages],
          unless => "/snap/bin/lxd.lxc storage show ${storage} | grep ${node}",
        }
      }

      # enable storage
      exec { "enable_storage_${storage}":
        command => "/snap/bin/lxd.lxc storage create ${storage} ceph \
                                                ceph.user.name=${user} \
                                                ceph.cluster_name=${cluster} \
                                                ceph.osd.pool_name=${pool} \
                                                volume.block.filesystem=xfs",
        user => 'root',
        group => 'root',
        require => Package[$packages],
        unless => "/snap/bin/lxd.lxc storage show ${storage} | grep 'status: Created'",
      }
    }
  }
}


class lxd::ceph::delete inherits lxd::ceph {

  # delete keyring from KV
  if $hostname == $initial_node {
    exec { 'delete_keyring_consul_kv':
      command => "/opt/consul/consul kv delete ${ceph_kv}",
      user => 'root',
      group => 'root',
      onlyif => "/opt/consul/consul kv get ${ceph_kv}",
    }
  }

  # delete keyring file
  file { $keyring:
    ensure => absent,
    purge => true,
    force => true,
  }

  package { $packages:
      ensure => 'purged',
  }

  # there are no code to delete ceph storage
  # because in any way we get permission denied from ceph
  # removal of this storage are done in ceph module
  # in pool deletion part
}
