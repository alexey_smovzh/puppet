class lxd::delete inherits lxd {

  # stop and delete all containers
  exec { 'stop_delete_containers':
    command => "for container in \$(sudo /snap/bin/lxd.lxc list --columns n --format csv)
                do
                  /snap/bin/lxd.lxc stop \$container
                  /snap/bin/lxd.lxc delete \$container
                done",
    provider => 'shell',
  }

  if "/bin/grep swapaccount ${grub_conf}" == true {
    # Remove kernel swapaccount parameter
    exec { 'remove_swapaccount':
      command => ". ${grub_conf}; \
                  /bin/sed -i \"s/swapaccount=1//g\" ${grub_conf}",
      onlyif => "/bin/grep swapaccount ${grub_conf}",
      provider => 'shell',
      require => Package[$packages],
      notify => Exec['grub-update'],
    }

    exec { 'grub_update':
      command => "/usr/sbin/update-grub",
      require => Exec['add_swapaccount'],
    }
  }

  # delete cluster certificate from KV store
  exec { "delete_kv_certificate":
    command => "/opt/consul/consul kv delete ${certificate}",
    user => 'root',
    group => 'root',
    onlyif => "/opt/consul/consul kv get ${certificate}",
  }

  # delete storages
  include lxd::zfs::delete
  include lxd::ceph::delete

  # Remove a packages and purge its config files
  package { $packages:
      ensure => 'purged',
      require => Service[$services],
  }

  # delete home
  file { $home:
    ensure => absent,
    purge => true,
    force => true,
    recurse => true,
    require => Package[$packages],
  }
}
