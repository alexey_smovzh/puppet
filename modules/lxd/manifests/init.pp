class lxd {

  $user = 'lxd'

  $depencies = ['snapd']
  $package = 'lxd'

  $grub_conf = '/etc/default/grub'
  $home = '/var/snap/lxd/common/lxd'
  $preseed = "${home}/cluster.preseed"

  # Get data from Hiera
  $admins = lookup('default_admin', Hash)
  $ceph = lookup('ceph', Hash)
  $lxd = lookup('lxd', Hash)
  $cluster = $lxd['cluster_name']
  $initial_node = $lxd['initial_node']
  $bootstrap_ip = resolv($initial_node)
  $storages = $lxd['storages']
  $profiles = $lxd['profiles']
  $nodes = $lxd['nodes']
  $nat_address = $lxd['nat_address']

  # Path to Consul data
  $certificate = "lxd/${cluster}/certificate"

}

# todo: network
# todo: local and distributed storage
# todo: profiles
# todo: server production configuration https://github.com/lxc/lxd/blob/master/doc/production-setup.md
