class lxd::zfs inherits lxd {
  $packages = ['zfsutils-linux']
}


class lxd::zfs::install inherits lxd::zfs {

  # install needed extra packages
  package { $packages:
    ensure => installed,
  }

  # iterate over storages but process only ZFS
  $storages.each |$type, $value| {

    $storage = $value['name']
    $disks = $value['device']

    if $type == 'zfs' {
      # run only once on initial node
      if $hostname == $initial_node {
        # concatenate devices into one string separated by space
        $devices = inline_template("<% @disks.each_with_index do |(key, value), idx| -%><%= key %><%= ' ' if idx < (@nodes.size - 1) %><% end -%>")
        # prepare storage on each node
        $nodes.each|$node, $value| {
          exec { "prepare_storage_${storage}_${node}":
            command => "/snap/bin/lxd.lxc storage create --target ${node} ${storage} zfs source=${devices}",
            user => 'root',
            group => 'root',
            require => Package[$packages],
            unless => "/snap/bin/lxd.lxc storage show ${storage} | grep ${node}",
          }
        }

        # create storage
        exec { "enable_storage_${storage}":
          command => "/snap/bin/lxd.lxc storage create ${storage} zfs",
          user => 'root',
          group => 'root',
          require => Package[$packages],
          unless => "/snap/bin/lxd.lxc storage show ${storage} | grep 'status: Created'",
        }
      }
    }
  }
}


class lxd::zfs::delete inherits lxd::zfs {

  # iterate over storages but process only ZFS
  $storages.each |$type, $value| {

    $storage = $value['name']

    if $type == 'zfs' {
      # delete storage
      exec { "delete_storage_${storage}":
        command => "/snap/bin/lxd.lxc storage delete ${storage}",
        user => 'root',
        group => 'root',
        onlyif => "/snap/bin/lxd.lxc storage show ${storage} | grep 'status: Created'",
      }
    }
  }

  package { $packages:
      ensure => 'purged',
  }
}
