### 0. Description
This module install LXD in cluster mode.
All cluster configuration must be provided with corresponding YAML file.
This module uses Ceph RBD for distributed persistent storage
and ZFS pool for local container storage.
Also make attention this module require:
- Consul KV storage to store and propagate Swarm cluster tokens.
- Pacemaker to handle float IP resource and container HA

This module setup consist of two puppet agent cycles.
On first are installed LXD and cluster initialized.
On second, creates storages and profiles on existing cluster and only if cluster
fully operational, e.g. count of members in yaml configuration file equal number
of nodes from output of 'lcx cluster list' command.
If we create storages and profiles before cluster initialization on all nodes
we can't add new node, because of storage and profiles configuration mismatch.


### 1. Tested environments
This module developed and tested on Ubuntu 18.04LTS only.


### 2. Usage
Make sure that LXD hosts can resolve each other by name.
Describe LXD cluster configuration in appropriate Hiera YAML file.
Notify that profile names corresponding to storage pool names.
Make sure thats profile templates exist in template folder.

For Example:

```
    lxd:
      cluster_name: lxd_management
      initial_node: *initial_node
      nat_address: 172.16.0.1/24

      storage:
        zfs:
          name: zfs_on_local
          device:
            - /dev/vdc
        ceph:
          name: xfs_on_ceph
          pool: lxd_pool

      profiles:
        distributed: xfs_on_ceph
        local: zfs_on_local

      nodes:
        lxd0:
        lxd1:
        lxd2:
```

Then to install or delete:

```
      # Install
      include lxd::install

      # Delete
      include lxd::delete
```


### 3. Known backgrounds and issues
Not found yet


### 4. Used documentation
Install: https://blog.ubuntu.com/2018/05/03/lxd-clusters-a-primer

Official docs: https://lxd.readthedocs.io/en/latest/

LXD Snap: https://stgraber.org/2016/10/17/lxd-snap-available/

LXD over Infiniband: https://community.mellanox.com/docs/DOC-2965
