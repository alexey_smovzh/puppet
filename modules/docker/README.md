### 0. Description
This module install Docker and initialize swarm mode.
All swarm cluster configuration must be provided with corresponding YAML file.
This module uses CephFS for persistent storage.
This module also includes rexray and wetopi/rbd plugins to connect
Docker volumes to Ceph RBD block devices. But they working very unstable.
So be careful and test it before use in production.
Also make attention - this module require Consul KV storage to store and
propagate Swarm cluster tokens.


### 1. Tested environments
This module developed and tested on Ubuntu 16.04LTS only.


### 2. Usage
Make sure that Docker hosts can resolve each other by name.
Describe Docker Swarm cluster configuration in appropriate Hiera YAML file.

For Example:

```
    # Docker swarm cluster
    docker:
        cluster_name: management
        initial_node: docker0
        nodes:
          docker0:
            manager: true
          docker1:
            manager: true
          docker2:
            manager: true
```

Then to install or delete:

```
    # Install
    include docker::install

    # Delete
    include docker::delete
```


### 3. Known backgrounds and issues
Not found yet


### 4. Used documentation
Install: https://docs.docker.com/install/linux/docker-ce/ubuntu/#uninstall-docker-ce

User remap: https://docs.docker.com/engine/security/userns-remap/#disable-namespace-remapping-for-a-container

wetopi/rbd: https://github.com/wetopi/docker-volume-rbd
            https://github.com/wetopi/docker-volume-rbd/issues/3

OpenStack/Docker persistent storage article: http://www.admin-magazine.com/Articles/Persistent-volumes-for-Docker-containers/(offset)/9
