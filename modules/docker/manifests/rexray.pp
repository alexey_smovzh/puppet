class docker::rexray inherits docker {

  # Get data from Hiera
  $services = ['rexray']
  $packages = ['rexray']
  $home = '/opt/rexray'
  $configs = '/etc/rexray'
  $package = 'rexray_0.11.3-1_amd64.deb'
  $url = 'https://dl.bintray.com/rexray/rexray/stable/0.11.3/rexray_0.11.3-1_amd64.deb'

  # Hiera values
  $ceph = lookup('ceph', Hash)
  # Extract variables
  $cluster = $ceph['cluster_name']
  $pools = $ceph['pools']

}

# sudo docker plugin install rexray/rbd RBD_DEFAULTPOOL=rbd RBD_CEPHARGS="--id admin --cluster ceph"
#

class docker::rexray::install inherits docker::rexray {


  # create rax-ray home dir
  file { $home:
    ensure => 'directory',
    recurse => true,
    recurselimit => 1,
  }

  # download package
  exec { 'download_rexray':
    command => "/usr/bin/wget ${url}",
    cwd => $home,
    creates => "${home}/${package}",
    require => File[$home],
  }

  # install
  exec { 'install_rexray':
    command => "/usr/bin/dpkg -i ${home}/${package}",
    user => 'root',
    group => 'root',
    require => Exec['download_rexray'],
    unless => "/usr/bin/dpkg -l | /bin/grep rexray",
  }

  # create config
  file { '/etc/rexray/config.yml':
    ensure => present,
    content => template('docker/rexray.yml.erb'),
    require => Exec['install_rexray'],
    notify => Service[$services]
  }

  # ensure service is running and enabled
  service { $services:
    ensure => running,
    enable => true,
    require => File['/etc/rexray/config.yml'],
  }
}



class docker::rexray::delete inherits docker::rexray {

  # stop and delete services
  service { $services:
    ensure => stopped,
    enable => false,
  }

  # delete rax-ray home
  file { [$home, $configs]:
    ensure => absent,
    purge => true,
    force => true,
    recurse => true,
  }

  # Purge
  package { $packages:
    provider => 'dpkg',
    ensure   => 'purged',
  }
}
