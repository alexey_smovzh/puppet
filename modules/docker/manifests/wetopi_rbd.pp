# https://github.com/wetopi/docker-volume-rbd
class docker::wetopi_rbd inherits docker {

  # need to directly provide plugin version
  $version = '1.0.1'

  # Hiera values
  $ceph = lookup('ceph', Hash)
  # Extract variables
  $cluster = $ceph['cluster_name']

  $client = 'client.dockerrbd'
  $keyring = "/etc/ceph/${cluster}.${client}.keyring"
  $dockerswarm_kv = "docker/${cluster}/docker_rbd_keyring"

}


# sudo ceph auth get-or-create-key client.dockerswarm mon 'allow r' osd 'allow rw pool=docker_rbd' -o /etc/ceph/ceph.client.dockerswarm.keyring
#
# sudo cat /etc/ceph/ceph.client.dockerswarm.keyring
# [client.dockerswarm]
#	  key = AQBzpHJbRZ4AIBAASwmM+8JeclSpQ5FVPovfdg==
#	  caps mon = "allow rw"
#	  caps osd = "allow rwx pool=docker_rbd"
#
# sudo docker plugin install wetopi/rbd --alias wetopi/rbd --grant-all-permissions LOG_LEVEL=1 RBD_CONF_CLUSTER=ceph  RBD_CONF_KEYRING_USER=client.dockerswarm

class docker::wetopi_rbd::install inherits docker::wetopi_rbd {

  # generate key on initial ceph node and then propagate it via consul
  if $hostname == $initial_node {
    # generate ceph auth key for docker swarm
    exec { 'generate_ceph_dockerswarm_key':
      command => "/usr/bin/ceph auth get-or-create-key ${client} \
                                mgr 'allow r' \
                                mon 'allow rw' \
                                osd 'allow rwx pool=${pool}' \
                  && /usr/bin/ceph auth get ${client} -o ${keyring}",
      user => 'root',
      group => 'root',
      require => Class['ceph::install',
                       'docker::install'],
      creates => $keyring,
    }

    # put keyring to consul KV store
    exec { 'put_keyring_consul_kv':
      command => "/opt/consul/consul kv put ${dockerswarm_kv} $(/bin/cat ${keyring} | /usr/bin/base32 -w 0 -)",
      user => 'root',
      group => 'root',
      require => Exec['generate_ceph_dockerswarm_key'],
      unless => "/opt/consul/consul kv get ${dockerswarm_kv}",
    }

  } else {
    # get keyring from consul
    exec { 'save_ceph_dockerswarm_key':
      command => "/opt/consul/consul kv get ${dockerswarm_kv} | /usr/bin/base32 -d > ${keyring}",
      user => 'root',
      group => 'root',
      onlyif => "/opt/consul/consul kv get ${dockerswarm_kv}",
      unless => "/usr/bin/test -s ${keyring}",    # file not exist or size is zero
    }
  }

  # docker plugin install wetopi/rbd:1.0.1
  #                             --alias wetopi-rbd
  #                             --grant-all-permissions
  #                             LOG_LEVEL=1
  #                             RBD_CONF_CLUSTER=ceph
  #                             RBD_CONF_KEYRING_USER=client.admin
  #                             RBD_CONF_POOL="docker"
  #                             RBD_CONF_DEVICE_MAP_ROOT="/dev/rbd"
  # install and enable wetopi/rbd docker plugin
  exec { 'install_wetopi_rbd':
    command => "/usr/bin/docker plugin install wetopi/rbd:${version} \
                                                --alias wetopi-rbd \
                                                --grant-all-permissions \
                                                LOG_LEVEL=1 \
                                                RBD_CONF_CLUSTER=${cluster} \
                                                RBD_CONF_KEYRING_USER=${client} \
                                                RBD_CONF_POOL=\"${pool}\" \
                                                RBD_CONF_DEVICE_MAP_ROOT=\"/dev/rbd\"",
    user => 'root',
    group => 'root',
    require => Class['docker::install'],
    unless => '/usr/bin/docker plugin ls | /bin/grep wetopi-rbd',
  }

  # ensure plugin is enabled
  exec { 'enable_wetopi_rbd':
    command => "/usr/bin/docker plugin enable wetopi-rbd",
    user => 'root',
    group => 'root',
    require => Class['docker::install'],
    unless => "/usr/bin/docker plugin ls | /bin/grep 'wetopi-rbd*.*true'",
  }
}



class docker::wetopi_rbd::delete inherits docker::wetopi_rbd {

  # delete wetopi/rbd plugin
  exec { 'delete_wetopi_rbd':
    command => "/usr/bin/docker plugin disable wetopi-rbd &&
                /usr/bin/docker plugin rm wetopi-rbd",
    user => 'root',
    group => 'root',
    onlyif => '/usr/bin/docker plugin ls | /bin/grep wetopi-rbd',
  }

  # delete keyring
  file { $keyring:
    ensure => absent,
    purge => true,
    force => true,
  }
}
