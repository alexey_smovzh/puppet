class docker::delete inherits docker {


#  include docker::rexray::delete

  # stop and delete services
  service { $services:
    ensure => stopped,
    enable => false,
  }

  # delete images, containers, volumes, or customized configuration files
  file { '/var/lib/docker':
    ensure => absent,
    purge => true,
    force => true,
    recurse => true,
  }

  # Remove a packages and purge its config files
  package { $packages:
      ensure => 'purged',
  }

  # Remove already unneeded depencies and dowloaded apk
  apt::apt_clean { 'clean_docker': }

}
