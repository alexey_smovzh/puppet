#
# create rbd volume on ceph distributed storage
# this function use wetopi/rbd plugin driver
# usage example:
#
#       docker::utils::create_ceph_volume { 'gogs': size=> '1024' }
#
define docker::utils::create_ceph_volume (String $size) {

  # class name are equal to volume name
  exec { "create_volume_${name}_data":
    command => "/usr/bin/docker volume create \
                                          -d wetopi-rbd \
                                          -o size=${size} \
                                          ${name}_data",
    user => 'root',
    group => 'root',
    onlyif => "/usr/bin/docker plugin ls | /bin/grep 'wetopi-rbd*.*true'",
    unless => "/usr/bin/docker volume ls | /bin/grep ${name}_data",
  }
}

#
# create local volume in /var/lib/docker/volumes directory
# usage example:
#
#       docker::utils::create_local_volume { 'gogs': size=> '1024' }
#
define docker::utils::create_local_volume {

# define docker::utils::create_local_volume (String $size) {

# todo: temporarily removed beause on ext4 option size does not make sence
#       need to return if local volumes wil be created on ZFS pool
#                                          -o size=${size} \

  # class name are equal to volume name
  exec { "create_volume_${name}_data":
    command => "/usr/bin/docker volume create \
                                          -d local \
                                          ${name}_data",
    user => 'root',
    group => 'root',
    unless => "/usr/bin/docker volume ls | /bin/grep ${name}_${hostname}",
  }
}

# create attachable overlay network
# default swarm overlay network 'ingresss' do not allow connect
# containers not managed by the swarm
# https://docs.docker.com/network/overlay/#customize-the-default-ingress-network
# usage example:
#
#       docker::utils::create_overlay_network { 'mysql': network => '10.0.0.0/24' }
#
define docker::utils::create_overlay_network (String $network) {

  # class name are equal to volume name
  exec { "create_overlay_network_${name}":
    command => "/usr/bin/docker network create \
                                          -d overlay \
                                          --subnet=${network} \
                                          --attachable \
                                          ${name}",
    user => 'root',
    group => 'root',
    unless => "/usr/bin/docker network ls | /bin/grep ${name}",
  }
}

# delete overlay network
# usage example:
#
#       docker::utils::delete_overlay_network { 'mysql' }
#
define docker::utils::delete_overlay_network {

  # class name are equal to volume name
  exec { "delete_overlay_network_${name}":
    command => "/usr/bin/docker network rm ${name}",
    user => 'root',
    group => 'root',
    onlyif => "/usr/bin/docker network ls | /bin/grep ${name}",
  }
}


/*

class docker::utils::service inherits docker (
    String $service
) {

}


class docker::utils::run inherits docker (
    String $app
) {

}
*/
