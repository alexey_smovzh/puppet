class docker::install inherits docker {

  # check if it not already defined and install depencies
  $depencies.each|$dep| {
    if !defined(Package[$dep]) {
      package { $dep:
        ensure => installed,
        require => Class['ceph::install',
                         'consul::install',
                         'pacemaker::install'],
      }
    }
  }

  # install apt repository by key ID
  apt::apt_repository { 'docker':
    key => '0EBFCD88',
    file => '/etc/apt/sources.list.d/docker-ce.list',
    repository => "deb [arch=amd64] https://download.docker.com/linux/ubuntu ${::lsbdistcodename} stable",
  }

  # install packages
  package { $packages:
    ensure => installed,
    require => Apt::Apt_repository['docker'],
  }

  # create dockerfiles directory if not exist
  file { $dockerfiles:
    ensure => 'directory',
    owner => 'root',
    group => 'root',
    recurse => true,
    recurselimit => 1,
  }

  # ensure service is running and enabled
  service { $services:
    ensure => running,
    enable => true,
    require => Package[$packages],
  }

  if $hostname == $initial_node {
    # Enable swarm
    # and export worker and manager keys
    exec { 'init_swarm':
      command => "/usr/bin/docker swarm init --advertise-addr ${ipaddress}",
      user => 'root',
      group => 'root',
      require => Service[$services],
      unless => "/usr/bin/docker info | /bin/grep 'Swarm: active'",
    }

    exec { 'put_kv_tokens':
      command => "/opt/consul/consul kv put ${worker_token} $(/usr/bin/docker swarm join-token --quiet worker)
                  /opt/consul/consul kv put ${manager_token} $(/usr/bin/docker swarm join-token --quiet manager)",
      user => 'root',
      group => 'root',
      require => Exec['init_swarm'],
      unless => ["/opt/consul/consul kv get ${worker_token}",
                 "/opt/consul/consul kv get ${manager_token}"],
    }

  } else {

    $ip = resolv($initial_node)
    # Decide in what role this node joins the cluster
    if $nodes["${hostname}"]['manager'] == true {
      $token = $manager_token
    } elsif $nodes["${hostname}"]['worker'] == true {
      $token = $worker_token
    }

    # Join swarm with appropriate token
    exec { 'join_swarm':
      command => "/usr/bin/docker swarm join \
                  --token $(/opt/consul/consul kv get ${token}) \
                    ${ip}:2377",
      user => 'root',
      group => 'root',
      require => Service[$services],
      onlyif => "/opt/consul/consul kv get ${token}",
      unless => "/usr/bin/docker info | /bin/grep 'Swarm: active'",
    }
  }

  # include default admins to group docker
  default_admin::utils::add_group { 'docker': }

  # install ceph rbd plugin if enabled
  if $pool != undef {
    include docker::wetopi_rbd::install
  }
}
