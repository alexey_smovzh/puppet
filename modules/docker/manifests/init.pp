class docker {

  $depencies = ['apt-transport-https', 'ca-certificates', 'curl', 'software-properties-common']
  $packages = ['docker-ce']
  $services = ['docker']
  $dockerfiles = '/var/lib/docker/dockerfiles'

  # Get data from Hiera
  $docker = lookup('docker', Hash)
  $cluster = $docker['cluster_name']
  $initial_node = $docker['initial_node']
  $nodes = $docker['nodes']
  $pool = $docker['pool']

  # Path to Consul data
  $worker_token = "docker/${cluster}/worker_token"
  $manager_token = "docker/${cluster}/manager_token"

}
