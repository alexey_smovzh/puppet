### 0. Description
This module install HA Proxy load balancer and binds it to Floating IP
provided by Pacemaker module.
Also this module create clustercheck script and service to monitor
MariaDB galera cluster.
Thats why for installation this module require Pacemaker and MariaDB
be already installed and operational.

This module enables HAProxy statistics web interface. It can be accessed
via '<node_ip>:<stats_port>/haproxy_stats' defined in init.pp manifest.
For example http://192.168.122.200:9000/haproxy_stats


### 1. Tested environments
This module developed and tested on Ubuntu 18.04LTS only.


### 2. Usage
Describe cluster configuration in Hiera yaml file:

```
  # Load balancing to mariadb and openstack services
  haproxy:
    nodes: *nodes
```

Use:

```
  # Install
  include haproxy::install

  # Delete
  include haproxy::delete

```

In this module are enabled web interface for statistic monitoring.
'<node_ip>:<stats_port>/haproxy_stats' defined in init.pp manifest.
For example http://192.168.122.200:9000/haproxy_stats
User and password defined in init.pp manifest via $stats_user and
$stats_pass variables

```
  $stats_user = 'admin'
  $stats_pass = 'admin'
  $stats_port = '9000'
```


### 3. Known backgrounds and issues
not found yet


### 4. Used documentation

HA Proxy for openstack: https://docs.openstack.org/ha-guide/controller-ha-haproxy.html

Clustercheck: https://github.com/olafz/percona-clustercheck

systemd sockets: https://www.freedesktop.org/software/systemd/man/systemd.socket.html

Self signed SSL certificate https://www.digitalocean.com/community/tutorials/how-to-create-a-self-signed-ssl-certificate-for-apache-in-ubuntu-18-04
