class haproxy::install inherits haproxy {


  # Configure the kernel parameter to allow non-local IP binding.
  # This allows running HAProxy instances to bind to a floatip for failover.
  augeas { "allow_non_local_ip_binding_${hostname}":
      context => $context,
      changes => [ "set net.ipv4.ip_nonlocal_bind 1", ],
      onlyif => "get net.ipv4.ip_nonlocal_bind != 1",
  }

  # load kernel parameters
  exec { "apply_sysctl_${hostname}":
    command => "/sbin/sysctl -p",
    user => 'root',
    group => 'root',
    refreshonly => 'true',
    subscribe => Augeas["allow_non_local_ip_binding_${hostname}"],
  }

  # https://haproxy.debian.net/#?distribution=Ubuntu&release=bionic&version=1.8
  # https://blog.sleeplessbeastie.eu/2017/12/30/how-to-install-haproxy-1-8-on-ubuntu-16/
  # install packages
  package { $packages:
    ensure => installed,
    require => [ Class['pacemaker::install'],
                 Class['mariadb::install'],
                 Augeas["allow_non_local_ip_binding_${hostname}"], ]
  }

  # generate ssl
  # openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout mysitename.key -out mysitename.crt
  # sudo eyaml encrypt -l 'private_cert' -f mysitename.key \
  #                    --pkcs7-private-key=/etc/puppetlabs/puppet/keys/private_key.pkcs7.pem \
  #                    --pkcs7-public-key=/etc/puppetlabs/puppet/keys/public_key.pkcs7.pem
  file { "/etc/ssl/certs/${controller}.crt":
    ensure => present,
    owner => 'root',
    group => 'root',
    content => template('haproxy/controller.crt.cert'),
    require => Package[$packages],
  }

  # save private key
  file { "/etc/ssl/private/${controller}.key":
    ensure => present,
    owner => 'root',
    group => 'ssl-cert',
    mode => '640',
    content => $private_cert,
    require => Package[$packages],
  }

  # create pem file for haproxy
  exec { 'create_haproxy_pem':
    command => "/bin/cat /etc/ssl/certs/${controller}.crt /etc/ssl/private/${controller}.key \
              | /usr/bin/tee /etc/haproxy/${controller}.pem",
    user => 'root',
    group => 'root',
    require => [ File["/etc/ssl/certs/${controller}.crt"],
                 File["/etc/ssl/private/${controller}.key"], ],
    creates => "/etc/haproxy/${controller}.pem",
  }

  # create config
  file { "/etc/haproxy/haproxy.cfg":
    ensure => present,
    content => template('haproxy/haproxy.cfg.erb'),
    require => Exec['create_haproxy_pem'],
    notify => Service[$services],
  }

  # add HAProxy to the cluster and ensure the floatip can only run
  # on machines where HAProxy is active
  exec { 'add_haproxy_resource_pacemaker':
    command => "/usr/sbin/pcs resource create ${app} systemd:haproxy --clone; \
                /usr/sbin/pcs constraint order start ${floatip_name} then ${app}-clone kind=Optional; \
                /usr/sbin/pcs constraint colocation add ${app}-clone with ${floatip_name}; ",
    unless => "/usr/sbin/pcs resource show ${app}",
    require => File["/etc/haproxy/haproxy.cfg"],
  }

  # run only once on initial node
  # galera replicates to others
  if $hostname == $initial_node {
    # configure mariadb haproxy user
    $lock_user = '/var/lib/mysql/haproxy_user_created.lock'
    exec { 'add_haproxy_user_to_mariadb':
      command => "/usr/bin/mysql -u root \
                                 -p${root_pass} \
                                 -e \"GRANT PROCESS ON *.* TO '${m_user}'@'localhost' IDENTIFIED BY '${m_pass}'; \
                                      flush privileges;\" \
              && /usr/bin/touch ${lock_user}",
      user => 'root',
      group => 'root',
      require => Package[$packages],
      onlyif => "/bin/systemctl is-active mariadb | /bin/grep active",
      unless => "/usr/bin/test -f ${lock_user}",
    }
  }

  # copy clustercheck
  # project page: https://github.com/olafz/percona-clustercheck
  file { "/usr/bin/clustercheck":
    ensure => present,
    content => template('haproxy/clustercheck.sh.erb'),
    owner => 'root',
    group => 'root',
    mode => '755',
    require => Package[$packages],
  }

  # create clustercheck services
  # For each socket file, a matching service file must exist,
  # describing the service to start on incoming traffic on the socket
  file { "/lib/systemd/system/mysqlchk@.service":
    ensure => present,
    content => template('haproxy/mysqlchk.service.erb'),
    owner => 'root',
    group => 'root',
    require => File["/usr/bin/clustercheck"],
    notify => Service[$services],
  }

  file { "/lib/systemd/system/mysqlchk.socket":
    ensure => present,
    content => template('haproxy/mysqlchk.socket.erb'),
    owner => 'root',
    group => 'root',
    require => File["/usr/bin/clustercheck"],
    notify => Service[$services],
  }

  service { $services:
    ensure => running,
    enable => true,
    require => [Exec['add_haproxy_resource_pacemaker'],
                File['/lib/systemd/system/mysqlchk@.service'],
                File['/lib/systemd/system/mysqlchk.socket'], ]

  }
}
