class haproxy {

  $packages = ['haproxy']
  $services = ['haproxy', 'mysqlchk.socket']

  # Internal parameters
  $config = '/etc/sysctl.conf'
  $context = "/files/${config}"
  $home = '/var/lib/haproxy'
  $user = 'haproxy'
  $max_connections = '4000'
  $app = $name                        # for resources naming

  # openstack services to proxying and its network port
  $applications = { 'glance_api_cluster' => '9292',
                    'glance_registry_cluster' => '9191',
                    'keystone_admin_public_internal_cluster' => '5000',
                    'nova_ec2_api_cluster' => '8773',
                    'nova_compute_api_cluster' => '8774',
                    'nova_metadata_api_cluster' => '8775',
                    'cinder_api_cluster' => '8776',
                    'ceilometer_api_cluster' => '8777',
                    'nova_vncproxy_cluster' => '6080',
                    'neutron_api_cluster' => '9696',
  }

  # Lookup data from Hiera
  $haproxy = lookup('haproxy', Hash)
  $nodes = $haproxy['nodes']
  $private_cert = $haproxy['private_cert']

  # user to check mariadb availability
  $m_user = 'checkgalera'
  $m_pass = $haproxy['db_user_pass']

  # user for statistics web interface
  $stats_user = 'admin'
  $stats_pass = $haproxy['stats_pass']
  $stats_port = '9000'

  # controller name assigned to floating ip provided by pacemaker
  $nodes_static = lookup('nodes_static', Hash)
  $controller = $nodes_static['floatip']['aliases']

  $pacemaker = lookup('pacemaker', Hash)
  $floatip = $pacemaker['floatip']
  $floatip_name = $pacemaker['floatip_name']

  $mariadb = lookup('mariadb', Hash)
  $initial_node = $mariadb['initial_node']
  $root_pass = $mariadb['password']

}
