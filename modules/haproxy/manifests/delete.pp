class haproxy::delete inherits haproxy {

  # stop and delete services
  service { $services:
    ensure => stopped,
    enable => false,
  }

  # delete images, containers, volumes, or customized configuration files
  file { [$repository,
          '/etc/haproxy',
          '/usr/bin/clustercheck',
          '/lib/systemd/system/mysqlchk@.service',
          '/lib/systemd/system/mysqlchk.socket',
          $home]:
    ensure => absent,
    purge => true,
    force => true,
    recurse => true,
  }

  # delete haproxy from pacemaker
  exec { 'delete_haproxy_resource_pacemaker':
    command => "/usr/sbin/pcs resource delete ${app}",
    onlyif => "/usr/sbin/pcs resource show ${app}",
    require => Service[$services]
  }

  # run only once on initial node
  # galera replicates to others
  if $hostname == $initial_node {
    # delete mariadb haproxy user
    $lock_user = '/var/lib/mysql/haproxy_user_created.lock'
    exec { 'delete_haproxy_user_from_mariadb':
      command => "/usr/bin/mysql -u root \
                                 -p${root_pass} \
                                 -e \"DROP USER '${m_user}'@'localhost'; \
                                      flush privileges;\" \
              && /bin/rm ${lock_user}",
      user => 'root',
      group => 'root',
      require => Service[$services]
      onlyif => ["/bin/systemctl is-active mariadb | /bin/grep active",
                 "/usr/bin/test -f ${lock_user}",]
    }
  }


  # Remove a packages and purge its config files
  package { $packages:
      ensure => 'purged',
  }

  # Remove already unneeded depencies and dowloaded apk
  apt::apt_clean { 'clean_haproxy': }

}
