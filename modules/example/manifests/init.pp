class example { }


# Create single hardcoded file
class example_simple inherits example {

  # create simple file
  file { '/tmp/test_file':
    ensure => present,
    owner => 'root',
    group => 'root',
    mode => '755',
    content => 'File content',
  }
}


# Create multiple files, names passed via variable
class example_multiple inherits example {

  # Hiera values
  $example = lookup('example', Hash)
  # Extract variables
  $files = $example['files']

  # create files
  $files.each |$file| {
    file { "${file}":
      ensure => present,
      owner => 'root',
      group => 'root',
      mode => '755',
    }
  }
}
