### 0. Description
This module add or remove administrator user accounts to/from managed systems.
User information it takes from default_admin section in common.yaml file.

Administrator accounts are added during OS installation by kickstart
scenario of pxe_boot_server module. But after OS initial install it
does not track future changes in administrators list. Purpose of this
module to do this jobs.

Also this module provide utils defined functions to simplify common tasks.
For example adding admin users to group during new software installation.
Comments below this functions provide usage description.


### 1. Tested environments
This module developed and tested on Ubuntu 16.04LTS, 18.04LTS only.


### 2. Usage
Add admin information to hiera/common.yaml:

```
# password generated with command: mkpasswd  -m sha-512 -S saltsalt -s
alex:
  enabled: true
  fullname: Alex
  password: $6$saltsalt$OpvVlYPY6YYXhosOAmr.a7PTpqaXv75xsI0bYfRKG3FGAOBgD32Mz/WXe.YKIKWhBVhh4DeNh281JyPVIBlKD0
```

To disable user account set value false to enabled key.
Do not delete user information from yaml file, just change this key value.

```
# password generated with command: mkpasswd  -m sha-512 -S saltsalt -s
alex:
  enabled: false
  fullname: Alex
  password: $6$saltsalt$OpvVlYPY6YYXhosOAmr.a7PTpqaXv75xsI0bYfRKG3FGAOBgD32Mz/WXe.YKIKWhBVhh4DeNh281JyPVIBlKD0

```


### 3. Known backgrounds and issues
Not found any yet


### 4. Used documentation
