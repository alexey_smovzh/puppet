#
# add default admins to privided as class name group
# this function must be called after package installation i.e. group creation
# usage example:
#
#       default_admin::utils::add_group { 'docker': }
#
define default_admin::utils::add_group {

  # Get data from Hiera
  $admins = lookup('default_admin', Hash)

  # include default admins to group docker
  $admins.each|$admin, $value| {
    if $value['enabled'] == true {
      # class name are equal to group name
      exec { "add_${admin}_to_group_${name}":
        command => "/usr/sbin/usermod -G ${name} -a ${admin}",
        user => 'root',
        group => 'root',
        unless => "/usr/bin/groups ${admin} | /bin/grep ${name}"
      }
    }
  }
}
