class default_admin {

  # Get data from Hiera
  $admins = lookup('default_admin', Hash)

  # if admin enabled add it to system
  # otherwise remove
  $admins.each|$admin, $value| {
    if $value['enabled'] == true {
      $state = 'present'
    } else {
      $state = 'absent'
    }

    user { $admin:
      ensure => $state,
      comment => $value['fullname'],
      home => "/home/${admin}",
      managehome => true,
      password => $value['password_crypted'],
    }
  }
}
