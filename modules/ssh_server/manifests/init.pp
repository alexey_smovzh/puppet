class ssh_server {

	# install openssh-server
	package { 'openssh-server':
		ensure => installed,
	}

	# ensure service is running and enabled
	service { 'ssh':
		ensure => running,
		enable => true,
		require => Package['openssh-server'],
	}
}
