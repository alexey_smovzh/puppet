This module provide installation of openssh server.
No any configuration changes from defaults are made.
Keys are generated during package installation.
No code for purging service are provided in module because it does not needed,
ssh server will be not needed anymore only if server itself will be decommissioned.
