### 0. Description
This module provide puppet functions to deal with Ubuntu apt command.
This module are useful to create apt repository "by hand" by installing key
and creating repository file. And for installing apt repository from deb package.
This module also has command to 'autoclean' and 'autoremove' unneeded packages
after deletion main module service, so it useful in purge sections of modules manifests.


### 1. Tested environments
This module developed and tested on Ubuntu 16.04LTS only.


### 2. Usage
Create repository example:

```
# Key by http link
apt::apt_repository { 'elasticsearch':
  key => 'https://artifacts.elastic.co/GPG-KEY-elasticsearch',
  file => '/etc/apt/sources.list.d/elastic-5.x.list',
  repository => 'deb https://artifacts.elastic.co/packages/5.x/apt stable main',
}

# Key by ID
apt::apt_repository { 'elasticsearch':
  key => 'AA9540512',
  file => '/etc/apt/sources.list.d/elastic-5.x.list',
  repository => 'deb https://artifacts.elastic.co/packages/5.x/apt stable main',
}

```

or without GPG key:

```
apt::apt_repository { 'mongodb':
  file => '/etc/apt/sources.list.d/mongodb-org-3.6.list',
  repository => 'deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.6 multiverse',
}
```

Unparameterized example:


```
apt::apt_clean { 'clean': }
```


In require parameter. Make attention to first capital letters in defines name:

```
require => Apt::Apt_repository['elasticsearch'],
```


### 3. Known backgrounds and issues
- There are no code for repository deletion


### 4. Used documentation
