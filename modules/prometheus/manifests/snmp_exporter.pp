class prometheus::snmp_exporter::install (
	String $version
) {

	 $home = '/opt/prometheus_snmp_exporter'

 		# create service user
     group { 'snmp_exporter' :
       ensure => 'present',
     }

 		user { 'snmp_exporter':
  		ensure => 'present',
      groups => 'snmp_exporter',
 			shell => '/bin/false',
		  home => $home,
 			comment => 'Prometheus SNMP Exporter',
 			password => '*',
			managehome => true,
 		}

		file { $home:
			require => User['snmp_exporter'],
		}

 		# download snmp_exporter binary
 		exec { 'download_snmp_exporter':
 			command => "/usr/bin/wget https://github.com/prometheus/snmp_exporter/releases/download/v0.11.0/${version}.tar.gz",
 			cwd => $home,
 			creates => "${home}/${version}.tar.gz",
      require => File[$home],
 		}

 		file { "${home}/${version}.tar.gz":
 			require => Exec['download_snmp_exporter'],
 		}

 		# unpack
 		exec { 'unpack_snmp_exporter':
 			command => "/bin/tar xf ${version}.tar.gz --strip 1",
 			cwd => $home,
 			require => File["${home}/${version}.tar.gz"],
			creates => "${home}/snmp_exporter"				# prevent to execute on every manifest run
 		}

 		# create service
 		file { '/lib/systemd/system/snmp_exporter.service':
 			ensure => present,
 			content => template('prometheus/snmp_exporter.service.erb'),
			notify => Service['snmp_exporter'],		# because parameters are passed through service exec command
 		}

     # create symlinks
     file { '/usr/bin/snmp_exporter':
       ensure => 'link',
       target => "${home}/snmp_exporter",
       require => Exec['unpack_snmp_exporter'],
     }

 		# ensure service is running and enabled
 		service { 'snmp_exporter':
 			ensure => running,
 			enable => true,
 			require => File['/lib/systemd/system/snmp_exporter.service'],
 		}
}


class prometheus::snmp_exporter::delete {

 		# stop and delete node exporter service
     service { 'snmp_exporter':
				ensure => stopped,
				enable => false,
			}

			# delete users
			user { 'snmp_exporter' :
				ensure => absent,
			}

     # delete service file and home directory
			file { [$home, '/lib/systemd/system/snmp_exporter.service'] :
				ensure => absent,
				recurse => true,
				purge => true,
				force => true,
			}

     # delete symlinks
     file { '/usr/bin/snmp_exporter' :
       ensure => absent,
     }

     # Remove already unneeded depencies and dowloaded apk
     apt::apt_clean { 'clean': }
}
