#
# Parameter $install used to dermine which branch of code to execute
# which installs app or that that purge it
#
class prometheus::prometheus_main (Boolean $install = true, String $version) {

   $prometheus_dir = ['/opt/prometheus', '/etc/prometheus', '/var/opt/prometheus']


	 if $install == true {
    		# create service user
        group { 'prometheus' :
          ensure => 'present',
        }

    		user { 'prometheus':
     			ensure => 'present',
          groups => 'prometheus',
    			home => '/var/opt/prometheus',
    			shell => '/bin/false',
    			comment => 'Prometheus',
    			password => '*',
    			managehome => true,
    		}

    		# create prometheus main and config directory
    		file { $prometheus_dir:
    			ensure => 'directory',
    			owner => 'prometheus',
    			group => 'prometheus',
    			recurse => true,
    			recurselimit => 1,
    		}

    		# download prometheus binary
    		exec { 'download_prometheus':
    			command => "/usr/bin/wget https://github.com/prometheus/prometheus/releases/download/v2.3.1/${version}.tar.gz",
    			cwd => '/opt/prometheus',
    			creates => "/opt/prometheus/${version}.tar.gz",
          require => File[$prometheus_dir],
    		}

    		file { "/opt/prometheus/${version}.tar.gz":
    			require => Exec['download_prometheus'],
    		}

    		# unpack
    		exec { 'unpack_prometheus':
    			command => "/bin/tar xf ${version}.tar.gz --strip 1",
    			cwd => '/opt/prometheus',
    			require => File["/opt/prometheus/${version}.tar.gz"],
    		}

    		# create service
    		file { '/lib/systemd/system/prometheus.service':
    			ensure => present,
    			content => template('prometheus/prometheus.service.erb'),
    		}

        # create symlinks
        file { '/usr/bin/prometheus':
          ensure => 'link',
          target => '/opt/prometheus/prometheus',
          require => Exec['unpack_prometheus'],
        }

        file { '/usr/bin/promtool':
          ensure => 'link',
          target => '/opt/prometheus/promtool',
          require => Exec['unpack_prometheus'],
        }

    		# create config
    		file { '/etc/prometheus/prometheus.yml':
    			ensure => present,
    			content => template('prometheus/prometheus.yml.erb'),
    			notify => Service['prometheus'],
          require => File[$prometheus_dir],
    		}

    		# ensure service is running and enabled
    		service { 'prometheus':
    			ensure => running,
    			enable => true,
    			require => File['/lib/systemd/system/prometheus.service'],
    		}

    } else {

    		# stop and delete prometheus service
        service { 'prometheus':
					ensure => stopped,
					enable => false,
				}

				# delete users
				user { 'prometheus' :
					ensure => absent,
				}

        # delete service file
				file { '/lib/systemd/system/prometheus.service':
					ensure => absent,
					purge => true,
					force => true,
				}

        # delete symlinks
        file { ['/usr/bin/prometheus', '/usr/bin/promtool']:
          ensure => absent,
        }

        # delete prometheus main and config directory
				file { $prometheus_dir:
					ensure => absent,
					recurse => true,
					purge => true,
					force => true,
				}

        # Remove already unneeded depencies and dowloaded apk
        apt::apt_clean { 'clean': }
    }
}
