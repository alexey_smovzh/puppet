### 0. Description
This module installs Prometheus to collect infrastructure health and operation
metrics and store it in time-series database.
An addition it install snmp exporter to gets snmp metrics from network devices.
Also this module include code to install Prometheus node exporter. Node exporter
must be installed on all servers which we want to monitor by Prometheus.


### 1. Tested environments
This module developed and tested on Ubuntu 16.04LTS only.


### 2. Usage
To install Prometheus use:

```
  class { 'profile::prometheus': install => true }
```

To purge Prometheus:  

```
  class { 'profile::prometheus': install => false }
```

Tune storage.tsdb.retention parameter in templates\prometheus.service.erb.
This parameter determinate period after old data will deleted (default 15 days).

Access to web interface http://<your_host>:9090 i.e. http://prometheus:9090

SNMP exporter.
After installation generate snmp.yml config file following this instructions:
https://github.com/prometheus/snmp_exporter/tree/master/generator
and add SNMP exporter scrab section to prometheus.yml configuration file:

```
scrape_configs:
  - job_name: 'snmp'
    static_configs:
      - targets:
        - 192.168.1.2  # SNMP device.
    metrics_path: /snmp
    params:
      module: [if_mib]
    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
        replacement: 127.0.0.1:9116  # The SNMP exporter's real hostname:port.
```

To check if metrics are collected access web page http://<your_host>:9116 i.e.
http://prometheus:9116


Node exporter installed on all servers in mandatory fashion.
To install/delete it manually:

```
  # true - install / false - purge
  class { 'profile::prometheus::node_exporter': install => true }
```

Check metrics are exported http://<yor_node>:9100 i.e. http://prometheus:9100

To view available metrics and its default state use command:

```
$ node_exporter --help
```

To enable some metric pass it as parameter --collector.<name> in ExecStart
parameter in templates/node_exporter.service.erb
For example:

```
ExecStart=/usr/bin/node_exporter --collector.cpu --no-collector.wifi
```

After installing node exporter on some node. Do not forget to add it to
prometheus/templates/prometheus.yml.erb to "scrape_configs:" Node Exporter section.



### 3. Known backgrounds and issues
Not found any yet


### 4. Used documentation
Install prometheus on ubuntu 16.04LTS:
https://www.digitalocean.com/community/tutorials/how-to-install-prometheus-on-ubuntu-16-04

Getting started: https://prometheus.io/docs/prometheus/latest/getting_started/

SNMP exporter: https://github.com/prometheus/snmp_exporter

First steps with Prometheus: https://prometheus.io/docs/introduction/first_steps/
