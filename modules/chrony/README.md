### 0. Description
This module install Chrony for NTP time synchronization.
Before installation it disables NTP service if it was enabled.
NTP servers addresses are getted from common.yaml 'ntp:' section


### 1. Tested environments
This module developed and tested on Ubuntu 16.04LTS and 18.04LTS only.


### 2. Usage
Add ntp server addresses into common.yaml.
Do not forgot dash symbol '-' before server address, it indicates that record
is an array element.

```
  ntp:
    - 10.64.10.1
    - 10.64.10.2
```

Then to install or delete:

```
  # Install
  include chrony::install

  # Delete
  include chrony::delete
```


### 3. Known backgrounds and issues
Not found yet
todo: add 'allow 10.0.0.0/24' directive into config file with list of
      networks from where clients are allowed


### 4. Used documentation

https://chrony.tuxfamily.org/doc/3.3/chrony.conf.html
