class chrony::delete inherits chrony {

  # run service
  service { 'chrony':
    ensure => stopped,
    enable => false,
  }

  # create config
  file { '/etc/chrony/chrony.conf':
    ensure => absent,
    require => Service['chrony'],
  }

  # install
  package { $packages:
    ensure => 'purged',
    require => File['/etc/chrony/chrony.conf'],
  }

  # Remove already unneeded depencies and dowloaded apk
  apt::apt_clean { 'clean_chrony': }
}
