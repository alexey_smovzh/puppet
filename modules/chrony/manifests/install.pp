class chrony::install inherits chrony {

  # ensure NTP service are disabled and stopped
  service { 'ntp':
    ensure => stopped,
    enable => false,
  }

  # install
  package { $packages:
    ensure => 'installed',
    require => Service['ntp'],
  }

  # create config
  file { '/etc/chrony/chrony.conf':
    ensure => present,
    content => template('chrony/chrony.conf.erb'),
    require => Package[$packages],
    notify => Service['chrony'],
  }

  # run service
  service { 'chrony':
    ensure => running,
    enable => true,
    require => File['/etc/chrony/chrony.conf'],
  }

}
