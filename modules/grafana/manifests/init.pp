class grafana(Boolean $install = true) {

    $depencies = ['apt-transport-https']
    $packages = ['grafana']
    $services = ['grafana-server']


    if $install == true {

      # install depencies if they are not already installed
      $depencies.each |$dependency| {
          if ! defined(Package[$dependency]) {
            package { $dependency:
                ensure => installed,
            }
          }
      }

      # add elasticsearch repo
      apt::apt_repository { 'grafana':
        key => 'https://packagecloud.io/gpg.key',
        file => '/etc/apt/sources.list.d/grafana.list',
        repository => 'deb https://packagecloud.io/grafana/stable/debian/ stretch main',
      }

			# install packages
			package { $packages:
        require => Apt::Apt_repository['grafana'],
				ensure => installed,
			}

      # create Grafana config
      file { '/etc/grafana/grafana.ini':
          ensure => present,
          owner => root,
          group => grafana,
          content => template('grafana/grafana.ini.erb'),
          notify => Service['grafana-server'],
          require => Package['grafana'],
      }

      # ensure all services are enabled and running
			$services.each |$service| {
					service { $service :
	          	ensure => running,
	          	enable => true,
	          	require => Package[$packages],
	      	}
			}

    } else {
      # Stop and disable services
      service { $services :
          ensure => stopped,
          enable => false,
      }

      # Remove a packages and purge its config files
      package { [$packages, $depencies]:
          ensure => 'purged',
      }

      # Remove already unneeded depencies and dowloaded apk
      apt::apt_clean { 'clean': }

    }
}
