### 0. Description
This module installs Grafana for visualization data from
Elasticsearch (logs and netflow) and Prometheus (time series metrics).
Also Grafana provides alerting.


### 1. Tested environments
This module developed and tested on Ubuntu 16.04LTS only.


### 2. Usage
To install Grafana use:

```
  class { grafana: install => true }
```

To purge Grafana:  

```
  class { grafana: install => false }
```

Access to web interface http://<yor_host>:3000 i.e. http://grafana:3000
Default user/password: admin/admin


### 3. Known backgrounds and issues
Not found any yet


### 4. Used documentation
Install instruction for Debian/Ubuntu http://docs.grafana.org/installation/debian/
Integration with Elasticsearch https://logz.io/blog/grafana-elasticsearch/
