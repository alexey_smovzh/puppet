class rsyslog {

	# ensure rsyslog presented in system
	package { 'rsyslog':
		ensure => installed,
	}

	# create config
	file { '/etc/rsyslog.d/50-default.conf':
		ensure => present,
		content => template('rsyslog/50-default.conf.erb'),
		require => Package['rsyslog'],
		notify => Service['rsyslog'],						# restart service on config change
	}

	# ensure service is running and enabled
	service { 'rsyslog':
		ensure => running,
		enable => true,
		require => Package['rsyslog'],
	}
}
