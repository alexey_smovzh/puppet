This module ensures that package rsyslog are installed.
And configures it to send syslog messages to graylog server via UDP transport.

This package being tested on Ubuntu 16.04LTS only.

This package is default for all servers.

Before use:
1. Create Syslog UDP input in graylog inputs configuration package
2. Default port number for rsyslog is 514, but it forbidden for unprivileged
services. So for graylog usually are used port 5140
3. Check graylog hostname and port number in templates/50-default.conf.erb
