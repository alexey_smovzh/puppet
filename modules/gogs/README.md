### 0. Description
This module install Gogs GIT server with nice web interface.
In this module Gogs configured to use sqlite3 to store configurtion.
MySQL or Postgress are not configured because of low load to this service
in our environment, so they will be overkill.
You can create and run Gogs in docker container with dockerize option.


### 1. Tested environments
This module developed and tested on Ubuntu 16.04LTS only.


### 2. Usage
To install/delete on baremetal host or inside VM:

```
  # Install
  include gogs::install

  # Delete
  include gogs::delete
```

To create and run docker container, apply following to docker host:

```
  include gogs::dockerize
```

Access to web interface http://<host_name>:3000 i.e. http://puppet:3000
Register new user, log in and create repository.


### 3. Known backgrounds and issues
Check Gogs version and archive URL before module use in 'init.pp' file.
It can be new version or url changed.


### 4. Used documentation
Gogs: https://gogs.io

Docker volumes: https://container-solutions.com/understanding-volumes-docker/
