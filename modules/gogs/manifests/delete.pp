class gogs::delete inherits gogs {

		# stop and delete gogs service
    service { $service:
			ensure => stopped,
			enable => false,
		}

		# delete user
		user { $user:
			ensure => 'absent',
		}

    # delete service file
		file { '/lib/systemd/system/gogs.service':
			ensure => absent,
			purge => true,
			force => true,
		}

    # delete gogs main and working directory
		file { [$home, $directories]:
			ensure => absent,
			recurse => true,
			purge => true,
			force => true,
		}
}
