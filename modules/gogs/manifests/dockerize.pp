class gogs::dockerize (
  Boolean $swarm
) inherits gogs {

  # Folder to store dockerfiles
  $dockerfiles = '/var/lib/docker/dockerfiles'

  # container limits
  $cpu = '0.2'
  $ram = '134217728'            # --limit-memory=134217728 i.e. 128Mb
  $vol = '1024'                 # volume size in MB


  # create Dockerfile
  file { "${dockerfiles}/${image}.Dockerfile":
    ensure => present,
    content => template("${image}/${image}.docker.erb"),
    notify => Exec["delete_image_${image}"],
    require => File[$dockerfiles],
  }

  # create configs
  $configs.each |$config| {
    file { "${dockerfiles}/${config}":
      ensure => present,
      content => template("${image}/${config}.erb"),
      notify => Exec["delete_image_${image}"],
      require => File[$dockerfiles],
    }
  }

  # delete image if dockerfile or config are changed
  exec { "delete_image_${image}":
    command => "/usr/bin/docker image rm ${image} --force",
    cwd => $dockerfiles,
    user => 'root',
    group => 'root',
    refreshonly => true,
    require => File["${dockerfiles}/${image}.Dockerfile"],
    onlyif => "/usr/bin/docker image ls | /bin/grep ${image}",
  }

  # build image
  exec { "build_image_${image}":
    command => "/usr/bin/docker build -f ${image}.Dockerfile --no-cache=true -t ${image} .",
    cwd => $dockerfiles,
    user => 'root',
    group => 'root',
    timeout => 1200,             # max time for process execution in seconds, over it process will be killed
    require => File["${dockerfiles}/${image}.Dockerfile"],
    unless => "/usr/bin/docker image ls | /bin/grep ${image}",
  }

  # Container run command in normal docker and docker in swarm mode are different
  if $swarm == true {
    $run = "/usr/bin/docker service create \
                                      --detach \
                                      --name ${image} \
                                      --mount type=volume,source=${image}_data,destination=${home} \
                                      --publish 3000:3000 --publish 9418:9418 \
                                      --limit-cpu=${cpu} \
                                      --limit-memory=${ram} \
                                      --restart-condition on-failure \
                                      ${image}"
    $check = "/usr/bin/docker service ls | /bin/grep ${image}"
    $only = ["/usr/bin/docker volume ls | /bin/grep ${image}_data",
             "/usr/bin/docker info | /bin/grep 'Swarm: active'"]

  # Standalone container run
  # use local volume
  } else {
    $run = "/usr/bin/docker run -itd \
                                -v ${image}_data:${home} \
                                -p 3000:3000 -p 9418:9418 \
                                --restart unless-stopped \
                                ${image}"
    $check = "/usr/bin/docker ps -a | /bin/grep ${image}"
    $only = "/usr/bin/docker volume ls | /bin/grep ${image}_data"
  }

  # create volume
  docker::utils::create_ceph_volume { 'gogs': size => $vol }

  # Run
  exec { "run_image_${image}":
    command => $run,
    cwd => $dockerfiles,
    user => 'root',
    group => 'root',
    onlyif => $only,
    require => Exec["build_image_${image}"],
    unless => $check,
  }
}
