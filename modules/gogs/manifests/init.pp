class gogs {

	$version = 'gogs_0.11.53_linux_amd64.tar.gz'
	$url = "https://dl.gogs.io/0.11.53/${version}"

	$user = 'gogs-git'
	$home = '/var/lib/gogs'
	$application = '/opt/gogs'
	$directories = ['/opt/gogs',
									'/opt/gogs/data',
									'/opt/gogs/custom',
									'/opt/gogs/custom/conf']

  $service = $name
	$image = $name
  $configs = ['app.ini']

}
