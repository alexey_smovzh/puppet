class gogs::install inherits gogs {

	# create service user
	user { $user:
		ensure => 'present',
		home => $home,
		shell => '/bin/false',
		comment => 'Gogs',
		password => '*',
		managehome => true,
	}

	# create gogs main and config directory
	file { $directories:
		ensure => 'directory',
		owner => $user,
		group => $user,
		recurse => true,
		recurselimit => 1,
	}

	# download gogs binary
	exec { 'download_gogs':
		command => "/usr/bin/wget -P ${application} ${url}",
		cwd => $application,
		creates => "${application}/${version}",
	}

	file { "${application}/${version}":
		require => Exec['download_gogs'];
	}

	# unpack
	exec { 'unpack gogs':
		command => "/bin/tar xf ${version} --strip 1",
		cwd => $application,
		require => File["${application}/${version}"],
		creates => "${application}/${service}",
	}

	# create service
	file { '/lib/systemd/system/gogs.service':
		ensure => present,
		content => template('gogs/gogs.service.erb'),
	}

	# create configs
	$configs.each |$config| {
		file { "/opt/gogs/custom/conf/${config}":
			ensure => present,
			content => template("${service}/${config}.erb"),
			notify => Service[$service],
		}
	}

	# ensure service is running and enabled
	service { $service:
		ensure => running,
		enable => true,
		require => File['/lib/systemd/system/gogs.service'],
	}
}
