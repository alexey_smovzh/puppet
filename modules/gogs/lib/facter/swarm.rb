# Custom fact
# Return docker swarm cluster mode state
Facter.add(:swarm) do
  setcode do
    Facter::Core::Execution.exec("/usr/bin/docker info | /bin/grep 'Swarm:' | /usr/bin/awk '{ print $2 }'")
  end
end
