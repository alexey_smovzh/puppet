class openstack {

  $depencies = [ 'software-properties-common',
                 'python-pymysql',
                 'python-openstackclient', ]


  # controller name assigned to floating ip provided by pacemaker
  $nodes = lookup('nodes_static', Hash)
  $controller = $nodes['floatip']['aliases']

  $mariadb = lookup('mariadb', Hash)
  $db_root_pass = $mariadb['password']

  $pacemaker = lookup('pacemaker', Hash)
  $floatip = $pacemaker['floatip']


  # Rocky cloud archive only supported on Bionic
  # Queens cloud archive only supported on Xenial (Bionic includes Queens by default)
  $release = $::lsbdistcodename ? { 'bionic' => 'rocky', 'xenial' => 'queens' }

  $home = '/opt/openstack'

  $openstack = lookup('openstack', Hash)
  $services = $openstack['services']
  $initial_node = $openstack['initial_node']
  $region = $openstack['region']

  $keystone = $services['keystone']
  $keystone_admin_user = $keystone['admin_user']
  $keystone_admin_pass = $keystone['admin_pass']
  $keystone_db = 'keystone'
  $keystone_db_user = 'keystone'
  $keystone_db_pass = $keystone['db_pass']



}
