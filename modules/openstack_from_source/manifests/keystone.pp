class openstack::keystone::install inherits openstack::install {


    # create keystone database
    openstack::utils::create_database { 'create_keystone_db':
      dbroot => $db_root_pass,
      dbname => $keystone_db,
      dbuser => $keystone_db_user,
      dbpass => $keystone_db_pass,
    }

    # create keystone user, group and config directory
    openstack::utils::create_credentials { 'create_keystone_credentials':
      service => 'keystone',
    }

    # create config
    file { '/etc/keystone/keystone.conf':
      ensure => present,
      owner => 'keystone',
      group => 'keystone',
      content => template('openstack/keystone.conf.erb'),
      require => [ Openstack::Utils::Create_database['create_keystone_db'],
                   Openstack::Utils::Create_credentials['create_keystone_credentials'],
                   Class['openstack::apache2::install'] ],
    }

    # populate the Identity service database
    exec { "populate_database_${keystone_db}":
      command => '/bin/sh -c "keystone-manage db_sync" keystone',
      user => 'root',
      group => 'root',
      provider => 'shell',
      require => File['/etc/keystone/keystone.conf'],
      onlyif => "/bin/hostname | /bin/grep ${initial_node}",        # run only on initial node, galera cluster replicates to others
      unless => "/usr/bin/mysql -u ${keystone_db_user} \
                                -p${keystone_db_pass} \
                                -e \"SHOW TABLES;\" \
                                ${keystone_db} | /bin/grep region",
    }

    # Initialize Fernet key repositories
    exec { 'initialize_key_repositories':
      command => "/usr/local/bin/keystone-manage fernet_setup --keystone-user ${keystone_admin_user} --keystone-group keystone \
               && /usr/local/bin/keystone-manage credential_setup --keystone-user ${keystone_admin_user} --keystone-group keystone",
      provider => 'shell',
      require => Exec["populate_database_${keystone_db}"],
      creates => [ '/etc/keystone/fernet-keys/0',
                   '/etc/keystone/fernet-keys/1', ],
    }

    # bootstrap the Identity service
    exec { 'bootstrap_keystone':
      command => "/usr/local/bin/keystone-manage bootstrap \
                                            --bootstrap-password ${keystone_admin_pass} \
                                            --bootstrap-admin-url http://${controller}:5000/v3/ \
                                            --bootstrap-internal-url http://${controller}:5000/v3/ \
                                            --bootstrap-public-url http://${controller}:5000/v3/ \
                                            --bootstrap-region-id ${region}",
      require => Exec['initialize_key_repositories'],
      unless => "/usr/bin/mysql -u ${keystone_db_user} \
                                 -p${keystone_db_pass} \
                                 -e \"SELECT id FROM region;\" \
                                 ${keystone_db} | /bin/grep ${region} ",
    }

    # create pacemaker ocf file for keystone monitoring
    file { '/usr/lib/ocf/resource.d/openstack/keystone':
      ensure => present,
      owner => 'keystone',
      group => 'keystone',
      content => template('openstack/keystone.ocf'),
      require => [ Class['pacemaker::install'],
                   Exec['bootstrap_keystone'], ]
    }

    # create pacemaker keystone resource
    exec { 'add_keystone_resource_pacemaker':
      command => "/usr/sbin/pcs resource create openstack-keystone systemd:openstack-keystone --clone interleave=true ",
      unless => "/usr/sbin/pcs resource show openstack-keystone",
      require => File['/usr/lib/ocf/resource.d/openstack/keystone'],
    }

    # create keystone apache site config
    file { '/etc/apache2/sites-available/keystone.conf':
      ensure => present,
      owner => 'root',
      group => 'root',
      content => template('openstack/site_keystone.conf.erb'),
      require => Exec['bootstrap_keystone'],
    }

    # enable keystone site
    file { '/etc/apache2/sites-enabled/keystone.conf':
      ensure => 'link',
      target => '/etc/apache2/sites-available/keystone.conf',
      require => File['/etc/apache2/sites-available/keystone.conf'],
    }

    # todo: create credentials file from finalize installation section
    #       https://docs.openstack.org/keystone/rocky/install/keystone-install-ubuntu.html
    # https://docs.openstack.org/keystone/rocky/install/keystone-install-obs.html

}
