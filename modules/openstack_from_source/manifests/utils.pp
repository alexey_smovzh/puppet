# create database, user and grant priviledges
# @dbroot - password for root database user
# @dbname - database name
# @dbuser - new user name
# @dbpass - password for new user
#

define openstack::utils::create_database(String $dbroot, String $dbname, String $dbuser, String $dbpass) {

  exec { "create_database_${dbname}":
    command => "/usr/bin/mysql -u root \
                               -p${dbroot} \
                               -e \"CREATE DATABASE ${dbname}; \
                               GRANT ALL PRIVILEGES ON ${dbname}.* TO '${dbuser}'@'localhost' IDENTIFIED BY '${dbpass}'; \
                               GRANT ALL PRIVILEGES ON ${dbname}.* TO '${dbuser}'@'%' IDENTIFIED BY '${dbpass}'; \
                               FLUSH PRIVILEGES;\" ",
    user => 'root',
    group => 'root',
    require => Class['mariadb::install'],
    onlyif => "/bin/systemctl is-active mariadb | /bin/grep active",
    unless => "/usr/bin/mysql -u root \
                               -p${dbroot} \
                               -e \"SHOW DATABASES;\" | /bin/grep ${dbname}",
  }
}
