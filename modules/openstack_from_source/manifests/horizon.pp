class openstack::horizon::install inherits openstack::install {



  # create keystone user, group and config directory
  openstack::utils::create_credentials { 'create_horizon_credentials':
    service => 'openstack-dashboard',
  }

  # create config
  file { '/etc/openstack-dashboard/local_settings.py':
    ensure => present,
    owner => 'root',
    group => 'openstack-dashboard',
    content => template('openstack/local_settings.py.erb'),
    require => [ Openstack::Utils::Create_credentials['create_horizon_credentials'],
                 Class['openstack::apache2::install'] ],
  }

  # compile translation messages
  exec { 'compile_horizon_translation_messages':
    command => '/usr/bin/python manage.py compilemessages \
             && /usr/bin/python manage.py collectstatic \
             && /usr/bin/python manage.py compress',
    user => 'root',
    group => 'root',
    cwd => "${home}/horizon",
    require => File['/etc/openstack-dashboard/local_settings.py'],
#    unless => "/usr/bin/mysql -u ${keystone_db_user} \
#                               -p${keystone_db_pass} \
#                               -e \"SHOW TABLES;\" \
#                               ${keystone_db} | /bin/grep region",
  }

  # https://docs.openstack.org/horizon/latest/install/from-source.html
  # sudo /usr/share/openstack-dashboard/manage.py make_web_conf \
  #                                               --apache \
  #                                               --ssl \
  #                                               --sslkey=/etc/ssl/private/control.key \
  #                                               --sslcert=/etc/ssl/certs/control.crt \
  #                                               > /etc/apache2/sites-available/horizon.conf
  file { '/etc/apache2/sites-available/horizon.conf':
    ensure => present,
    owner => 'root',
    group => 'root',
    content => template('openstack/site_horizon.conf.erb'),
    require => Exec['compile_horizon_translation_messages'],
  }

  # enable dashboard site
  file { '/etc/apache2/sites-enabled/horizon.conf':
    ensure => link,
    target => '/etc/apache2/sites-available/horizon.conf',
    require => File['/etc/apache2/sites-available/horizon.conf'],
  }
}
