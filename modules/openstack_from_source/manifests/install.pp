class openstack::install inherits openstack {

  # check if it not already defined and install depencies
  $depencies.each|$dep| {
    if !defined(Package[$dep]) {
      package { $dep:
        ensure => 'installed',
      }
    }
  }

  # create openstack home
  file { $home:
    ensure => 'directory',
    owner => 'root',
    group => 'root',
    recurse => true,
    require => Class[ 'mariadb::install',
                      'pacemaker::install',
                      'haproxy::install',
                      'rabbitmq::install' ],
  }

  # create directory for pacemaker ocf files
  # used to monitor openstack components
  file { '/usr/lib/ocf/resource.d/openstack':
    ensure => 'directory',
    owner => 'root',
    group => 'root',
    recurse => true,
    recurselimit => 1,
    require => [ Class['pacemaker::install'] ]
  }


  # install apache
  include openstack::apache2::install


  # clone services git repository to openstack home
  # and install it
  $services.keys().each|$service| {

    # get commit hash sum
    $commit = $services[$service]['commit']

    # clone git repository and checkout to desired commit
    exec { "git_clone_${service}":
      command => "/usr/bin/git clone https://git.openstack.org/openstack/${service} -b stable/${release} \
               && /usr/bin/git -C '${home}/${service}' checkout -f ${commit}",
      user => 'root',
      group => 'root',
      cwd => $home,
      timeout => '1200',
      require => File[$home],
      notify => Exec["python_setup_${service}"],
      unless => "/usr/bin/test -f ${home}/${service}/setup.py",
    }

    # pull git repository if current commit sha1 sum not equal desired one
    # and checkout to commit hash sum
    exec { "git_fetch_${service}":
      command => "/usr/bin/git fetch --all \
               && /usr/bin/git checkout -f ${commit}",
      user => 'root',
      group => 'root',
      timeout => '1200',
      cwd => "$home/${service}",
      require => File[$home],
      notify => Exec["python_setup_${service}"],
      unless => "/usr/bin/git show --pretty=format:\"%H\" --no-patch | /bin/grep ${commit}",
    }

    # setup service
    # this exec runs only by notify action from git repository update
    exec { "python_setup_${service}":
      command => '/usr/bin/python setup.py install',
      user => 'root',
      group => 'root',
      cwd => "${home}/${service}",
      notify => Exec['reload_apache'],
      refreshonly => 'true',
    }

    # install service
    include "openstack::${service}::install"
  }

  # reload apache after all services are installed
  exec { 'reload_apache':
    command => '/usr/sbin/service apache2 restart',
    user => 'root',
    group => 'root',
    refreshonly => 'true',
  }
}
