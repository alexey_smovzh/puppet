class openstack::apache2::install inherits openstack::install {

  $apache = [ 'apache2',
              'libapache2-mod-wsgi',]


  # install apache2
  package { $apache:
    ensure => 'installed',
  }

  # create config
  file { '/etc/apache2/apache2.conf':
    ensure => present,
    content => template('openstack/apache2.conf.erb'),
    require => Package[$apache],
  }

  # delete default sites
  exec { 'delete_apache_default_sites':
    command => "/bin/rm -f /etc/apache2/sites-enabled/*",
    user => 'root',
    group => 'root',
    require => Package[$apache],
    onlyif => '/bin/ls /etc/apache2/sites-enabled/*default*',
  }

  # enable modules
  exec { "enable_apache_modules_${hostname}":
    command => "/usr/sbin/a2enmod wsgi \
             && /usr/sbin/a2enmod ssl ",
    user => 'root',
    group => 'root',
    require => Package[$apache],
    creates => [ '/etc/apache2/mods-enabled/ssl.load',
                 '/etc/apache2/mods-enabled/wsgi.load', ],
  }

  # generate ssl
  # openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout mysitename.key -out mysitename.crt
  file { '/etc/ssl/certs/control.crt':
    ensure => present,
    owner => 'root',
    group => 'root',
    content => template('openstack/control.crt.cert'),
    require => Exec["enable_apache_modules_${hostname}"],
  }

  # create config
  # todo: private key in free access its a bad idea!
  file { '/etc/ssl/private/control.key':
    ensure => present,
    owner => 'root',
    group => 'ssl-cert',
    mode => '640',
    content => template('openstack/control.key.cert'),
    require => Exec["enable_apache_modules_${hostname}"],
  }
}
