class disable_ipv6 {

  $config = '/etc/sysctl.d/70-disable-ipv6.conf'

  file { "${config}":
    ensure => present,
    owner => root,
    group => root,
    content => 'net.ipv6.conf.all.disable_ipv6 = 1',
    notify => Exec['apply_changes'],
  }

  exec { 'apply_changes':
     command => "/sbin/sysctl -p -f ${config}",
     unless => "/sbin/sysctl net.ipv6.conf.all.disable_ipv6 | grep 1",
     require => File["${config}"],
  }
}
