class ceph::mgr inherits ceph {
  $packages = ['ceph-mgr']
  $services = ["${cluster}-mgr@${hostname}"]

  $keyring = "/var/lib/ceph/mgr/ceph-${hostname}/keyring"
  $home = "/var/lib/ceph/mgr/${cluster}-${hostname}"
}



class ceph::mgr::install inherits ceph::mgr {

  # install packages
  package { $packages:
    ensure => installed,
    require => Apt::Apt_repository['ceph'],
  }

  # create mgr data dir
  file { $home:
    ensure => 'directory',
    owner => 'ceph',
    group => 'ceph',
    recurse => 'true',
    require => Package[$packages],
  }

  # create auth key
  # ceph auth get-or-create mgr.$name mon 'allow profile mgr' osd 'allow *' mds 'allow *'
  exec { "generate_mgr_${hostname}_auth_key":
    command => "/usr/bin/ceph auth get-or-create --cluster ${cluster} \
                              mgr.${hostname} \
                              mon 'allow profile mgr' \
                              mgr 'allow *' \
                              mds 'allow *' \
                              osd 'allow *' \
                && /usr/bin/ceph auth get mgr.${hostname} -o ${keyring} --cluster ${cluster}",
    user => 'root',
    group => 'root',
    creates => $keyring,
    onlyif => "/usr/bin/test -s ${admin_keyring}",         # require admin keyring to create new key
    require => File[$home],
  }

  # start service
  # ensure service is running and enabled
  service { $services:
    ensure => running,
    enable => true,
    require => Exec["generate_mgr_${hostname}_auth_key"],
  }
}


class ceph::mgr::delete inherits ceph::mgr {
  # remove monitor from cluster
  exec { 'stop_mgr':
    command => "/usr/bin/ceph mgr fail mgr.${hostname} --cluster ${cluster}",
    user => 'ceph',
    group => 'ceph',
  }

  # stop and delete services
  service { $services:
    ensure => stopped,
    enable => false,
    require => Exec['stop_mgr'],
  }

  # remove monitor dir
  file { $home:
    ensure => absent,
    recurse => true,
    purge => true,
    force => true,
    require => Service[$services],
  }

  # Remove a packages and purge its config files
  package { $packages:
      ensure => 'purged',
      require => File[$home],
  }
}
