class ceph::install inherits ceph {

  # install depencies
  package { $depencies:
    ensure => installed,
    require => Class['consul::install']
  }

  # install ceph apt repository
  apt::apt_repository { 'ceph':
    key => 'https://git.ceph.com/release.asc',
    file => '/etc/apt/sources.list.d/ceph.list',
    repository => "deb https://download.ceph.com/debian-${release}/ ${::lsbdistcodename} main",
  }

  # install common for all roles packages
  package { $packages:
    ensure => installed,
    require => [Apt::Apt_repository['ceph'],
                Package[$depencies]]
  }

  # create config
  file { "/etc/ceph/ceph.conf":
    ensure => present,
    content => template('ceph/ceph.conf.erb'),
    require => Package[$packages],
    notify => Service[$services]
  }

  # ensure service is running and enabled
  service { $services:
    ensure => running,
    enable => true,
    require => File['/etc/ceph/ceph.conf'],
  }

  # include default admins to group ceph
  default_admin::utils::add_group { 'ceph': }

  # If node is monitor node
  if $nodes["${hostname}"]['monitor'] == true {
    include ceph::monitor::install
    include ceph::mgr::install
  }

  # If node is mds node
  if $nodes["${hostname}"]['mds'] == true {
    include ceph::mds::install
    include ceph::cephfs::install
  }

  # If node is OSD node
  if $nodes["${hostname}"]['osd'] != undef {
    include ceph::osd::install
    include ceph::pool::install
  }
}
