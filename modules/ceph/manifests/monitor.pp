class ceph::monitor inherits ceph {
  $packages = ['ceph-mon']
  $services = ["${cluster}-mon@${hostname}"]

  $root = "/var/lib/ceph"
  $home = "${root}/mon/${cluster}-${hostname}"
  $tmp = "/var/lib/ceph/tmp"

  $mon_keyring = "${tmp}/keyring"
  $mon_map = "${tmp}/monmap"
  $mon_tmp = "${tmp}/ceph.mon.keyring"
  $mon_kv = "ceph/${cluster}/monitor_keyring"
  $admin_tmp = "${tmp}/ceph.client.admin.keyring"
  $admin_kv = "ceph/${cluster}/admin_keyring"
  $osd_tmp = "${tmp}/bootstrap-osd.ceph.keyring"
  $osd_kv = "ceph/${cluster}/osd_keyring"

}

class ceph::monitor::install inherits ceph::monitor {

  # install packages
  package { $packages:
    ensure => installed,
    require => Apt::Apt_repository['ceph'],
  }

  # enshure tmp folder exist
  file { $tmp:
    ensure => 'directory',
    owner => 'ceph',
    group => 'ceph',
    require => Package[$packages],
  }

  # hack to fix ceph directory permission issue
  # it created with root user as owner not ceph
  file { $root:
    ensure => 'directory',
    owner => 'ceph',
    group => 'ceph',
    recurse => 'true',
    require => Package[$packages],
  }

  # monitor secret key
  # ceph-authtool --create-keyring /tmp/ceph.mon.keyring --gen-key -n mon. --cap mon 'allow *'
  #
  # administarator keyring
  # ceph-authtool --create-keyring /tmp/ceph.client.admin.keyring --gen-key -n client.admin --set-uid=0 --cap mon 'allow *' --cap osd 'allow *' --cap mds 'allow *' --cap mgr 'allow *'
  #
  # bootstrap osd keyring
  # ceph-authtool --create-keyring /tmp/bootstrap-osd.ceph.keyring --gen-key -n client.bootstrap-osd --cap mon 'profile bootstrap-osd'
  #
  # import keys
  # ceph-authtool /tmp/ceph.mon.keyring --import-keyring /tmp/ceph.client.admin.keyring
  # ceph-authtool /tmp/ceph.mon.keyring --import-keyring /tmp/bootstrap-osd.ceph.keyring
  #

  if $hostname == $initial_node {
    # create administrator keyring
    exec { "generate_admin_key":
      command => "/usr/bin/ceph-authtool \
                                --create-keyring ${admin_tmp} \
                                --gen-key \
                                -n client.admin \
                                --set-uid=0 \
                                --cap mon 'allow *' \
                                --cap osd 'allow *' \
                                --cap mds 'allow *' \
                                --cap mgr 'allow *' \
              && /opt/consul/consul kv put ${admin_kv} $(/bin/cat ${admin_tmp} | /usr/bin/base32 -w 0 -)",
      user => 'ceph',
      group => 'ceph',
      require => Package[$packages],
      unless => "/opt/consul/consul kv get ${admin_kv}",
    }

    # create osd keyring
    exec { "generate_osd_key":
      command => "/usr/bin/ceph-authtool \
                                --create-keyring ${osd_tmp} \
                                --gen-key \
                                -n client.bootstrap-osd \
                                --cap mon 'profile bootstrap-osd' \
              && /opt/consul/consul kv put ${osd_kv} $(/bin/cat ${osd_tmp} | /usr/bin/base32 -w 0 -)",
      user => 'ceph',
      group => 'ceph',
      require => Package[$packages],
      unless => "/opt/consul/consul kv get ${osd_kv}",
    }

    # create monitor secret key and put in KV store
    exec { "generate_monitor_key":
      command => "/usr/bin/ceph-authtool \
                                --create-keyring ${mon_tmp} \
                                --gen-key \
                                -n mon. \
                                --cap mon 'allow *' \
              && /usr/bin/ceph-authtool \
                                ${mon_tmp} \
                                --import-keyring ${admin_tmp} \
              && /usr/bin/ceph-authtool \
                                ${mon_tmp} \
                                --import-keyring ${osd_tmp} \
              && /opt/consul/consul kv put ${mon_kv} $(/bin/cat ${mon_tmp} | /usr/bin/base32 -w 0 -)",
      user => 'ceph',
      group => 'ceph',
      require => Exec["generate_admin_key",
                      "generate_osd_key"],
      unless => "/opt/consul/consul kv get ${mon_kv}",
    }
  }

  # get monitor keyring
  exec { "get_mon_keyring_consul_${hostname}":
    command => "/opt/consul/consul kv get ${mon_kv} | /usr/bin/base32 -d > ${mon_keyring} \
             && /bin/chmod 600 ${mon_keyring}",
    user => 'ceph',
    group => 'ceph',
    require => Package[$packages],
    unless => "/usr/bin/test -s ${mon_keyring}",    # file not exist or size is zero
  }

  # get administrator keyring
  exec { "get_admin_keyring_consul_${hostname}":
    command => "/opt/consul/consul kv get ${admin_kv} | /usr/bin/base32 -d > ${admin_keyring} \
             && /bin/chmod 600 ${admin_keyring}",
    user => 'root',
    group => 'root',
    require => Package[$packages],
    unless => "/usr/bin/test -s ${admin_keyring}",
  }

  # get osd keyring
  exec { "get_osd_keyring_consul_${hostname}":
    command => "/opt/consul/consul kv get ${osd_kv} | /usr/bin/base32 -d > ${osd_keyring} \
             && /bin/chmod 600 ${osd_keyring}",
    user => 'ceph',
    group => 'ceph',
    require => Package[$packages],
    unless => "/usr/bin/test -s ${osd_keyring}",
  }

  # generate initial monitor map
  $ip = resolv($initial_node)
  exec { 'gen_mon_map':
    command => "/usr/bin/monmaptool --create --add ${initial_node} ${ip} --fsid ${fsid} ${mon_map}",
    creates => $mon_map,
    user => 'ceph',
    group => 'ceph',
    require => Package[$packages],
    unless => ["/usr/bin/test -f ${home}/done",
               "/usr/bin/monmaptool --print ${mon_map} | /bin/grep ${initial_node}"]
  }

  # add other monitors to monitor map
  $hostnames = keys($nodes)
  $hostnames.each |$host| {
    # add only not initial node
    if $host != $initial_node {
      $ip = resolv($host)
      exec { "add_monitor_${host}":
        command => "/usr/bin/monmaptool --add ${host} ${ip} ${mon_map}",
        user => 'ceph',
        group => 'ceph',
        require => Exec['gen_mon_map'],
        unless => ["/usr/bin/test -f ${home}/done",
                   "/usr/bin/monmaptool --print ${mon_map} | /bin/grep ${host}"]
      }
    }
  }

  # populate monitor map and keyring
  exec { 'pop_mon':
    command => "/usr/bin/ceph-mon \
                          --cluster ${cluster} \
                          --mkfs \
                          -i ${hostname} \
                          --monmap ${mon_map} \
                          --keyring ${mon_keyring}",
    user => 'ceph',
    group => 'ceph',
    require => Exec['gen_mon_map'],
    onlyif => "/usr/bin/test -s ${mon_keyring}",
    unless => "/usr/bin/test -f ${home}/done",
  }

  file { "${home}/done":
    ensure => present,
    owner => 'ceph',
    group => 'ceph',
    require => Exec['pop_mon'],
  }

  # ensure service is running and enabled
  service { $services:
    ensure => running,
    enable => true,
    require => File["${home}/done"],
  }
}

class ceph::monitor::delete inherits ceph::monitor {

  # remove monitor from cluster
  exec { 'rm_mon':
    command => "/usr/bin/ceph mon remove ${hostname} --cluster ${cluster}",
    user => 'ceph',
    group => 'ceph',
  }

  # stop and delete services
  service { $services:
    ensure => stopped,
    enable => false,
    require => Exec['rm_mon'],
  }

  # remove monitor dir
  file { $home:
    ensure => absent,
    recurse => true,
    purge => true,
    force => true,
    require => Service[$services],
  }

  # Remove a packages and purge its config files
  package { $packages:
      ensure => 'purged',
      require => File[$home],
  }
}
