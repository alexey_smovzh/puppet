class ceph::osd inherits ceph {
  $packages = ['ceph-osd']
  $services = ["${cluster}-osd@${hostname}"]
}

class ceph::osd::install inherits ceph::osd {

  # install packages
  package { $packages:
    ensure => installed,
    require => Apt::Apt_repository['ceph'],
  }

  # create osd's
  $osds.each |$osd| {
    # create osd enable and start service
    # https://github.com/openstack/puppet-ceph/blob/master/manifests/osd.pp
    exec { "create_osd_${osd}":
      command => "/usr/sbin/ceph-volume --cluster ${cluster} lvm create --data ${osd}
                  id=$(/usr/sbin/ceph-volume --cluster ${cluster} lvm list ${osd} | /bin/grep === | /bin/sed 's/.*osd.\([0-9]*\).*/\1/')
                  /usr/sbin/service ceph-osd@\$id start
                  /bin/systemctl enable ceph-osd@\$id",
      user => 'root',
      group => 'root',
      require => Package[$packages],
      onlyif => "/usr/bin/test -s ${osd_keyring}",
      unless => "/usr/sbin/ceph-volume --cluster ${cluster} lvm list ${osd}",
    }
  }
}


class ceph::osd::delete inherits ceph::osd {

  # delete osds
  $osds.each |$osd| {
    # remove osd from cluster
    exec { "remove_osd_${osd}":
      command => "/bin/true # comment to satisfy puppet syntax requirements
                  id=$(/usr/sbin/ceph-volume --cluster ${cluster} lvm list ${osd} | /bin/grep === | /bin/sed 's/.*osd.\([0-9]*\).*/\1/')
                  /usr/sbin/service ceph-osd@\$id stop
                  /bin/systemctl disable ceph-osd@\$id
                  /usr/bin/ceph osd out \$id --cluster ${cluster}
                  /usr/bin/ceph osd purge \$id --yes-i-really-mean-it --cluster ${cluster}
                  /sbin/vgremove $(/sbin/pvdisplay ${osd} | grep ceph- | awk '{ print $3 }') --force
                  /sbin/pvremove ${osd} --force",
      user => 'root',
      group => 'root',
    }
  }

  # Remove a packages and purge its config files
  package { $packages:
      ensure => 'purged',
  }
}
