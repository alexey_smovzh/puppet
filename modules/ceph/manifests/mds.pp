# When the active MDS becomes unresponsive, a Monitor will wait the number of seconds specified by the mds_beacon_grace option. Then the Monitor marks the MDS daemon as laggy and one of the standby daemons becomes active depending on the configuration.
# To change the value of mds_beacon_grace, add this option to the Ceph configuration file and specify the new value.
# mds beacon interval - The frequency (in seconds) of beacon messages sent to the monitor.
#
# [mds.a]
#   mds_standby_replay = true
#   mds_standby_for_rank = 0
#   mds_beacon_interval = 2
#   mds_beacon_grace = 5
#
# [mds.b]
#   mds_standby_replay = true
#   mds_standby_for_rank = 0
#

class ceph::mds inherits ceph {
  $packages = ['ceph-mds']
  $services = ["${cluster}-mds@${hostname}"]

  $home = "/var/lib/ceph/mds/${cluster}-${hostname}"
  $keyring = "${home}/keyring"
}


class ceph::mds::install inherits ceph::mds {

  # install packages
  package { $packages:
    ensure => installed,
    require => Apt::Apt_repository['ceph'],
  }

  # create mds data dir
  file { $home:
    ensure => 'directory',
    owner => 'ceph',
    group => 'ceph',
    require => Package[$packages],
  }

  # create auth key
  # ceph auth get-or-create mds.<your-mds-id> mon 'profile mds' mgr 'profile mds' mds 'allow *' osd 'allow *' > /var/lib/ceph/mds/ceph-<your-mds-id>/keying
  exec { "generate_mds.${hostname}_auth_key":
    command => "/usr/bin/ceph auth get-or-create --cluster ${cluster} \
                              mds.${hostname} \
                              mon 'profile mds' \
                              mgr 'profile mds' \
                              mds 'allow *' \
                              osd 'allow *' \
                && /usr/bin/ceph auth get mds.${hostname} -o ${keyring} --cluster ${cluster}",
    user => 'root',
    group => 'root',
    creates => $keyring,
    onlyif => "/usr/bin/test -s ${admin_keyring}",         # require admin keyring to create new key
    require => Package[$packages],
  }

  # start service
  # ensure service is running and enabled
  service { $services:
    ensure => running,
    enable => true,
    require => Exec["generate_mds.${hostname}_auth_key"],
  }
}


class ceph::mds::delete inherits ceph::mds {
  # remove monitor from cluster
  exec { 'stop_mds':
    command => "/usr/bin/ceph mds fail mds.${hostname} --cluster ${cluster}",
    user => 'ceph',
    group => 'ceph',
  }

  # stop and delete services
  service { $services:
    ensure => stopped,
    enable => false,
    require => Exec['stop_mds'],
  }

  # remove monitor dir
  file { $home:
    ensure => absent,
    recurse => true,
    purge => true,
    force => true,
    require => Service[$services],
  }

  # Remove a packages and purge its config files
  package { $packages:
      ensure => 'purged',
      require => File[$home],
  }
}
