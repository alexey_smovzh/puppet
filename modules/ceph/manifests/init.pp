class ceph {

  $depencies = ['apt-transport-https', 'ca-certificates', 'software-properties-common']
  $packages = ['ceph-base']
  $services = ['ceph']

  # Hiera values
  $ceph = lookup('ceph', Hash)
  # Extract variables
  $cluster = $ceph['cluster_name']
  $fsid = $ceph['fsid']
  $public_network = $ceph['public_network']
  $cluster_network = $ceph['cluster_network']
  $initial_node = $ceph['initial_node']
  $nodes = $ceph['nodes']
  $pools = $ceph['pools']
  $cephfs = $ceph['cephfs']
  $rbd_features = $ceph['rbd_features']

  # keyrings
  $admin_keyring = '/etc/ceph/ceph.client.admin.keyring'
  $osd_keyring = '/var/lib/ceph/bootstrap-osd/ceph.keyring'

  # release names
  $release = 'mimic'

  # extract osd params for this host
  $osds = $ceph['nodes']["${hostname}"]['osd']

}
