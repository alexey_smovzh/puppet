class ceph::delete inherits ceph {

  include ceph::cephfs::delete
  include ceph::mds::delete
  include ceph::osd::delete
  include ceph::pool::delete  
  include ceph::mgr::delete
  include ceph::monitor::delete


  # stop and delete services
  service { $services:
    ensure => stopped,
    enable => false,
  }

  # remove custom created files and folders
  file { ['/var/lib/ceph',
          '/etc/ceph']:
    ensure => absent,
    purge => true,
    force => true,
    recurse => true,
    require => Service[$services],
  }

  # Remove a packages and purge its config files
  package { [$packages, $depencies]:
      ensure => 'purged',
      require => Service[$services],
  }

  # Remove already unneeded depencies and dowloaded apk
  apt::apt_clean { 'clean_ceph': }


}
