class ceph::pool inherits ceph {
}

class ceph::pool::install inherits ceph::pool {

  if $pools != undef {
    # create pool's
    $pools.each |$pool, $value| {

      $type = $value['type']
      $pg = $value['pg_num']

      # create pool
      # https://ceph.com/community/new-luminous-pool-tags/
      exec { "create_pool_${pool}":
        command => "/usr/bin/ceph osd pool create ${pool} ${pg} ${type} --cluster ${cluster} \
                 && /usr/bin/rbd pool init ${pool}",
        user => 'root',
        group => 'root',
        require => Class['ceph::install',
                         'ceph::monitor::install',
                         'ceph::osd::install',
                         'ceph::mgr::install',],
        unless => "/usr/bin/ceph osd lspools --cluster ${cluster} | /bin/grep ${pool}",
      }
    }
  }
}


class ceph::pool::delete inherits ceph::pool {

  if $pools != undef {
    # delete pools
    $pools.each |$pool| {
      # remove pool from cluster
      exec { "remove_pool_${pool}":
        command => "/usr/bin/ceph tell mon.\\* injectargs '--mon-allow-pool-delete=true' \
                 && /usr/bin/ceph osd pool delete ${pool} ${pool} --yes-i-really-really-mean-it \
                 && /usr/bin/ceph tell mon.\\* injectargs '--mon-allow-pool-delete=false'",
        user => 'root',
        group => 'root',
      }
    }
  }
}
