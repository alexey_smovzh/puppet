# sudo ceph osd pool create cephfs_docker_data 32
# sudo ceph osd pool create cephfs_docker_metadata 32
# sudo ceph fs new cephfs_docker cephfs_docker_metadata cephfs_docker_data
#
# sudo vi /etc/ceph/admin.secret
#
# sudo apt-get install --install-recommends linux-generic-hwe-16.04


class ceph::cephfs inherits ceph {
}

class ceph::cephfs::install inherits ceph::cephfs {

  # make shure cepfs params are present
  if $cephfs != undef {
    $cephfs.each |$fs, $param| {
      # extract values
      $point = $param['point']
      $user = $param['user']
      $client = "client.${user}"
      $pg_num = $param['pg_num']

      $keyring = "/etc/ceph/${cluster}.${client}.keyring"
      $fs_kv = "ceph/${cluster}/${fs}/${user}_keyring"
      $secret = "/etc/ceph/${user}.secret"
      $key_kv = "ceph/${cluster}/${fs}/${user}_key"


      # Code to run only once on initial node
      if $hostname == $initial_node {
        exec { "create_${fs}":
          command => "/usr/bin/ceph osd pool create \
                                    ${fs}_data \
                                    ${pg_num} \
                                    --cluster ${cluster} \
                   && /usr/bin/ceph osd pool create \
                                    ${fs}_metadata \
                                    ${pg_num} \
                                    --cluster ${cluster} \
                  && /usr/bin/ceph fs new \
                                    ${fs} \
                                    ${fs}_metadata \
                                    ${fs}_data",
          user => 'root',
          group => 'root',
          require => Class['ceph::monitor::install',
                           'ceph::mds::install',],
          unless => "/usr/bin/ceph fs get ${fs}",
        }

        # generate auth key
        exec { "generate_${fs}_key":
          command => "/usr/bin/ceph auth get-or-create ${client} \
                                    mds 'allow' \
                                    mon 'allow *' \
                                    osd 'allow * pool=${fs}_data, allow * pool=${fs}_metadata' \
                      && /usr/bin/ceph auth get ${client} -o ${keyring}",
          user => 'root',
          group => 'root',
          require => Exec["create_${fs}"],
          creates => $keyring,
        }

        # put keyring to consul KV store
        exec { "put_keyring_consul_${fs}":
          command => "/opt/consul/consul kv put ${fs_kv} $(/bin/cat ${keyring} | /usr/bin/base32 -w 0 -)
                      /opt/consul/consul kv put ${key_kv} $(/usr/bin/ceph auth get-or-create-key ${client})",
          user => 'root',
          group => 'root',
          require => Exec["generate_${fs}_key"],
          unless => ["/opt/consul/consul kv get ${fs_kv}",
                     "/opt/consul/consul kv get ${key_kv}",],
        }

      } else {
        # get key on ordinary nodes
        exec { "get_keyring_consul_${fs}":
          command => "/opt/consul/consul kv get ${fs_kv} | /usr/bin/base32 -d > ${keyring}",
          user => 'root',
          group => 'root',
          creates => $keyring,
          onlyif => "/opt/consul/consul kv get ${fs_kv}",
          unless => "/usr/bin/test -s ${keyring}",
        }
      }

      # get secret
      exec { "get_secret_consul_${fs}":
        command => "/opt/consul/consul kv get ${key_kv} > ${secret}",
        user => 'root',
        group => 'root',
        creates => $secret,
        onlyif => "/opt/consul/consul kv get ${key_kv}",
        unless => "/usr/bin/test -s ${secret}",
      }

      # ensure mount point exist
      file { $point:
        ensure => 'directory',
      }

      # create fstab entries and mount cephfs
      # docker0,docker1,docker2:6789:/		/mnt/cephfs	ceph	name=admin,secretfile=/etc/ceph/admin.secret,_netdev,noatime	0	0
      # sudo mount -vvvv -t ceph docker0,docker1,docker2:6789:/ /mnt/cephfs -o name=admin,secretfile=/etc/ceph/admin.secret,_netdev,noatime
      $monitors = inline_template("<% @nodes.each_with_index do |(key, value), idx| -%><%= key %><%= ',' if idx < (@nodes.size - 1) %><% end -%>:6789:/")
      mount { $point:
        ensure => 'mounted',
        atboot => true,
        device => $monitors,
        fstype => 'ceph',
        options => "name=${user},secretfile=${secret},_netdev,noatime",
        require => [Exec["get_secret_consul_${fs}"],
                    File[$point]],
      }
    }
  }
}

# stop mds daemons first!!
#  ceph fs rm <cephfs name> --yes-i-really-mean-it
#  sudo ceph tell mon.\* injectargs '--mon-allow-pool-delete=true'
#  ceph osd pool delete <cephfs data pool> <cephfs data pool> --yes-i-really-really-mean-it
#  ceph osd pool delete <cephfs metadata pool> <cephfs metadata pool> --yes-i-really-really-mean-it
#  sudo ceph tell mon.\* injectargs '--mon-allow-pool-delete=false'
#  ceph auth del mds."$hostname"
class ceph::cephfs::delete inherits ceph::cephfs {

  if $cephfs != undef {
    $cephfs.each |$fs, $param| {

      service { "${cluster}-mds@${hostname}":
        ensure => stopped,
        enable => false,
      }

      # purge cephfs
      exec { "purge_${fs}_on_${hostname}":
        command => "/usr/bin/ceph fs rm ${fs} --yes-i-really-mean-it \
                 && /usr/bin/ceph tell mon.\\* injectargs '--mon-allow-pool-delete=true' \
                 && /usr/bin/ceph osd pool delete ${fs}_data ${fs}_data --yes-i-really-really-mean-it \
                 && /usr/bin/ceph osd pool delete ${fs}_metadata ${fs}_metadata --yes-i-really-really-mean-it \
                 && /usr/bin/ceph tell mon.\\* injectargs '--mon-allow-pool-delete=false' \
                 && /usr/bin/ceph auth del mds.${hostname}",
        user => 'root',
        group => 'root',
        # if last command from exec are done successfully
        # thats mean that top commands also complete successfully
        # since we are use '&&' operator
        onlyif => "/usr/bin/ceph auth get-or-create mds.${hostname}",
        require => Service["${cluster}-mds@${hostname}"],
      }
    }
  }
}
