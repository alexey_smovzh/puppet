### TODO:
  0. custom crushmap:
    - data distribution between datacenters and racks
    - osd pools placement with different SLA (fast - NVMe, slow - SAS)

  1. scrubbing: https://ivirt-it.ru/ceph-reduce-osd-scrub-impact/



### 0. Description
This module install Ceph cluster.
Cluster parameters you must define in corresponding Hiera YAML file.
This module rely on Consul module. It uses consul kv to propagate keys
during installation.


### 1. Tested environments
This module developed and tested on Ubuntu 16.04LTS only.


### 2. Usage
Make sure that Ceph hosts can resolve each other by name.
Describe Ceph cluster configuration in appropriate Hiera YAML file.

For example:

```
ceph:
    cluster_name: docker_ceph
    fsid: f687d4e2-b210-4eb5-ad85-7624fd296102
    public_network: 192.168.122.0/24
    cluster_network: 192.168.122.0/24
    initial_member: docker0

    # for some applications we need to limit image features
    # https://tracker.ceph.com/issues/15000
    rbd_features: 7

    # pool example configuration
    pools:
      lxd_pool:
        name: xfs_on_ceph
        type: replicated

    # cephfs example
    cephfs:
      cephfs_docker:
        # mount..
        point: /var/lib/docker/volumes/
        user: dockerfs
        pg_num: 32

    # Don't forget dash symbol '-', it indicates that record is an array element
    nodes:
      docker0:
        monitor: true
        osd:
          - /dev/vdb
          - /dev/vdc
          - /dev/vdd

      docker1:
        monitor: true
        osd:
          - /dev/vdb
          - /dev/vdc
          - /dev/vdd

      docker2:
        monitor: true
        osd:
          - /dev/vdb

```


Then to install or delete:

```
  # Install
  include ceph::install

  # Delete
  include ceph::delete
```


### 3. Known backgrounds and issues
It can install or delete monitor or osd role completely, from scratch.
It can add new OSD or Monitor, after you declare it in YAML configuration file.
But it can't track changes in YAML file between current and desired state.
So it can't delete OSD or Monitor, if you remove its declaration from YAML
configuration file.
You must remove monitor or OSD manually!

There is warning with 'clock scew detected on monitors'
This setup was tested only on virtual environment and this warning can be result
of not proper hardware clock emulation. Need to check this warning on bare-metal
server. If it occurs, need to tune ntp client time synchronizations parameters
or change default ntp client to another one.


### 4. Used documentation
Manual deployment: http://docs.ceph.com/docs/master/install/manual-deployment/
Monitor troubleshooting: https://ceph.com/geen-categorie/ceph-monitor-troubleshooting/
Prometheus module: http://docs.ceph.com/docs/master/mgr/prometheus/
CephFS: https://access.redhat.com/documentation/en-us/red_hat_ceph_storage/3/pdf/ceph_file_system_guide/Red_Hat_Ceph_Storage-3-Ceph_File_System_Guide-en-US.pdf
Ceph image features: https://tracker.ceph.com/issues/15000
