### 0. Description
This module installs Elasticsearch and Graylog for collecting logs from
servers, network equipment and collecting netflow traffic.


### 1. Tested environments
This module developed and tested on Ubuntu 16.04LTS only.


### 2. Usage

To install Graylog use:

```
  class { graylog: install => true }
```

To purge Graylog and all its depencies use:  

```
  class { graylog: install => false }
```


Access to web interface http://<host_name>:9000 i.e. http://prometheus:9000


### 3. Known backgrounds and issues
Graylog module are use Elasticsearch version 5.x, its not compatible
with version 6.x from elasticsearch module. When use elasticsearch module
with this on the same node error "dublicate class reference [elasticsearch]" arise.


### 4. Used documentation
Install instructions: http://docs.graylog.org/en/2.4/pages/installation/os/ubuntu.html
