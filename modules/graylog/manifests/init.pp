class graylog (Boolean $install) {

  $depencies = ['apt-transport-https', 'openjdk-8-jre-headless', 'uuid-runtime', 'pwgen']
  $packages = ['mongodb-org', 'elasticsearch', 'graylog-server']
  $services = ['mongod', 'elasticsearch', 'graylog-server']
  $elastic_dir = ['/var/opt/elasticsearch', '/var/opt/elasticsearch/data', '/var/opt/elasticsearch/log']
  $graylog_dir = ['/var/opt/graylog']


  if $install == true {
      # install depencies
      package { $depencies:
        ensure => installed,
      }

      # add mongodb key
      exec { 'mongodb_key':
  			command => '/usr/bin/apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5',
  			cwd => '/tmp',
  		}

      # add mongodb repo
      apt::apt_repository { 'mongodb':
        file => '/etc/apt/sources.list.d/mongodb-org-3.6.list',
        repository => 'deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.6 multiverse',
      }

      # add elasticsearch repo
      apt::apt_repository { 'elasticsearch':
        key => 'https://artifacts.elastic.co/GPG-KEY-elasticsearch',
        file => '/etc/apt/sources.list.d/elastic-5.x.list',
        repository => 'deb https://artifacts.elastic.co/packages/5.x/apt stable main',
      }

      # add graylog repository
      apt::apt_repository_deb { 'graylog':
        deb => 'https://packages.graylog2.org/repo/packages/graylog-2.4-repository_latest.deb',
        package => 'graylog-2.4-repository_latest.deb',
        fullname => 'graylog-2.4-repository',
      }

      # install packages
      package { $packages:
        require => [Apt::Apt_repository_deb['graylog'],
                    Apt::Apt_repository['mongodb'],
                    Apt::Apt_repository['elasticsearch']],
        ensure => installed,
      }

      # create Elasticsearch config
      file { '/etc/elasticsearch/elasticsearch.yml':
          ensure => present,
          owner => 'root',
          group => 'elasticsearch',
          content => template('graylog/elasticsearch.yml.erb'),
          notify => Service['elasticsearch'],
          require => Package['elasticsearch'],
      }

      # Create Elasticsearch JVM config
      file { '/etc/elasticsearch/jvm.options':
          ensure => present,
          owner => 'root',
          group => 'elasticsearch',
          content => template('graylog/elastic_jvm.options.erb'),
          notify => Service['elasticsearch'],
          require => Package['elasticsearch'],
      }

      # create elasticsearch data and log directories
			file { $elastic_dir:
					ensure => 'directory',
					owner => 'elasticsearch',
					group => 'elasticsearch',
					recurse => true,
					recurselimit => 1,
          require => Package['elasticsearch'],
			}

      # create graylog config
      file { '/etc/graylog/server/server.conf':
          ensure => present,
          owner => 'root',
          group => 'root',
          content => template('graylog/graylog_server.conf.erb'),
          notify => Service['graylog-server'],
          require => Package['graylog-server'],
      }

      # Create graylog JVM config
      file { '/etc/default/graylog-server':
          ensure => present,
          owner => 'root',
          group => 'root',
          content => template('graylog/graylog_jvm.options.erb'),
          notify => Service['graylog-server'],
          require => Package['graylog-server'],
      }

      # create graylog data dir
      file { $graylog_dir:
          ensure => 'directory',
          owner => 'graylog',
          group => 'graylog',
          recurse => true,
          recurselimit => 1,
          require => Package['graylog-server'],
      }



      # ensure all services are enabled and running
			$services.each |$service| {
					service { $service :
	          	ensure => running,
	          	enable => true,
	          	require => Package[$packages],
	      	}
			}

  } else {

    # Stop and disable services
    service { $services :
        ensure => stopped,
        enable => false,
    }

    # delete elasticsearch and graylog data and log directories
    file { [$elastic_dir, $graylog_dir] :
      ensure => absent,
      recurse => true,
      purge => true,
      force => true,
    }

    # Remove a packages and purge its config files
    package { $packages:
        ensure => 'purged',
    }

    # Remove already unneeded depencies and dowloaded apk
    apt::apt_clean { 'clean': }

  }
}
