### 0. Description
This module install pacemaker and corosync


### 1. Tested environments
This module developed and tested on Ubuntu 16.04LTS only.


### 2. Usage
Describe cluster configuration in Hiera yaml file:

```
pacemaker:
  floatip: 192.168.122.10
  floatip_name: FloatIP
  password: $1$SaltSalt$DHCh76WjILaxjiTLdCb/x/
  nodes:
      docker0:
      docker1:
      docker2:
```

Use:

```
  # Install
  include pacemaker::install

  # Delete
  include pacemaker::delete

```

PCSD provides Web-interface to cluster manage and monitoring by this link https://<cluster_node>:2224/login.
To access to this web-interface used credentials of user 'hacluster'.
Pacemaker package during installation create this user, but don't set the password.
To set password for this user generate sha-512 password hash and set it in
yaml files as 'password:' value

```
# Example of generation sha-512 password sum
mkpasswd  -m sha-512 -S <salt> -s
```



### 3. Known backgrounds and issues
Be shure that node name not resolves to local host address 127.0.0.1 or 127.0.1.1
Otherwise you get error to run corosync with error status 20

todo: configure STONITH
may be useful for software fencing https://tools.bitfolk.com/wiki/Softdog


### 4. Used documentation

http://clusterlabs.org/pacemaker/doc/en-US/Pacemaker/1.1/html/Clusters_from_Scratch/_configure_the_cluster_for_drbd.html

https://www.digitalocean.com/community/tutorials/how-to-create-a-high-availability-setup-with-corosync-pacemaker-and-floating-ips-on-ubuntu-14-04
