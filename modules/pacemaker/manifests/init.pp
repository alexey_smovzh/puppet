class pacemaker {

  $packages = ['pacemaker', 'corosync', 'pcs', 'crmsh-']
  $services = ['pacemaker', 'corosync', 'pcsd']

  $cluster = lookup('pacemaker', Hash)
#  $password = lookup('password', String)   # hacluster user password
  # Extract cluster parameters
  $floatip_name = $cluster['floatip_name']
  $floatip_addr = $cluster['floatip']
  $password = $cluster['password']
  $nodes = $cluster['nodes']

}
