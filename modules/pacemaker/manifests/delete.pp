class pacemaker::delete inherits pacemaker {

  # stop and delete services
  service { $services:
    ensure => stopped,
    enable => false,
  }

  # delete key file and pacemaker files
  file { ['/etc/pacemaker/',
          '/var/lib/pacemaker/',
          '/etc/corosync/authkey' ]:
    ensure => absent,
    purge => true,
    force => true,
    recurse => true,
  }

  # Remove a packages and purge its config files
  package { $packages:
      ensure => 'purged',
  }

  # Remove already unneeded depencies and dowloaded apk
  apt::apt_clean { 'clean_pacemaker': }

}
