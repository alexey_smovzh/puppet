class pacemaker::install inherits pacemaker {

  # install packages
  package { $packages:
    ensure => installed,
  }

  # create corosync config
  file { '/etc/corosync/corosync.conf':
    ensure => present,
    owner => 'root',
    group => 'root',
    content => template('pacemaker/corosync.conf.erb'),
    require => Package[$packages],
    notify => Service[$services],
  }

  # Corosync pregenerated key:
  # apt install haveged
  # corosync-keygen -k authkey
  # apt purge haveged
  # cp -pR authkey /etc/puppetlabs/code/environment/production/modules/pacemaker/files
  # chown puppet:puppet /etc/puppetlabs/code/environment/production/modules/pacemaker/files/authkey
  file { '/etc/corosync/authkey':
    ensure => present,
    owner => 'root',
    group => 'root',
    mode => '400',
    source => 'puppet:///modules/pacemaker/authkey',
    require => Package[$packages],
  }

  # if fencing devices not configured and in 2 node setup
  # we must disable STONITH
#  if $nodes.length == 2 {
    exec { 'disable_stonith':
      command => '/usr/sbin/pcs property set stonith-enabled=false \
                                             no-quorum-policy=ignore; \
                  /usr/sbin/pcs resource defaults resource-stickness=100 ',
      unless => "/usr/sbin/pcs property show | /bin/grep 'stonith-enabled: false'",
      require => Package[$packages],
    }
#  }

  # set password to user hacluster
  # used to access web interface and to authenticate inside cluster
  user { 'hacluster':
    ensure => 'present',
    password => $password,
    require => Package[$packages],
  }

  $nodes_list = inline_template('<% @nodes.each do |key, value| -%><%= key %> <% end -%>')
  # Authorise cluster nodes
  exec { 'authorise_nodes':
    command => "/usr/sbin/pcs cluster auth ${nodes_list} -u hacluster -p hacluster",
    onlyif => "/usr/sbin/pcs status | /bin/grep 'Unable to authenticate'",
  }

  # Create cluster ip resource
  exec { 'add_floatip_res':
    command => "/usr/sbin/pcs resource create ${floatip_name} ocf:heartbeat:IPaddr2 ip=${floatip_addr} cidr_netmask=24 op monitor interval=10s",
    unless => "/usr/sbin/pcs resource show ${floatip_name}",
    require => Package[$packages],
  }

  # Set cluster properties
  # longer history for cluster debug on failure
  # reduce pacemaker internal processes interval from default 15 min
  exec { 'cluster_properties':
    command => "/usr/sbin/pcs property set pe-warn-series-max=1000 \
                                           pe-input-series-max=1000 \
                                           pe-error-series-max=1000 \
                                           cluster-recheck-interval=3m ",
    unless => "/usr/sbin/pcs property show | /bin/grep 'cluster-recheck-interval: 3m'",
    require => Package[$packages],
  }

  # ensure service is running and enabled
  service { $services:
    ensure => running,
    enable => true,
    require => Package[$packages],
  }
}
