

class consul::delete inherits consul {

  # stop service
  service { 'consul':
    ensure => stopped,
    enable => false,
  }

  # delete user
  user { $user:
    ensure => 'absent',
  }

  # delete group
  group { $group:
    ensure => 'absent',
  }
  
  # delete service file
  file { '/lib/systemd/system/consul.service':
    ensure => absent,
    purge => true,
    force => true,
  }

  # delete working directory
  file { $home:
    ensure => absent,
    recurse => true,
    purge => true,
    force => true,
  }
}
