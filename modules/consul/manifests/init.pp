class consul {
  $user = 'consul'
  $group = 'consul'
  $home = '/opt/consul'
  $description = 'Consul cluster from HashiCorp'
  $consul = lookup('consul', Hash)
  $bootstrap = $consul['bootstrap']

  $depencies = ['unzip']

  # timeout and retry count for wget to check if consul service are
  # started on initial node
  $tries = '30'
  $timeout = '1200'
}
