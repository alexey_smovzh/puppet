class consul::install inherits consul {

  # install depencies
  package { $depencies:
    ensure => 'installed',
  }

  if $hostname != $bootstrap {
    # wait until consul service start on initial node
    exec { 'wait_for_initial_node':
      command => "/usr/bin/wget --tries=${tries} --retry-connrefused -O /dev/null -o /dev/null ${bootstrap}:8302",
      require => Package[$depencies],
      timeout => $timeout,
      unless => "/opt/consul/consul members | /bin/grep ${hostname}",
    }
  }

  # add group
  group { $group:
    ensure => 'present',
    require => Package[$depencies],
  }

  # create service user
  user { "${user}":
    ensure => 'present',
    groups => $group,
    home => $home,
    shell => '/bin/false',
    comment => $description,
    password => '*',
    managehome => true,
    require => Group[$group],
  }

  # download consul binary
  exec { 'download_consul':
    command => '/usr/bin/wget https://releases.hashicorp.com/consul/1.2.2/consul_1.2.2_linux_amd64.zip',
    cwd => $home,
    creates => "${home}/consul_1.2.2_linux_amd64.zip",
    require => User[$user],
  }

  file { "${home}/consul_1.2.2_linux_amd64.zip":
    require => Exec['download_consul'];
  }

  # unpack
  exec { 'unpack_consul':
    command => '/usr/bin/unzip consul_1.2.2_linux_amd64.zip',
    cwd => $home,
    require => File["${home}/consul_1.2.2_linux_amd64.zip"],
    creates => "${home}/consul",
  }

  # create service
  file { '/lib/systemd/system/consul.service':
    ensure => present,
    content => template('consul/consul.service.erb'),
    require => Exec['unpack_consul'],
    notify => Service['consul'],
  }

  # create config
  file { "${home}/consul.json":
    ensure => present,
    content => template('consul/consul.json.erb'),
    require => Exec['unpack_consul'],
    notify => Service['consul'],
  }

  # run service
  service { 'consul':
    ensure => running,
    enable => true,
    require => File['/lib/systemd/system/consul.service'],
  }
}
