### 0. Description
This module install Consul cluster.
Cluster name and node list you must define in corresponding Hiera YAML file.


### 1. Tested environments
This module developed and tested on Ubuntu 16.04LTS only.


### 2. Usage
Make sure that Consul hosts can resolve each other by name.
Describe Consul cluster configuration in appropriate Hiera YAML file.

For example:

```
consul:
  cluster: docker_consul
  nodes: ["docker0:8301", "docker1:8301", "docker2:8301"]

```


Then to install or delete:

```
  # Install
  include consul::install

  # Delete
  include consul::delete
```


### 3. Known backgrounds and issues
Not found yet
todo: tls authentification!!!


### 4. Used documentation
https://imaginea.gitbooks.io/consul-devops-handbook/content/server_configuration.html
https://www.consul.io/intro/getting-started/kv.html
