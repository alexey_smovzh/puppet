### 0. Description
This module configure node ip network interfaces with static IP, DHCP
or delete IP configuration at all if interface 'del' provided.
Network information taked from node Hiera yaml file.
In Ubuntu16.pp subnet address and broadcast are calculated automatically
by custom function on Ruby (look at: cidr.rb).


### 1. Tested environments
This module developed and tested on Ubuntu 16.04LTS
and Ubuntu 18.04LTS (netplan).


### 2. Usage
Create file or add to file hiera/nodes/<fqdn>.yaml
network information like in example:

```
# Start network configuration
network_config:
  # Interface with static IP
  ens3:
    ip: 192.168.122.101/24
    gw: 192.168.122.1
    dns: 8.8.8.8

  # Interface with DHCP
  ens4:
    ip: dhcp

  # Deleted interface
  ens5:
    ip: del

```

In site.pp in 'class server' this module will called by:

```
include network_config
```


### 3. Known backgrounds and issues
all fixed now


### 4. Used documentation
Augeas example:
https://blog.bigon.be/2011/05/16/managing-etcnetworkinterfaces-with-puppet-and-augeas/

Calculating network(cidr) information Ruby program example:
http://codepad.org/o9AHsly9

Ubuntu 18 netplan: https://linuxconfig.org/how-to-configure-static-ip-address-on-ubuntu-18-04-bionic-beaver-linux
