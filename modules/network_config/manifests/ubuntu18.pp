class network_config::ubuntu18 {

  $dir = '/etc/netplan'
  # network config file created during OS installation
  $default = '/etc/netplan/01-netcfg.yaml'

  lookup('network_config', Hash).each |String $interface, Hash $value| {

    $ip = $value[ip]
    $gw = $value[gw]
    $dns = $value[dns]

    # generate file path
    # priority = interface number + 10
    # config file path = priority - interface name without digits .yaml
    $device_name = regsubst($interface, '\d', '')
    $device_number = regsubst($interface, '[a-z]+', '')
    $priority = $device_number + 10
    $config = "${dir}/${priority}-${device_name}.yaml"


    # delete default config
    file { $default:
      ensure => absent,
      purge => true,
      force => true,
      notify => Exec['netplan'],
    }

    # Delete interface
    if $ip == 'del' {
      # delete interface configuration file
  		file { $config:
  			ensure => absent,
  			purge => true,
  			force => true,
        notify => Exec['netplan'],
  		}

    # Configure DHCP
    } elsif $ip == 'dhcp' {
      #
      file { $config:
        ensure => present,
        owner => 'root',
        group => 'root',
        mode => '644',
        content => template("${module_name}/dhcp.erb"),
        notify => Exec['netplan'],
      }

    # Configure static IP address
    } else {
      # use different template depending of interface type
      case $device_name {
          'br': { $template = '' }
          default: { $template = 'static'}
      }

      # create interface config
      file { $config:
        ensure => present,
        owner => 'root',
        group => 'root',
        mode => '644',
        content => template("${module_name}/${template}.erb"),
        notify => Exec['netplan'],
      }
    }
  }

  # Apply changes
  exec { 'netplan':
    command => "/usr/sbin/netplan apply",
    user => 'root',
    group => 'root',
    refreshonly => 'true',
  }
}
