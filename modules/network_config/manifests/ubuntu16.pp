class network_config::ubuntu16 {
  
  $config = '/etc/network/interfaces'
  $context = "/files/${config}"

  # Getting data from Hiera
  lookup('network_config', Hash).each |String $interface, Hash $value| {

      $ip = $value[ip]
      $gw = $value[gw]
      $dns = $value[dns]

      # Delete iterface
      if $ip == 'del' {
        augeas { "rm_${interface}":
            context => $context,
            changes => [ "rm iface[. = '${interface}']", ],
        }

        # Dirty hack to delete parameter 'auto <interface>'
        # how to do it from augeas?...
        exec { "rm_auto_${interface}":
            command => "/bin/sed -i /^'auto ${interface}'/d ${config}",
            onlyif  => "/bin/grep ^'auto ${interface}' ${config}",
        }


      # Configure DHCP
      } elsif $ip == 'dhcp' {

          augeas { "auto_${interface}":
              context => $context,
              changes => [ "set auto[last()+1]/1 ${interface}", ],
              onlyif => "match auto/*[ . = '${interface}'] size == 0",
          }

          augeas { "${interface}":
              context => $context,
              changes => [
                            "set iface[. = '${interface}'] ${interface}",
                            "set iface[. = '${interface}']/family inet",
                            "set iface[. = '${interface}']/method dhcp",
                            "rm iface[. = '${interface}']/address",
                            "rm iface[. = '${interface}']/netmask",
                            "rm iface[. = '${interface}']/network",
                            "rm iface[. = '${interface}']/broadcast",
                            "rm iface[. = '${interface}']/gateway",
                            "rm iface[. = '${interface}']/dns-nameservers",
                         ],
              notify => Service['networking'],
          }

      # Configure static IP address
      } else {

          # calculate netmask, subnet and broadcast
          $values = cidr($ip)

          # To view available options:
          # $ sudo /opt/puppetlabs/puppet/bin/augtool ls /files/etc/network/interfaces/iface
          augeas { "auto_${interface}":
              context => $context,
              changes => [ "set auto[last()+1]/1 ${interface}", ],
              onlyif => "match auto/*[ . = '${interface}'] size == 0",
          }

          augeas { "ip_${interface}":
              context => $context,
              changes => [
                            "set iface[. = '${interface}'] ${interface}",
                            "set iface[. = '${interface}']/family inet",
                            "set iface[. = '${interface}']/method static",
                            "set iface[. = '${interface}']/address ${values[address]}",
                            "set iface[. = '${interface}']/netmask ${values[netmask]}",
                            "set iface[. = '${interface}']/network ${values[network]}",
                            "set iface[. = '${interface}']/broadcast ${values[broadcast]}",
                         ],
              onlyif => "get iface[. = '${interface}']/address != ${values[address]}",
              notify => Service['networking'],
          }

          # Dirty hack to clear old interface ip configuration
          # otherwise new ip address will be setted as secondary ip
          # service networking restart doesn't change this behaivor
          exec { "flush_${interface}":
            command => "/bin/ip addr flush dev ${interface}",
            require => Augeas["ip_${interface}"],
            unless => "/bin/ip address show dev ${interface} | /bin/grep ${values[address]}"
          }
      }

      # check if gateway not empty update it, otherwise remove
      if $gw != undef {
          augeas { "gw_set_${interface}":
            context => $context,
            changes => [ "set iface[. = '${interface}']/gateway ${gw}" ],
            onlyif  => "get iface[. = '${interface}']/gateway != '${gw}'",
          }
      } else {
          augeas { "gw_rm_${interface}":
            context => $context,
            changes => [ "rm iface[. = '${interface}']/gateway" ],
          }
      }

      # check if dns not empty update it, otherwise remove
      if $dns != undef {
          augeas { "dns_set_${interface}":
            context => $context,
            changes => [ "set iface[. = '${interface}']/dns-nameservers ${dns}" ],
            onlyif  => "get iface[. = '${interface}']/dns-nameservers != '${dns}'",
          }
      } else {
          augeas { "dns_rm_${interface}":
            context => $context,
            changes => [ "rm iface[. = '${interface}']/dns-nameservers" ],
          }
      }
  }

  # Network service
  service { 'networking':
      ensure => running,
      enable => true,
  }
}
