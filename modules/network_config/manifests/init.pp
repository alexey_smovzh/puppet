# create /etc/network/interfaces file with given parameters
#
class network_config {

  case $::operatingsystem {

    'ubuntu', 'debian': {
        case $::lsbdistcodename {
            'bionic': { include network_config::ubuntu18 }
            'xenial': { include network_config::ubuntu16 }
        }
    }

    'RedHat', 'Fedora', 'CentOS', 'Scientific', 'SLC', 'Ascendos', 'CloudLinux', 'PSBM', 'OracleLinux', 'OVS', 'OEL': {
        # redhat specific
    }
    'SLES', 'SLED', 'OpenSuSE', 'SuSE': {
        # suse specific
    }
  }
}
