### 0. Description
This module setup several services to provide network unattended installation
of operating system to bare metal servers.
This module install and configure such services:
dnsmasq - provide DHCP to network boot configuration
          provide TFTP to download initial boot images and scripts
lighttpd - provide access to distribution files via HTTP protocol to
          installation procedure

Manifest download_ubuntu.pp - downloads ubuntu 16.04 Server iso and creates
network installation from it. By this example can be created network install
for other distributions.          

File ks_ubuntu.cfg - KickStart script to provide unattended installation

! Take attention: postinstall scripts on the end of ks_ubuntu.cfg kickstart file.
! Take attention: default administrator account getted from hiera/common.yaml

This module usage scenario implies that dnsmasq and lighttpd services will
listen on management network interface only (not on data or public interfaces).
New/Clean bare metal server will connect through its management interface to
management network and turn on. In next, server configures its network management
interface via DHCP with any IP address from DHCP lease range and its production
hostname (see templates/dnsmasq.conf.erb), boots and install operating system.
Postinstall scripts fix apt repositories entries, locale settings and installs
Puppet5 agent. After that point all node configuration tasks includes
network interfaces settings, software installation and configuration
performed by Puppet.

! Do not forget to sign new node puppet certificate and check if it working well


### 1. Tested environments
This module developed and tested on Ubuntu 16.04LTS only.


### 2. Usage
To setup PXE Boot Server use:

```
  class { pxe_boot_server: install => true }
```

To purge PXE Boot Server:  

```
  class { pxe_boot_server: install => false }
```

To assign production hostname to server via its MAC address - edit file
hiera/common.yaml. Script templates/configure_networks.sh.erb getting all
values from it:

```
nodes_static:
  lxd0:
    mac: 52:54:00:de:aa:61
    ip: 192.168.122.200
    comment: LXC containers primary host
```

To add another linux distribution to network install use as example
manifests/download_ubuntu.pp. To add new distribution to installation menu
edit file templates/txt.cfg.erb (Not tested!):

```
menu label ^Automatically install Ubuntu 16.04
kernel ubuntu-installer/amd64/linux
append auto=true url=http://<%= @ipaddress %>/ubuntu/preseed/local-sources.seed vga=788 initrd=ubuntu-installer/amd64/initrd.gz ks=http://<%= @ipaddress %>/ubuntu/ks_ubuntu.cfg --- quiet
```


### 3. Known backgrounds and issues
Default manifest operation execution time is 300 seconds, when this time is gone
and if operation not completed yet, it will be interrupted by timeout.
In download_ubuntu.pp manifest this kind of operation is downloading ISO image.
We do not now how many time it takes to complete, it depends on Internet speed,
server availability and other factors. Now this option sets to 1200 seconds
(20 min) if this timeout too long or too short you can tune it with attribute
timeout in operation definition. But do not forget that puppet agent on host
runs every 30 minutes by default.

download_ubuntu.pp:
```
# Download ubuntu 16.04 server
exec { 'download_ubuntu':
  command => "/usr/bin/wget http://releases.ubuntu.com/16.04/ubuntu-16.04.4-server-amd64.iso",
  cwd => $iso_home,
  timeout => 1200,
  creates => "${iso_home}/${iso}",
}
```


### 4. Used documentation
PXE boot configuration: https://www.tecmint.com/install-ubuntu-via-pxe-server-using-local-dvd-sources/

Kick start installation: http://gyk.lt/ubuntu-16-04-desktop-unattended-installation/
