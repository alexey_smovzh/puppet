class pxe_boot_server::install inherits pxe_boot_server {

    # install packages
    package { $packages:
      ensure => installed,
    }

    # create tftp directory
    file { $tftp_home :
      ensure => 'directory',
      owner => 'dnsmasq',
      recurse => true,
      recurselimit => 1,
    }

    # create www directories
    file { $www_home :
      ensure => 'directory',
      owner => 'www-data',
      group => 'www-data',
      recurse => true,
      recurselimit => 1,
    }

    # create config
    file { '/etc/dnsmasq.conf':
      ensure => present,
      content => template('pxe_boot_server/dnsmasq.conf.erb'),
      require => Package['dnsmasq'],
      notify => Service[$boot_service],						# restart service on config change
    }

    # create lighttp config
    file { '/etc/lighttpd/lighttpd.conf':
      ensure => present,
      content => template('pxe_boot_server/lighttpd.conf.erb'),
      require => Package['lighttpd'],
      notify => Service[$www_service],
    }

    # ensure service is running and enabled
    service { $services:
      ensure => running,
      enable => true,
      require => Package[$packages],
    }

    # download and setup boot imges
    include pxe_boot_server::download_ubuntu

}
