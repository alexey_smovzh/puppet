class pxe_boot_server {

  $packages = ['dnsmasq', 'lighttpd']
  $boot_service = 'dnsmasq'
  $www_service = 'lighttpd'
  $services = [$boot_service, $www_service]
  $tftp_home = '/var/tftp'
  $www_home = ['/var/www', '/var/www/html', '/var/www/html/iso']
  $directories = [$tftp_home, $www_home]

  # Hiera data
  $nodes = lookup('nodes_static', Hash)
  $admins = lookup('default_admin', Hash)

  # Ubuntu constants
  $release = 'bionic'
  $url = 'http://cdimage.ubuntu.com/releases/18.04/release/'
  $iso = 'ubuntu-18.04.1-server-amd64.iso'
  $home = '/var/www/html/ubuntu'
  $iso_home = '/var/www/html/iso'
  $postinstall = "${home}/postinstall"
  $scripts = ['fix_apt_repository.sh',
              'fix_locale.sh',
              'install_puppet5.sh']
}
