class pxe_boot_server::delete inherits pxe_boot_server {
  # Stop and disable services
  service { $services :
    ensure => stopped,
    enable => false,
  }

  # remove directories
  file { $directories :
    ensure => absent,
    recurse => true,
    purge => true,
    force => true,
  }

  # Remove a packages and purge its config files
  package { $packages:
      ensure => 'purged',
  }

  # Remove already unneeded depencies and dowloaded apk
  apt::apt_clean { 'clean': }
}
