class pxe_boot_server::download_ubuntu inherits pxe_boot_server {


    # create distribution and postinstall directory
    file { [$home, $postinstall]:
      ensure => 'directory',
      owner => 'www-data',
      group => 'www-data',
    }

    # Download ubuntu 16.04 server
    exec { 'download_ubuntu':
      command => "/usr/bin/wget ${url}/${iso}",
      cwd => $iso_home,
      timeout => 1200,             # max time for process execution in seconds, over it it will be killed
      creates => "${iso_home}/${iso}",
    }

    # mount iso
    exec { 'mount_ubuntu':
      command => "/bin/mount -o loop ${iso_home}/${iso} /mnt",
      creates => "${home}/ubuntu",         # it not creates this file exactly, but if run this command earlier this file will exist
      require =>  Exec['download_ubuntu'],
    }

    # copy netboot files
    exec { 'copy_netboot':
      command => "/bin/cp -rf /mnt/install/netboot/* /var/tftp/",
      user => 'dnsmasq',
      require => Exec['mount_ubuntu'],
      creates => '/var/tftp/version.info',
    }

    # copy distribution files
    exec { 'copy_distribution':
      command => "/bin/cp -rf /mnt/* ${home}",
      user => 'www-data',
      require => Exec['mount_ubuntu'],
      creates => "${home}/ubuntu",
      notify => Service[$www_service],
    }

    # boot menu options
    file { '/var/tftp/ubuntu-installer/amd64/boot-screens/txt.cfg':
      ensure => present,
      owner => 'dnsmasq',
      content => template('pxe_boot_server/txt.cfg.erb'),
      require => Exec['copy_netboot'],
    }

    # time out to run automatic install
    file { '/var/tftp/ubuntu-installer/amd64/boot-screens/syslinux.cfg':
      ensure => present,
      owner => 'dnsmasq',
      content => template('pxe_boot_server/syslinux.cfg.erb'),
      require => Exec['copy_netboot'],
    }

    # create preseed file
    file { "${home}/ubuntu/preseed/local-sources.seed":
      ensure => present,
      owner => 'www-data',
      group => 'www-data',
      content => template('pxe_boot_server/local-sources.seed.erb'),
      require => Exec['copy_distribution'],
    }

    # create kick start file
    file { "${home}/ks_ubuntu.cfg":
      ensure => present,
      owner => 'www-data',
      group => 'www-data',
      content => template('pxe_boot_server/ks_ubuntu.cfg.erb'),
      require => Exec['copy_distribution'],
      notify => Service[$www_service],
    }

    # postinstall scripts
    $scripts.each |$script| {
      file { "${postinstall}/${script}":
          ensure => present,
          owner => 'www-data',
          group => 'www-data',
          content => template("pxe_boot_server/${script}.erb"),
          require => Exec['copy_distribution'],
          notify => Service[$www_service],
      }
    }

    # umount iso
    mount { '/mnt':
      ensure => absent,
      require => Exec['copy_distribution', 'copy_netboot'],
    }
}
