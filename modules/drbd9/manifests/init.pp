class drbd9 {
	 $packages = ['drbd-utils', 'python-drbdmanage', 'drbd-dkms', 'parted', 'psmisc']
}

#
class drbd9::install (
		Hash $resource,
		Hash $cluster,
) inherits drbd9 {

	 # Add drbd repository
	 apt::apt_repository { 'drbd9':
	 	 key => 'CEAA9512',
	   file => '/etc/apt/sources.list.d/drbd9.list',
	   repository => 'deb http://ppa.launchpad.net/linbit/linbit-drbd9-stack/ubuntu xenial main',
	 }

	 # install packages
	 package { $packages:
		 require => Apt::Apt_repository['drbd9'],
		 ensure => installed,
	 }

	# create global config
	file { '/etc/drbd.d/global_common.conf':
		 ensure => present,
		 owner => root,
		 group => root,
		 content => template('drbd9/global_common.conf.erb'),
		 require => Package[$packages],
	}

	# Make shure that service are disabled
	# since we use cluster manager
	service {'drbd':
			ensure => stopped,
			enable => false,
	}


	# It can be multiple resources, create them all in cycle
	$resource.each |$key, $value| {

			$floatip_name = $cluster['floatip_name']
			# Extract variables
			$volume = $key
			$device = $value['device']
			$disk = $value['disk']
			$primary = $value['primary']
	    $nodes = $value['nodes']
			$mount_point = $value['mount_point']


			# Volume config
			file { "/etc/drbd.d/${volume}.res":
				 ensure => present,
				 owner => root,
				 group => root,
				 content => template("drbd9/volume.res.erb"),
				 require => Package[$packages],
			}

			# Create partition
			$device_without_numbers = regsubst($disk, '\d+$', '')
			exec {"partition_${volume}":
				 command => "/sbin/parted --script ${device_without_numbers} \
    																			 mklabel gpt \
    									  									 mkpart primary ext4 1MiB 100%",
				 # parted -s -a optimal /dev/sda mklabel gpt -- mkpart primary ext4 1 -1s
				 unless => "/sbin/fdisk -l | /bin/grep ${disk}",
				 require => File["/etc/drbd.d/${volume}.res"],
			}

			# Create device
			exec {"create_${volume}":
				command => "/usr/sbin/drbdadm create-md ${volume} --force",
				unless => "/usr/sbin/drbdadm status ${volume}",
				require => Exec["partition_${volume}"],
			}

			# UP device
			exec {"up_${volume}":
				command => "/usr/sbin/drbdadm up ${volume}",
				require => Exec["create_${volume}"],
				unless => "/usr/sbin/drbdadm status ${volume}",
			}

			# create mount mount
			file { $mount_point:
				ensure => 'directory',
				recurse => true,
			}

			# Check if this node hostname match primary node from config
			if $fqdn == $primary {
					# Secondary server can be not ready now
					# force this to be primary server
					# otherwise next commands exit with errors
					$result = "/usr/sbin/drbdadm status ${volume} | /bin/grep UpToDate"

					exec {"force_${volume}":
						command => "/usr/sbin/drbdadm primary ${volume} --force",
						require => Exec["up_${volume}"],
						unless => $result,
					}
				  # drbdadm -- --overwrite-data-of-peer primary v0/0
					exec {"init_${volume}":
						command => "/usr/sbin/drbdadm -- --overwrite-data-of-peer primary ${volume}",
						require => Exec["force_${volume}"],
						unless => $result,
					}
					# delay for 60 seconds
					# wait until drbd brings resource up
					exec {"delay_${volume}":
						command => "/bin/sleep 60",
						require => Exec["init_${volume}"],
						unless => $result,
					}

					exec {"mkfs_${volume}":
						command => "/sbin/mkfs.ext4 ${device}",
						require => Exec["delay_${volume}"],
						unless => $result,
					}

					# Configure cluster resource
					exec { "add_cluster_res_${volume}":
						command => "/usr/sbin/pcs resource create ${volume}_res ocf:linbit:drbd drbd_resource=${volume} op monitor interval=29s role=Master op monitor interval=31s role=Slave && \
												/usr/sbin/pcs	resource master ${volume}_master_slave ${volume}_res master-max=1 master-node-max=1 clone-max=2 clone-node-max=1 notify=true && \
												/usr/sbin/pcs	constraint colocation add ${volume}_master_slave with ${floatip_name} INFINITY && \
												/usr/sbin/pcs	constraint order ${floatip_name} then ${volume}_master_slave && \
												/usr/sbin/pcs	resource create ${volume}_fs_res Filesystem device=${device} directory=${mount_point} fstype=ext4 && \
												/usr/sbin/pcs	constraint colocation add ${volume}_fs_res ${volume}_master_slave INFINITY with-rsc-role=Master && \
												/usr/sbin/pcs	constraint order ${volume}_master_slave then ${volume}_fs_res",
						unless => "/usr/sbin/pcs resource show | grep ${volume}_res",
/*
						command => "/usr/sbin/crm configure primitive ${volume}_res ocf:linbit:drbd params drbd_resource=${volume} op monitor interval=29s role=Master op monitor interval=31s role=Slave \
																			configure ms ${volume}_master_slave ${volume}_res meta master-max=1 master-node-max=1 clone-max=2 clone-node-max=1 notify=true \
																			configure primitive ${volume}_fs_res ocf:heartbeat:Filesystem params device=${device} directory=/var/www/html fstype=ext4 \
																			configure colocation ${volume}_fs_colo INFINITY: ${volume}_fs_res ${volume}_master_slave:Master \
																			configure order ${volume}_fs_after_drbd mandatory: ${volume}_master_slave:promote ${volume}_fs_res:start",
						unless => "/usr/sbin/crm configure show | grep primitive ${volume}_res",
*/
						require => Exec["mkfs_${volume}"],
					}
			}
	}
}

class drbd9::delete inherits drbd9 {

		 	# Remove a packages and purge its config files
		 	package { $packages:
		 			ensure => 'purged',
		 	}

		 	# Remove already unneeded depencies and dowloaded apk
		 	apt::apt_clean { 'clean_drbd9': }

}
