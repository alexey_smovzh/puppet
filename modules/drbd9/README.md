### 0. Description
This module install DRBD 9


### 1. Tested environments
This module developed and tested on Ubuntu 16.04LTS only.


### 2. Usage
Describe drbd resources under resource section in Hiera yaml file:

```
resource:
  lxd_volume0:
    device: /dev/drbd0
    disk: /dev/sdb1
    nodes:
      lxd0: 192.168.122.200:7789
      lxd1: 192.168.122.201:7789
    primary: lxd0
```

Evaluate data from hiera and call DRBD module:

```
    # Hiera values
    $resource = lookup('resource', Hash)

    if $install == true {

      class { 'drbd9::install': resource => $resource }

    } else {

      include drbd9::delete

    }
```


### 3. Known backgrounds and issues

todo: split-brain notification
todo: tune drbd resource config parameters


### 4. Used documentation

DRBD documentation: https://docs.linbit.com/docs/users-guide-9.0/

DRBD install and configuration video: https://www.youtube.com/watch?v=WQGi8Nf0kVc

Troubleshooting: http://avid.force.com/pkb/articles/en_US/Compatibility/Troubleshooting-DRBD-on-MediaCentral
