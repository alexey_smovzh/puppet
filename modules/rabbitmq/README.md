### 0. Description
This module install RabbitMQ message broker and creates a cluster.
All parameters stored in init.pp manifest.

In current settings are enabled web management plugin.
It available on tcp/15672 port of each rabbitmq node.


### 1. Tested environments
This module developed and tested on Ubuntu 18.04LTS only.


### 2. Usage
Describe configuration parameters in init.pp manifest (future work to move it in to Heira)
And include in appropriate profile.

```
  # Install
  include rabbitmq::install

  # Delete
  include rabbitmq::delete
```


### 3. Known backgrounds and issues
all fixed now


### 4. Used documentation

Installation: https://www.rabbitmq.com/install-debian.html

Configuration for openstack: https://docs.openstack.org/ha-guide/shared-messaging.html

Config example: https://github.com/rabbitmq/rabbitmq-server/blob/master/docs/rabbitmq.conf.example
