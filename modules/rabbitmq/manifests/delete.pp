class rabbitmq::delete inherits rabbitmq {


  # stop and delete services
  service { $services:
    ensure => stopped,
    enable => false,
  }

  # Remove a packages and purge its config files
  package { [$packages,
             $depencies]:
      ensure => 'purged',
  }

  # remove repository
  file { $repository:
    ensure => absent,
    purge => true,
    force => true,
    recurse => true,
  }

  # Remove already unneeded depencies and dowloaded apk
  apt::apt_clean { 'clean_mariadb': }


}
