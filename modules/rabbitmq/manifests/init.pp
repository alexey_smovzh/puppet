class rabbitmq {

  $repository = '/etc/apt/sources.list.d/rabbitmq.list'
  $limits = '/etc/systemd/system/rabbitmq-server.service.d'
  $depencies = ['init-system-helpers', 'socat', 'logrotate', 'adduser']
  $packages = ['rabbitmq-server']
  $services = ['rabbitmq-server']

  # enabled plugins
  $plugins = ['rabbitmq_management', 'rabbitmq_management_agent']

  # cluster
  $rabbitmq = lookup('rabbitmq', Hash)
  $cluster = $rabbitmq['cluster_name']
  $initial_node = $rabbitmq['initial_node']
  $nodes = $rabbitmq['nodes']
  # todo: maybe change to defautl admins
  $user = $rabbitmq['user']
  $password = $rabbitmq['password']


}
