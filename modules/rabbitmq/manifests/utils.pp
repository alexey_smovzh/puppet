# create rabbitmq user
# @user - user name
# @password - user password
# @group - user tag in rabbitmq terms
#

define rabbitmq::utils::create_user(String $user, String $password, String $group) {

  exec { "create_rabbitmq_user_${user}":
    command => "/usr/sbin/rabbitmqctl add_user ${user} ${password} \
             && /usr/sbin/rabbitmqctl set_user_tags ${user} ${group} \
             && /usr/sbin/rabbitmqctl set_permissions -p / ${user} \".*\" \".*\" \".*\"",
    user => 'root',
    group => 'root',
    unless => "/usr/sbin/rabbitmqctl list_users | /bin/grep ${user}",
  }
}
