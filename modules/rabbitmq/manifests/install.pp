class rabbitmq::install inherits rabbitmq {

  # add rabbitmq repository
  apt::apt_repository { 'rabbitmq':
    key => 'https://dl.bintray.com/rabbitmq/Keys/rabbitmq-release-signing-key.asc',
    file => $repository,
    repository => "deb https://dl.bintray.com/rabbitmq/debian ${::lsbdistcodename} main erlang",
  }

  # check if it not already defined and install depencies
  $depencies.each|$dep| {
    if !defined(Package[$dep]) {
      package { $dep:
        ensure => installed,
      }
    }
  }

  # install common for all roles packages
  package { $packages:
    ensure => installed,
    require => [Apt::Apt_repository['rabbitmq'],
                Package[$depencies]]
  }

  # create cookie
  # content can be anything but the same on all nodes
  # this created with: echo 'rabbitmq' | md5sum | tr [a-z] [A-Z]m
  file { "/var/lib/rabbitmq/.erlang.cookie":
    ensure => present,
    content => 'C773D81D1F6B1D7BB58BD3B30F732A12',
    mode => '400',
    require => Package[$packages],
    notify => Service[$services],
  }

  # create rabbitmq limits configuration directory
  # with systemd it configures througth files in /etc/systemd/system directory
	file { $limits:
		ensure => 'directory',
		recurse => true,
    require => Package[$packages],
	}

  # update limits file
  file { "${limits}/limits.conf":
    ensure => present,
    content => template('rabbitmq/rabbitmq.limits.erb'),
    require => File[$limits],
    notify => Exec['reload_systemd'],
  }

  # reload systemd to reload units
  exec { 'reload_systemd':
    command => "/bin/systemctl daemon-reload ",
    user => 'root',
    group => 'root',
    require => File["${limits}/limits.conf"],
    refreshonly => 'true',
    notify => Service[$services],
  }

  # ensure rabbitmq service running and enabled
  service { $services:
    provider => 'systemd',
    ensure => running,
    enable => true,
    require => File["${limits}/limits.conf"],
  }

  #
  # Configuration tasks
  #

  # add 'admin' user
  exec { "configure_user_accounts_${hostname}":
    command => "/usr/sbin/rabbitmqctl add_user ${user} ${password} \
             && /usr/sbin/rabbitmqctl set_user_tags ${user} administrator \
             && /usr/sbin/rabbitmqctl set_permissions -p / ${user} \".*\" \".*\" \".*\"",
    user => 'root',
    group => 'root',
    require => Service[$services],
    unless => "/usr/sbin/rabbitmqctl list_users | /bin/grep ${user}",
  }

  # delete default user 'guest'
  exec { "delete_guest_user_${hostname}":
    command => "/usr/sbin/rabbitmqctl delete_user guest",
    user => 'root',
    group => 'root',
    require => Service[$services],
    onlyif => "/usr/sbin/rabbitmqctl list_users | /bin/grep guest",
  }

  # enable plugins
  if $plugins != undef {
    $plugins.each|$plugin| {
      exec { "enable_plugin_${plugin}_${hostname}":
        command => "/usr/sbin/rabbitmq-plugins enable ${plugin}",
        user => 'root',
        group => 'root',
        require => Service[$services],
        unless => "/usr/sbin/rabbitmqctl status | /bin/grep ${plugin}",
      }
    }
  }

  # on primary node change cluster name
  if $initial_node == $hostname {
    exec { "set_rabbitmq_cluster_name":
      command => "/usr/sbin/rabbitmqctl set_cluster_name ${cluster}",
      user => 'root',
      group => 'root',
      require => Service[$services],
      unless => "/usr/sbin/rabbitmqctl cluster_status | /bin/grep '{cluster_name,<<\"${cluster}\">>},'",
    }

  # on other nodes join to cluster
  } else {
    exec { "join_cluster_${hostname}":
      command => "/usr/sbin/rabbitmqctl stop_app \
               && /usr/sbin/rabbitmqctl join_cluster ${cluster}@${initial_node} \
               && /usr/sbin/rabbitmqctl start_app",
      user => 'root',
      group => 'root',
      require => Service[$services],
      unless => "/usr/sbin/rabbitmqctl cluster_status | /bin/grep ${initial_node}",
    }
  }
}
