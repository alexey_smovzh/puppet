todo: now in mariadb.cnf presented parameters only for galera cluster startup
      need to provide mysql tuning related parameters


### 0. Description
This module install MariaDB with Galera multi-master cluster.
All cluster configuration must be provided in init.pp file.
This module also include dockerize.pp manifest to run MariaDB in
docker container, but it not properly tested, look at known issues.


### 1. Tested environments
This module developed and tested on Ubuntu 16.04LTS and 18.04LTS.


### 2. Usage
Write cluster configuration in init.pp manifest.
Then include install class to profile:

```
    # Install
    include mariadb::install

    # Delete
    include mariadb::delete

```


### 3. Known backgrounds and issues
In my test environment

ubuntu 16.04LTS (host) -> kvm 2.25 -> docker-ce 18.06

Primary cluster node in docker container starts fine, but secondary nodes
got following error when try to join to cluster. Probably this error due
overvirtualized environment. On docker running on baremetal server all will be ok.

```
180915 11:39:57 [ERROR] mysqld got signal 11 ;
This could be because you hit a bug. It is also possible that this binary
or one of the libraries it was linked against is corrupt, improperly built,
or misconfigured. This error can also be caused by malfunctioning hardware.

To report this bug, see https://mariadb.com/kb/en/reporting-bugs

We will try our best to scrape up some info that will hopefully help
diagnose the problem, but since we have already crashed,
something is definitely wrong and this may fail.

Server version: 10.3.9-MariaDB-1:10.3.9+maria~bionic
key_buffer_size=0
read_buffer_size=131072
max_used_connections=0
max_threads=153
thread_count=2
It is possible that mysqld could use up to
key_buffer_size + (read_buffer_size + sort_buffer_size)*max_threads = 336324 K  bytes of memory
Hope that's ok; if not, decrease some variables in the equation.

Thread pointer: 0x0
Attempting backtrace. You can use the following information to find out
where mysqld died. If you see no messages after this, something went
terribly wrong...
stack_bottom = 0x0 thread_stack 0x49000
/usr/sbin/mysqld(my_print_stacktrace+0x2e)[0x55de11c8608e]
/usr/sbin/mysqld(handle_fatal_signal+0x5a5)[0x55de1171f515]
/lib/x86_64-linux-gnu/libpthread.so.0(+0x12890)[0x7f9af92a2890]
/lib/x86_64-linux-gnu/libc.so.6(abort+0x230)[0x7f9af87b38f0]
/usr/lib/libgalera_smm.so(+0x69053)[0x7f9af5fba053]
/usr/lib/libgalera_smm.so(_Z13gcs_core_recvP8gcs_coreP12gcs_act_rcvdx+0xd8a)[0x7f9af60fc60a]
/usr/lib/libgalera_smm.so(+0x1b0a2f)[0x7f9af6101a2f]
/lib/x86_64-linux-gnu/libpthread.so.0(+0x76db)[0x7f9af92976db]
/lib/x86_64-linux-gnu/libc.so.6(clone+0x3f)[0x7f9af889488f]
The manual page at http://dev.mysql.com/doc/mysql/en/crashing.html contains
information that should help you find out what is causing the crash.
Hangup
```


Cluster successfully handles with lost of 1 or 2 nodes in 3 node cluster,
when at least one node remain operational. But it can not survive the
complete cluster shutdown. It don't know which node are the last and which node
contain most older copy of data. So it can't establish quorum automatically
and need human intervention.

There are two possible solutions:

3.1. View content of '/var/lib/mysql/grastate.dat' file in each node.
Found one with 'safe_to_bootstrap: 1' and 'seqno:' with some positive value.
Start this node. If it come alive, start other cluster nodes.

```
node0$ sudo cat /var/lib/mysql/grastate.dat
  # GALERA saved state
  version: 2.1
  uuid:    ece1f588-ba90-11e8-b60f-2b564dd13350
  seqno:   -1
  safe_to_bootstrap: 0

node1$ sudo cat /var/lib/mysql/grastate.dat
# GALERA saved state
version: 2.1
uuid:    ece1f588-ba90-11e8-b60f-2b564dd13350
seqno:   23
safe_to_bootstrap: 1

node1$ sudo service mariadb start
node1$ sudo service mariadb status
...
node0$ sudo service mariadb start
```

3.2. If solution from p.3.1 not working. Backup mariadb data folder '/var/lib/mysql'
in all nodes. On one node start galera cluster from scratch, when primary node
comes operational start other nodes. Check database data.

```
node0$ sudo galera_new_cluster
node0$ sudo service mariadb status
...
nodeX$ sudo service mariadb start
```


### 4. Used documentation

Galera official documentation: http://galeracluster.com/documentation-webpages/startingcluster.html

MariaDB documentation: https://mariadb.com/kb/en/library/galera-cluster/
