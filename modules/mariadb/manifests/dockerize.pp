class mariadb::dockerize inherits mariadb {

  # Folder to store dockerfiles
  $dockerfiles = '/var/lib/docker/dockerfiles'


  # create Dockerfile
  file { "${dockerfiles}/${image}.Dockerfile":
    ensure => present,
    content => template("${image}/${image}.docker.erb"),
    notify => Exec["delete_image_${image}"],
  }

  # create configs
  $configs.each |$config| {
    file { "${dockerfiles}/${config}":
      ensure => present,
      content => template("${image}/${config}.erb"),
      notify => Exec["delete_image_${image}"],
    }
  }

  # delete image if dockerfile or config are changed
  exec { "delete_image_${image}":
    command => "/usr/bin/docker image rm ${image} --force",
    cwd => $dockerfiles,
    user => 'root',
    group => 'root',
    refreshonly => true,
    onlyif => "/usr/bin/docker image ls | /bin/grep ${image}",
  }

  # build image
  exec { "build_image_${image}":
    command => "/usr/bin/docker build -f ${image}.Dockerfile --no-cache=true -t ${image} .",
    cwd => $dockerfiles,
    user => 'root',
    group => 'root',
    timeout => 2400,             # max time for process execution in seconds, over it process will be killed
    require => File["${dockerfiles}/${image}.Dockerfile"],
    unless => "/usr/bin/docker image ls | /bin/grep ${image}",
  }

  # create volume
# todo: read function comment in docker/manifests/utils.pp
#  docker::utils::create_local_volume { $image: size => $volume_size }
  docker::utils::create_local_volume { $image: }

  # error:
  # [ERROR] WSREP: Requested size 314574120 for '/var/lib/mysql//galera.cache' exceeds available storage space 0: 28 (No space left on device)
  #
  # fix:
  # create empty file with size of sum of gcaches
  # wsrep_provider_options="gcache.size=300M; gcache.page_size=300M"
  $size = $gcache_size + $gcache_page_size
  exec { "create_gcache_${image}":
    command => "/usr/bin/truncate -s ${size}m /var/lib/docker/volumes/${image}_${hostname}/_data/galera.cache",
    onlyif => "/usr/bin/docker volume list | /bin/grep ${image}_${hostname}",
    creates => "/var/lib/docker/volumes/${image}_${hostname}/_data/galera.cache",
  }

  # create overlay network
  docker::utils::create_overlay_network { $image: network => $galera_network }

  # Standalone container run
  $ip = $nodes[$hostname]
  $run = "/usr/bin/docker run -itd \
                              -v ${image}_${hostname}:${home} \
                              -p 3306:3306 -p 4567:4567 -p 4568:4568 -p 4444:4444 \
                              --network mysql \
                              --ip ${ip} \
                              --restart unless-stopped \
                              ${image}"
  $check = "/usr/bin/docker ps | /bin/grep ${image}"

  # docker exec -ti <container> bash
  # mysql --socket=/var/lib/mysql/mysql.sock -u root
  # SHOW STATUS LIKE 'wsrep_cluster_size';
  exec { "run_image_${image}":
    command => $run,
    cwd => $dockerfiles,
    user => 'root',
    group => 'root',
    require => Exec["build_image_${image}"],
    unless => $check,
  }
}
