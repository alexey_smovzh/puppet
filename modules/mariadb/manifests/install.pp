class mariadb::install inherits mariadb {

  # check if it not already defined and install depencies
  $depencies.each|$dep| {
    if !defined(Package[$dep]) {
      package { $dep:
        ensure => installed,
      }
    }
  }

  # install mariadb apt repository
  apt::apt_repository { 'mariadb':
    key => '0xF1656F24C74CD1D8',
    file => $repository,
    repository => "deb [arch=amd64] http://ftp.eenet.ee/pub/mariadb/repo/${version}/ubuntu ${::lsbdistcodename} main",
  }

  # install common for all roles packages
  package { $packages:
    ensure => installed,
    require => [Apt::Apt_repository['mariadb'],
                Package[$depencies]]
  }

  # create config
  file { "/etc/mysql/mariadb.cnf":
    ensure => present,
    content => template('mariadb/mariadb.cnf.erb'),
    require => Package[$packages],
  }

  # mysql -u root -e "SHOW STATUS LIKE 'wsrep_cluster_size';"
  $lock_cluster = '/var/lib/mysql/cluster_initialized.lock'
  # stop mariadb service
  # wait 20 seconds until mariadb service shutdown
  # init galera cluster on initial node
  # create lock file to ensure we run init script only once
  # and wait 60 second to become server ready again
  exec { 'init_galera_cluster':
    command => "/usr/sbin/service mariadb stop \
             && /bin/sleep 20 \
             && /usr/bin/galera_new_cluster \
             && /usr/bin/touch ${lock_cluster} \
             && /bin/sleep 60 ",
    user => 'root',
    group => 'root',
    provider => 'shell',
    require => File["/etc/mysql/mariadb.cnf"],
    onlyif => "/bin/hostname | /bin/grep ${initial_node}",        # run only on initial node, galera cluster replicates to others
    unless => "/usr/bin/test -f ${lock_cluster}",
  }

  # in initial node service are started by galera_new_cluster script
  if $hostname != $initial_node {
    # ensure service is running and enabled
    service { $services:
      ensure => running,
      enable => true,
      subscribe => File["/etc/mysql/mariadb.cnf"],
    }
  }

  # set password for mariadb root user
  $lock_password = '/var/lib/mysql/root_password_set.lock'
  exec { 'set_mariadb_root_password':
    command => "/usr/bin/mysqladmin --user root password \"${password}\" \
             && /usr/bin/touch ${lock_password}",
    user => 'root',
    group => 'root',
    require => File["/etc/mysql/mariadb.cnf"],
    onlyif => [ "/bin/systemctl is-active mariadb | /bin/grep active",
                "/bin/hostname | /bin/grep ${initial_node}" ],
    unless => "/usr/bin/test -f ${lock_password}",
  }
}
