class mariadb {

  $depencies = ['software-properties-common', 'lsof', 'rsync']
  $packages = ['mariadb-server', 'mariadb-client-10.3', 'mariadb-common']
  $services = ['mysqld']
  $repository = '/etc/apt/sources.list.d/mariadb.list'
  $home = '/var/lib/mysql'
  # todo: bind to management interface
  $bind_address = resolv($hostname)

  $release = 'bionic'
  $version = '10.3'


  # cluster
  $mariadb = lookup('mariadb', Hash)
  $cluster = $mariadb['cluster_name']
  $initial_node = $mariadb['initial_node']
  $nodes = $mariadb['nodes']
  $password = $mariadb['password']


  # dockerize parameters
	$image = $name
  $configs = ['mariadb.cnf', 'start.sh']
  $galera_network = '172.16.100.0/24'
#  $nodes = { 'docker0' => '172.16.100.10', 'docker1' => '172.16.100.11', 'docker2' => '172.16.100.12',}
  $volume_size = '1024'             # persistent storage size in Mb

  # program parameters
  $gcache_size = 128
  $gcache_page_size = 128

}
